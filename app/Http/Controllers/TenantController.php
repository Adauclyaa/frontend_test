<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class TenantController extends Controller
{
    public function index(){
        $data['user'] = api_get_user_login()[0];
        $data['tenant'] = api_read('tenant/read?filter[id]='.$data['user']->tenantid)[0];
        return view ('tenant.dashboard', ["title" => "Dashboard"])->with($data);
    }

    public function get_started($tenantid)
    {
        $data['tenant'] = api_read('activetenant/read?filter[id]='.$tenantid)[0];

        if(api_get_user_login()[0]->tenantcompanyid != $data['tenant']->tenantcompanyid){
            $param = [
                'message' => 'Link Tidak Valid atau Tidak Sah'
            ];
            return redirect()->route('/')->withErrors($param);
        }

        $data['npwp'] = str_replace(['.', '-'], '', $data['tenant']->npwp);
        $siteid = $data['tenant']->siteid;
        $data['email'] = api_get_claims()->email;
        $data['site'] = api_read("site/read?filter[siteid]=$siteid")[0];
        $data['provinsi'] = api_read("wilayah_provinsi/read");
        $data['category'] = api_read_without_token('tenantcategory/read');
        $data['storetype'] = api_read('storetype/read');
        $data['credential'] = api_read('credential/read');
        $categoryid = $data['tenant']->categoryid;
        $data['listdocument'] = api_read("listdocument/read?filter[categoryid]=$categoryid&filter[is_mandatory]=1");
        return view ('tenant.get_started', ["title" => "Get Started"])->with($data);
    }

    public function list_tenant($tenantcompanyid)
    {
        $data['tenant'] = api_read('activetenant/read?filter[tenantcompanyid]='.$tenantcompanyid);
        return view ('tenant.list_tenant', ["title" => "Profile"])->with($data);
    }

    public function get_profile()
    {
        $data = api_get_user_login()[0];
        $tenant = api_read('activetenant/read?filter[tenantcompanyid]='.$data->tenantcompanyid);
        foreach ($tenant as $key => $value) {
            $register[] = $value->registerstep;
        }

        if(in_array(4, $register) || in_array(3, $register)){
            $param = [
                'status' => true,
                'message' => '',
                'link' => url('tenant/profile/'.$data->tenantcompanyid)
            ];
        }else{
            $param = [
                'status' => false,
                'message' => 'Selesaikan profil minimal sampai tahap ke 3'
            ];
        }

        return response()->json($param, 200);
    }

    public function profile($tenantcompanyid)
    {
        if(api_get_user_login()[0]->tenantcompanyid != $tenantcompanyid){
            $param = [
                'message' => 'Link Tidak Valid atau Tidak Sah'
            ];
            return redirect()->route('/')->withErrors($param);
        }

        $data['tenantcompany'] = api_read('tenantcompany/read?filter[id]='.$tenantcompanyid)[0];
        $data['tenant'] = api_read('activetenant/read?filter[tenantcompanyid]='.$tenantcompanyid);
        $data['credential'] = api_read('credential/read');
        $data['tenantcategory'] = api_read('tenantcategory/read?filter[id]='.$data['tenantcompany']->categoryid)[0];
        $data['companyid'] = $tenantcompanyid;
        $count = count(api_read('activetenant/read?filter[tenantcompanyid]='.$tenantcompanyid.'&filter[status]=incomplete'));
        $count = $count + count(api_read('activetenant/read?filter[tenantcompanyid]='.$tenantcompanyid.'&filter[status]=inprogress'));
        if($count == 0){
            $data['tenantflag'] = 'false';
        }else{
            $data['tenantflag'] = 'true';
        }

        return view ('tenant.profile', ["title" => "Profile"])->with($data);
    }

    public function list_document()
    {
        $tenantid = $_POST['tenantid'];
        $tenantcompanyid = $_POST['tenantcompanyid'];
        $categoryid = $_POST['categoryid'];
        $data = api_read('listdocument/read?filter[categoryid]='.$categoryid);
        foreach ($data as $key => $value) {
            $document = api_read("tenantdocument/read?filter[tenantid]=$tenantid&filter[documentid]=$value->documentid");
            if(isset($document[0])){
                $document = $document[0];
                $value->docid = $document->id;
                $value->file = $document->file;
                $value->expireddate = $document->expireddate;
                $value->document_created_at = $document->created_at;
                $value->status =  $document->status;
                $value->documentapprovalby =  $document->documentapprovalby;
                $value->name_approval =  $document->name_approval;
                $value->url = $document->url.$tenantcompanyid.'/'.$document->file;
                $value->documentapprovaldate =  $document->documentapprovaldate;
                $value->documentapprovalnote =  $document->documentapprovalnote;
                $value->release =  $document->release;
            }else{
                $value->docid = '0';
                $value->file = NULL;
                $value->expireddate = '-';
                $value->document_created_at = NULL;
                $value->status =  'empty';
                $value->documentapprovalby = NULL;
                $value->name_approval = NULL;
                $value->url = NULL;
                $value->documentapprovaldate = NULL;
                $value->documentapprovalnote = NULL;
                $value->release = NULL;
            }
        }

        return DataTables::of($data)->addIndexColumn()
        ->addColumn('document', function($row){
            if($row->url == NULL){
                $btn = $row->documentname;
            }else{
                $btn = '
                <a href="javascript:void(0)" class="showfile" link="'.$row->url.'"
                    docid="'.$row->docid.'" docdate="'.$row->document_created_at.'" expdate="'.$row->expireddate.'"
                    hasexpiredate="'.$row->hasexpireddate.'" totalrelease="'.$row->release.'">
                    <img src="'.asset('theme/img/pdf.svg').'" style="width:35px; height:35px;"> '.$row->documentname.'
                </a>
                ';
            }
            return $btn;
        })
        ->addColumn('status', function($row){
            if($row->status == 'approved'){
                $text = 'Diterima';
				$class = 'success';
				$reason = "";
			}else if($row->status == 'revised'){
                $text = 'Revisi';
				$class = 'warning';
				$reason = '<small>Alasan: '.$row->documentapprovalnote.'</small>';
			}else if($row->status == 'empty'){
                $text = 'Kosong';
				$class = 'danger';
				$reason = "";
			}else if($row->status == 'waiting_review'){
                $text = 'Menunggu ulasan';
				$class = 'secondary';
				$reason = "";
			}
            ($row->documentapprovalby != null) ? $by = '<small>('.$row->documentapprovaldate.') </small>' : $by = null ;

            $status = '<h6><span class="badge badge-'.$class.'">'.$text.'</span></h6>'.$by.'<br>'.$reason;
            return $status;
        })
        ->addColumn('action', function($row){
            if($row->status == 'empty'){
                $btn = '<button class="btn btn-primary btn-sm" onclick="modal_upload('.$row->docid.', '.$row->documentid.', \''.$row->hasexpireddate.'\', \''.$row->documentname.'\')">Unggah</button>';
            }else if($row->status == 'revised'){
                $btn = '<button class="btn btn-warning btn-sm" onclick="modal_revisi('.$row->docid.', \''.$row->hasexpireddate.'\', \''.$row->documentname.'\')">Revisi</button>';
            }else if($row->status == 'approved'){
                $btn = '<button class="btn btn-info btn-sm" onclick="modal_update('.$row->docid.', \''.$row->hasexpireddate.'\', \''.$row->documentname.'\')">Perbarui</button>';
            }else{
                $btn = '';
            }
            return $btn;
        })
        ->rawColumns(['action', 'status', 'document'])
        ->make(true);
    }

    public function upload_document_legal(Request $request)
    {
        $post = $request->All();

        if($post['type'] == 'tambah'){

            $name = $_FILES['file']['name'];
            $tmp_name = $_FILES['file']['tmp_name'];
            $type = $_FILES['file']['type'];

            $file = curl_file_create($tmp_name,$type,$name);

            $tambah_dokumen = [
                'file' => $file,
                'tenantid' => $post['tenantid'],
                'documentid' => $post['documentid'],
                'expireddate' => '',
                'created_by' => api_get_claims()->id,
                'updated_by' => api_get_claims()->id,
                'leasingreviewby' => '',
                'leasingreview' => '0',
                'leasingnote' => '',
                'leasingdate' => '',
                'documentapprovalby' => '',
                'documentapproval' => '',
                'documentapprovalrole' => '',
                'documentapprovalnote' => '',
                'documentapprovaldate' => '',
                'status' => 'waiting_review',
                'release' => '1',
                'remark' => '',
                'is_additional_document' => '0',
                'additional_doc' => '',
                'upload_by' => 'tenant',
            ];

            $token = $_COOKIE['token'];
            $content = 'multipart/form-data';
            $aksi = api_create('tenantdocument/create', $tambah_dokumen, $token, $content);

        }else if($post['type'] == 'update'){

            $token = $_COOKIE['token'];
            $document = api_read("tenantdocument/read?filter[id]=".$post['docid'])[0];
            $tenant = api_read("tenant/read?filter[id]=".$document->tenantid)[0];

            $document_old = (array) $document;
            $dat = api_create('tenantdocumenthistory/create', json_encode($document_old), $token);

            $name = $_FILES['file']['name'];
            $tmp_name = $_FILES['file']['tmp_name'];
            $type = $_FILES['file']['type'];

            $file = curl_file_create($tmp_name,$type,$name);

            $tambah_dokumen = [
                'file' => $file,
                'expireddate' => '',
                'created_by' => api_get_claims()->id,
                'updated_by' => api_get_claims()->id,
                'status' => 'waiting_review',
                'documentapprovalby' => NULL,
                'release' => $document->release+1,
                'upload_by' => 'tenant',
            ];

            $token = $_COOKIE['token'];
            $content = 'multipart/form-data';
            $req = 'POST';
            $aksi = api_update('tenantdocument/update/'.$post['docid'], $tambah_dokumen, $req, $content);

            $activity = array(
				'tenantid'	 	=> $document->tenantid,
				'activity'      => $tenant->storename.' telah memperbarui dokumen "'.$document->documentname.'"',
				'created_by'    => api_get_claims()->id
            );
            api_create('tenantdocumentactivity/create', json_encode($activity), $token);


        }

        return response()->json($aksi, 200);
    }

    public function getwilayah(Request $request)
    {
        $post = $request->All();
        $value = str_replace(' ', '%20', $post['data']);
        $filter = $post['filter'];
        $get = $post['get'];

        $data = api_read("wilayah_$get/read?filter[$filter]=$value");
        return response()->json($data, 200);
    }

    public function save_started(Request $request)
    {
        $post = $request->All();
        $tenantid = $post['tenantid'];
        $tenantcompanyid = $post['tenantcompanyid'];
        $tenant = api_read('tenant/read?filter[id]='.$tenantid)[0];

        if($post['step'] == 1){
            $param = [
                'registerstep' => $post['step']
            ];
            $post_field = json_encode($param);
            $update = api_update('tenant/update/'.$tenantid, $post_field);

            if($tenant->registerstep >= 1){
                $update = ['status' => true];
            }

            return response()->json($update, 200);
        }else if($post['step'] == 2){

            $name = $_FILES['logo']['name'];
            $tmp_name = $_FILES['logo']['tmp_name'];
            $type = $_FILES['logo']['type'];

            $param = [
                'companyname' => $post['companyname'],
                'email' => $post['companymail'],
                'npwp' => $post['npwp'],
                'provinsi' => $post['provinsi'],
                'kabupaten' => $post['kota'],
                'kecamatan' => $post['kecamatan'],
                'kelurahan' => $post['kelurahan'],
                'kodepos' => $post['kodepos'],
                'address' => $post['alamat'],
                'registerstep' => $post['step']
            ];

            if(empty($name)){
                $file = '';
            }else{
                $file = curl_file_create($tmp_name,$type,$name);
                $param['logo'] = $file;
            }

            $req = 'POST';
            $content = 'multipart/form-data';
            $update = api_update('tenantcompany/updatepost/'.$tenantcompanyid, $param, $req, $content);

            if($tenant->registerstep == 2){
                $update = ['status' => true];
            }

            return response()->json($update, 200);

        }else if($post['step'] == 3){

            $param = [
                'storename' => $post['tenantname'],
                'email' => $post['tenantemail'],
                'businesstype' => $post['jenistenant'],
                'luas' => $post['luas'],
                'description' => $post['deskripsitenant'],
                'lantai' => $post['lantai'],
                'unit' => $post['unit'],
                'type' => 'self',
                'created_by' => api_get_claims()->id,
                'updated_by' => api_get_claims()->id,
                'step' => 2,
                'registerstep' => $post['step']
            ];

            $post_field = json_encode($param);
            $update = api_update('tenant/update/'.$tenantid, $post_field);

            if($tenant->registerstep == 3){
                $update = ['status' => true];
            }

            return response()->json($update, 200);

        }else if($post['step'] == 4){

            $post = $request->All();

            foreach($post['id_document'] as $key => $value){

                $name = $_FILES['document']['name'][$key];
                $tmp_name = $_FILES['document']['tmp_name'][$key];
                $type = $_FILES['document']['type'][$key];

                $file = curl_file_create($tmp_name,$type,$name);

                $tambah_dokumen = [
                    'file' => $file,
                    'tenantid' => $tenantid,
                    'documentid' => $value,
                    'expireddate' => '',
                    'created_by' => api_get_claims()->id,
                    'updated_by' => api_get_claims()->id,
                    'leasingreviewby' => '',
                    'leasingreview' => '0',
                    'leasingnote' => '',
                    'leasingdate' => '',
                    'documentapprovalby' => '',
                    'documentapproval' => '',
                    'documentapprovalrole' => '',
                    'documentapprovalnote' => '',
                    'documentapprovaldate' => '',
                    'status' => 'waiting_review',
                    'release' => '1',
                    'remark' => '',
                    'is_additional_document' => '0',
                    'additional_doc' => '',
                    'upload_by' => 'tenant',
                ];

                $token = $_COOKIE['token'];
                $content = 'multipart/form-data';
                $update = api_create('tenantdocument/create', $tambah_dokumen, $token, $content);

            }

            if($update){
                $param = [
                    'registerstep' => $post['step'],
                    'status' => 'inprogress',
                    'step' => 3
                ];
                $post_field = json_encode($param);
                api_update('tenant/update/'.$tenantid, $post_field);
            }

            return response()->json($update, 200);
        }
    }

    public function detail_tenant($tenantid)
    {
        $data['tenant'] = api_read('activetenant/read?filter[id]='.$tenantid);
        if(!isset($data['tenant'][0]) || api_get_user_login()[0]->tenantcompanyid != $data['tenant'][0]->tenantcompanyid){
            $param = [
                'message' => 'Link Tidak Valid atau Tidak Sah'
            ];
            return redirect()->route('/')->withErrors($param);
        }
        $data['tenant'] = $data['tenant'][0];
        $data['credential'] = api_read('credential/read');
        $data['tenantcategory'] = api_read('tenantcategory/read?filter[id]='.$data['tenant']->categoryid)[0];
        return view ('tenant.detail_tenant', ["title" => "Dashboard"])->with($data);
    }

    public function documenthistory()
    {
        $doc = api_read('tenantdocument/read?filter[id]='.$_POST['docid'])[0];
        $data = api_read('tenantdocumenthistory/read?filter[tenantid]='.$_POST['tenantid'].'&filter[documentid]='.$doc->documentid);
        return DataTables::of($data)->addIndexColumn()
        ->addColumn('document', function($row){
            $doc = '
            <a href="'.$row->url.$_POST['tenantcompanyid'].'/'.$row->file.'">
                <img src="'.asset('theme/img/pdf.svg').'" style="width:35px; height:35px;"> '.$row->documentname.'
            </a>
            ';
            return $doc;
        })
        ->addColumn('tanggal', function($row){
            $tgl = date("d-m-Y H:i:s", strtotime($row->updated_at));
            return $tgl;
        })
        ->rawColumns(['document', 'tanggal'])
        ->make(true);
    }
    public function adendum_pengalihan(Request $request){
        $data['tenantcategory'] = api_read_without_token('tenantcategory/read');
        $data['error'] = "";
        return view ('tenant.adendumpengalihan', ["title" => "Pengalihan"])->with($data);
    }
    public function adendum_pengalihan_list(Request $request) {
        $step = 3;
        $siteid = api_get_claims()->siteid;
        if(isset($request->All()['status'])){
            if($request->All()['status'] != 'all'){
                $filterstatus = $request->All()['status'];
                $list = api_read("pengalihan/read?filter[siteid]=$siteid&filter[status]=$filterstatus");
            }else{
                $list = api_read("pengalihan/read?filter[siteid]=".$siteid);
            }
        }else{
            $list = api_read("pengalihan/read?filter[siteid]=".$siteid);
        }

        return DataTables::of($list)->addIndexColumn()
        ->addColumn('action', function($row){
            // $btn = '<a href="'.url("admin/detail_incomplete_tenant/$row->id").'" class="btn btn-primary btn-sm" target="_blank">Review</a>';
            $btn = '<a class="btn btn-primary btn-sm" target="_blank">Review</a>';
            return $btn;
        })
        ->rawColumns(['action'])
        ->make(true);
    }
    public function adendum_pengalihan_create(Request $request){
        $data['error'] = "";
        $tenantid = api_get_user_login()[0]->tenantid;
        $data['Data'] = api_read('activetenant/read?filter[id]='.$tenantid)[0];
        // dd($data['Data']);
        return view ('tenant.adendumpengalihancreate', ["title" => "Pengalihan"])->with($data);
    }
    public function adendum_pengalihan_save(Request $request){
        $post = $request->All();
        $date = $post['date'];
        $up = $post['up'];
        $nama_perusahaan = $post['nama_perusahaan'];
        $perwakilan_perusahaan = $post['perwakilan_perusahaan'];
        $jabatan = $post['jabatan'];
        $alamat_korespondensi = $post['alamat_korespondensi'];
        $kontak_pihak_pertama = $post['kontak_pihak_pertama'];

        $nama_toko = $post['nama_toko'];
        $lantai_unit = $post['lantai_unit'];

        $pihak_kedua_nama = $post['pihak_kedua_nama'];
        $pihak_kedua_perusahaan = $post['pihak_kedua_perusahaan'];
        $pihak_kedua_perwakilan = $post['pihak_kedua_perwakilan'];
        $pihak_kedua_jabatan = $post['pihak_kedua_jabatan'];
        $pihak_kedua_alamat = $post['pihak_kedua_alamat'];
        $pihak_kedua_kontak = $post['pihak_kedua_kontak'];

        $pt_pihak_pertama = $post['pt_pihak_pertama'];
        $nama_pt_pihak_pertama = $post['nama_pt_pihak_pertama'];
        $jabatan_pt_pihak_pertama = $post['jabatan_pt_pihak_pertama'];

        $pt_kedua_kontak = $post['pt_kedua_kontak'];
        $nama_pt_kedua_kontak = $post['nama_pt_kedua_kontak'];
        $jabatan_pt_kedua_kontak = $post['jabatan_pt_kedua_kontak'];

        $siteid = api_read('activetenant/read?filter[id]='.api_get_user_login()[0]->tenantid)[0]->siteid;

        $update_dokumen = [
            'tenantid' => api_get_user_login()[0]->tenantid,
            'namaperusahaan' =>$nama_perusahaan,
            'siteid' =>$siteid,
            'created_by' =>"1",
            'updated_by' =>"1",
            'tanggal' =>$date,
            'up' =>$up,
            'pp_nama_perorangan' =>$nama_perusahaan,
            'pp_diwakili_oleh' =>$perwakilan_perusahaan,
            'pp_jabatan' =>$jabatan,
            'pp_alamat_korespondensi' =>$alamat_korespondensi,
            'pp_telepon_fax' =>$kontak_pihak_pertama,
            'nama_toko' =>$nama_toko,
            'lantai_unit' =>$lantai_unit,
            'pk_nama' =>$pihak_kedua_nama,
            'pk_perusahaan' =>$pihak_kedua_perusahaan,
            'pk_diwakili_oleh' =>$pihak_kedua_perwakilan,
            'pk_jabatan' =>$pihak_kedua_jabatan,
            'pk_alamat' =>$pihak_kedua_alamat,
            'pk_telepon_fax' =>$pihak_kedua_kontak,
            'pp_pt' =>$pt_pihak_pertama,
            'pp_nama' =>$nama_pt_pihak_pertama,
            'jabatan' =>$jabatan_pt_pihak_pertama,
            'pk_pt' =>$pt_kedua_kontak,
            'nama_ttd' =>$nama_pt_kedua_kontak,
            'jabatan_ttd' =>$jabatan_pt_kedua_kontak,
            'status' =>"waiting_review",
            'note' =>"",
            'approver' =>""
        ];
        $post_field = json_encode($update_dokumen);
        $token = $_COOKIE['token'];
        $update = api_create('pengalihan/create', $post_field, $token);
        return redirect('tenant/adendum_pengalihan');
    }

    public function new_tenant($tenantcompanyid){
        if(api_get_user_login()[0]->tenantcompanyid != $tenantcompanyid){
            $param = [
                'message' => 'Link Tidak Valid atau Tidak Sah'
            ];
            return redirect()->route('/')->withErrors($param);
        }

        $data['tenant'] = api_read('activetenant/read?filter[tenantcompanyid]='.$tenantcompanyid)[0];
        $data['npwp'] = str_replace(['.', '-'], '', $data['tenant']->npwp);
        $siteid = $data['tenant']->siteid;
        $data['email'] = api_get_claims()->email;
        $data['site'] = api_read('site/read');
        $data['provinsi'] = api_read("wilayah_provinsi/read");
        $data['category'] = api_read_without_token('tenantcategory/read');
        $data['storetype'] = api_read('storetype/read');
        $data['credential'] = api_read('credential/read');
        $categoryid = $data['tenant']->categoryid;
        $data['listdocument'] = api_read("listdocument/read?filter[categoryid]=$categoryid&filter[is_mandatory]=1");
        return view ('tenant.addnewtenant', ["title" => "New Tenant"])->with($data);
    }
    public function save_new_tenant(Request $request)
    {
        $post = $request->All();
        $tenantid = $post['tenantid'];
        $tenantcompanyid = $post['tenantcompanyid'];
        $tenant = api_read('tenant/read?filter[id]='.$tenantid)[0];

        if($post['step'] == 1){
            $update = ['status' => true];

            return response()->json($update, 200);
        }else if($post['step'] == 2){
            if($post['lokasi'] == ''){
                $update = ['status' => false,'message' => 'Lokasi harus di Isi'];
                return response()->json($update, 200);
            }else{
                $param = [
                    'siteid' => $post['lokasi'],
                    'storename' => $post['tenantname'],
                    'email' => $post['tenantemail'],
                    'businesstype' => $post['jenistenant'],
                    'luas' => $post['luas'],
                    'description' => $post['deskripsitenant'],
                    'lantai' => $post['lantai'],
                    'unit' => $post['unit'],
                    'type' => 'self',
                    'created_by' => api_get_claims()->id,
                    'updated_by' => api_get_claims()->id,
                    'step' => 2,
                    'registerstep' => 3,
                    'status' => 'incomplete',
                    'tenantcompanyid' => $tenantcompanyid
                ];

                $post_field = json_encode($param);
                $token = $_COOKIE['token'];
                $update = api_create('tenant/create', $post_field,$token);
                if($tenant->registerstep == 2){
                    $update = ['status' => true];
                }

                return response()->json($update, 200);
            }

        }else if($post['step'] == 3){

            $post = $request->All();

            foreach($post['id_document'] as $key => $value){

                $name = $_FILES['document']['name'][$key];
                $tmp_name = $_FILES['document']['tmp_name'][$key];
                $type = $_FILES['document']['type'][$key];

                $file = curl_file_create($tmp_name,$type,$name);

                $tambah_dokumen = [
                    'file' => $file,
                    'tenantid' => $tenantid,
                    'documentid' => $value,
                    'expireddate' => '',
                    'created_by' => api_get_claims()->id,
                    'updated_by' => api_get_claims()->id,
                    'leasingreviewby' => '',
                    'leasingreview' => '0',
                    'leasingnote' => '',
                    'leasingdate' => '',
                    'documentapprovalby' => '',
                    'documentapproval' => '',
                    'documentapprovalrole' => '',
                    'documentapprovalnote' => '',
                    'documentapprovaldate' => '',
                    'status' => 'waiting_review',
                    'release' => '1',
                    'remark' => '',
                    'is_additional_document' => '0',
                    'additional_doc' => '',
                    'upload_by' => 'tenant',
                ];

                $token = $_COOKIE['token'];
                $content = 'multipart/form-data';
                $update = api_create('tenantdocument/create', $tambah_dokumen, $token, $content);

            }

            if($update){
                $param = [
                    'registerstep' => $post['step'],
                    'status' => 'inprogress',
                    'step' => 4
                ];
                $post_field = json_encode($param);
                api_update('tenant/update/'.$tenantid, $post_field);
            }

            return response()->json($update, 200);
        }
    }
}
