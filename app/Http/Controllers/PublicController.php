<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PublicController extends Controller
{

    public function __construct()
    {
    }

    function index(){
        $response = api_read_without_token('site/read');
        return response()->json($response, 200);
    }

    function change_site(){
        $user = api_read('get_user', $_COOKIE['token']);
        $field = ["siteid" => $_POST['siteid']];
        $field_json = json_encode($field);

        if(isset($user[0]->id)){
            $update = api_update('site_user/update/'.$user[0]->id, $field_json);
            setcookie('token', $update->token, time() + (28800 * 30), "/");
        }else{
            $update = ['status'=>false];
        }

        if($update->status){

            $split = explode('.', $update->token);
            $base_decode = base64_decode($split[1]);
            $data_decode = json_decode($base_decode);

            $response['status'] = true;
            $response['code'] = 200;
            $response['token'] = $update->token;
            $response['site'] = $data_decode->siteid;
            $response['redirect'] = $_SERVER['HTTP_REFERER'];
        }
        return response()->json($response, 200);
    }
}
