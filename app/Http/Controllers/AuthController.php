<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Cookie;
use Session;

class AuthController extends Controller
{
    function login(){
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => api_domain_url().'login',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
                "email": "'.$_POST['email'].'",
                "password": "'.$_POST['password'].'"
            }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $hasil = curl_exec($curl);
        curl_close($curl);
        $hasil = json_decode($hasil);

        if(isset($hasil->success) && $hasil->success==true){

            $split = explode('.', $hasil->token);
            $base_decode = base64_decode($split[1]);
            $data_decode = json_decode($base_decode);

            if($data_decode->role == 'superadmin'){
                $redirect = 'admin';
            }else if($data_decode->role == 'admin'){
                $redirect = 'admin';
            }else if($data_decode->role == 'tenant'){
                $redirect = 'tenant';
            }else{
                $redirect = '/';
            }

            setcookie('token', $hasil->token, time() + (28800 * 30), "/");

            $response["status"] = true;
            $response["token"] = $hasil->token;
            $response["redirect"] = $redirect;
            $response["name"] = $data_decode->name;
            $response["email"] = $data_decode->email;
        }else{
            $response["status"] = false;
            $response["message"] = (isset($hasil->error->email[0])) ? $hasil->error->email[0] : $hasil->message;
            $response["token"] = "Invalid";
            $response["redirect"] = '/';
        }

        return response()->json($response, 200);
    }

    function register(){
        $json_post = json_encode($_POST);
        $put = api_create('register', $json_post, null);

        if(isset($put->success)){
            $response["status"] = true;
            $response["message"] = $put->message."<br>Mohon menunggu proses review dari tim kami, Terima kasih";
        }else{
            if(isset($put->error)){
                $key = key((array)$put->error);
                $response["status"] = false;
                $response["message"] = $put->error->$key[0];
            }else{
                $response["status"] = false;
                $response["message"] = $put->error;
            }
        }

        return response()->json($response, 200);
    }

    function signup(){
        $data['site'] = api_read_without_token('site/read');
        $data['category'] = api_read_without_token('tenantcategory/read');
        return view ('auth.register')->with($data);
    }

    function logout(Request $request){
        $hasil = api_logout();
        if($hasil){
            if (isset($_COOKIE['token'])) {
                unset($_COOKIE['token']);
                setcookie('token', null, -1, '/');
                return '<script>localStorage.clear();location.href = "/";</script>';
            } else {
                return '<script>localStorage.clear();location.href = "/";</script>';
            }
            return '<script>localStorage.clear();location.href = "/";</script>';
        }
    }

    function backdoor(Request $request){
        $user = api_read('get_user', $_COOKIE['token']);
        $field = [];
        $field_json = json_encode($field);

        if(isset($user[0]->id)){
            $update = api_create('usersinternal/backdoor/'.$_POST['id'], $field_json, $_COOKIE['token']);
            setcookie('token', $update->token, time() + (28800 * 30), "/");
        }else{
            $update = ['status'=>false];
        }

        if($update->status){

            $split = explode('.', $update->token);
            $base_decode = base64_decode($split[1]);
            $data_decode = json_decode($base_decode);

            $response['status'] = true;
            $response['code'] = 200;
            $response['token'] = $update->token;
            $response['site'] = $data_decode->siteid;
            $response['email'] = $data_decode->email;
            $response['name'] = $data_decode->name;
            $response['redirect'] = $_SERVER['HTTP_REFERER'];
        }
        return response()->json($response, 200);

    }
}
