<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class MasterController extends Controller
{

    public function __construct()
    {
    }

    function users_admin(){
        $data['department'] = api_read('department/read');
        $data['site'] = api_read('site/read');
        $data['role'] = api_read('groups/read');
        return view ('admin.users_admin', ["title" => "Users Admin"])->with($data);
    }

    public function list_users_admin()
    {
        $list = api_read("usersinternal/read");
        return DataTables::of($list)->addIndexColumn()
        ->addColumn('action', function($row){
            $btn = '
            <div class="dropdown" style="vertical-align:middle;">
                <button class="btn dropdown-toggle btn-sm btn-light btn-active-light-primary " type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                    Action
                </button>
                <ul class="dropdown-menu menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-125px py-4" aria-labelledby="dropdownMenuButton1">
                    <li>
                        <div class="menu-item px-3">
                            <a onclick="ubah(' . "'" . $row->id . "'" . ')" class="menu-link px-3">
                                <i class="fas fa-pencil fs-5 text-info" style="margin-right:10px;"></i>
                                Edit
                            </a>
                        </div>
                    </li>
                    <li>
                        <div class="menu-item px-3">
                            <a onclick="hapus(' . "'" . $row->id . "'" . ',' . "'" . $row->name . "'" . ')" class="menu-link px-3" data-kt-customer-table-filter="delete_row">
                                <i class="fas fa-trash fs-5 text-danger" style="margin-right:10px;"></i>
                                Delete
                            </a>
                        </div>
                    </li>
                    <li>
                        <div class="menu-item px-3">
                            <a onclick="backlogin('.$row->id.')" class="menu-link px-3" data-kt-customer-table-filter="delete_row">
                                <i class="fas fa-key fs-5 text-warning" style="margin-right:10px;"></i>
                                Log In
                            </a>
                        </div>
                    </li>
                </ul>
             </div>';

            return $btn;
        })
        ->rawColumns(['action'])
        ->addIndexColumn()
        ->make(true);
    }

    public function edit_admin($id)
    {
        $data['user'] = api_read("usersinternal/read?filter[id]=$id");
        $adminsite = api_read("adminsite/read?filter[userid]=$id");
        $department = api_read("usersgroups/read?filter[user_id]=$id");

		foreach($adminsite as $key => $value){
			$data['site'][] = $value->siteid;
		}

		foreach($department as $key => $value){
			$data['department'][] = $value->departmentid;
		}

        return response()->json($data, 200);
    }

    public function add_admin(Request $request)
    {
        $data = $request->all();
        if($data['action'] == 'edit'){
            $field = [
                'name' => $data['name'],
                'email' => $data['email'],
                'username' => $data['username'],
                'password'=> $data['password'],
                'status' => $data['status'],
                'role' => $data['role'],
                'department' => $data['department'],
                'company' => $data['company'],
                'approver' => $data['approver'],
                'notification' => $data['notification'],
            ];

            $post_field = json_encode($field);
            $token = $_COOKIE['token'];
            $update = api_update('usersinternal/update/'.$data['id'], $post_field);

            return response()->json($update, 200);
        }else{
            $field = [
                'name' => $data['name'],
                'email' => $data['email'],
                'username' => $data['username'],
                'password'=> $data['password'],
                'status' => $data['status'],
                'role' => $data['role'],
                'department' => $data['department'],
                'company' => $data['company'],
                'approver' => $data['approver'],
                'notification' => $data['notification'],
            ];

            $post_field = json_encode($field);
            $token = $_COOKIE['token'];
            $create = api_create('usersinternal/create', $post_field, $token);

            return response()->json($create, 200);
        }
    }

    public function delete_admin(Request $request)
    {
        $data = $request->all();
        $token = $_COOKIE['token'];
        $delete = api_delete('usersinternal/delete/', $data['id']);

        return response()->json($delete, 200);
    }
}
