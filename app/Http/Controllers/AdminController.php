<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Session;

class AdminController extends Controller
{

    public function __construct()
    {
    }

    function index(){
        $data = [];
        return view ('admin.dashboard', ["title" => "dashboard"])->with($data);
    }

    public function view_list_formulir()
    {
        $data = [];
        return view ('admin.formulir_pemeriksaan', ["title" => "Formulir Pemeriksaan"])->with($data);
    }

    function list_formulir() {

        $list = api_read("transaksi/read");

        return DataTables::of($list)->addIndexColumn()
        ->addColumn('action', function($row){
            $btn = '<a href="'.url("transaksi/add_formulir_pemeriksaan/$row->id").'" class="btn btn-warning btn-sm" target="_blank">Edit</a>
            <a href="'.url("transaksi/delete_formulis_pemeriksaan/$row->id").'" class="btn btn-danger btn-sm" target="_blank">Hapus</a>';
            return $btn;
        })
        ->rawColumns(['action'])
        ->make(true);
    }

    public function add_formulir_pemeriksaan($id){
        $data['transaksi'] = api_read("transaksi/read?filter[id]=$id");
        $data['mastersampel'] = api_read("mastersampelair/read");
        $data['masterdiagnosa'] = api_read("masterdiagnosa/read");

        return view ('admin.add_formulir_pemeriksaan', ["title" => "Tambah Formulir Pemeriksaan"])->with($data);
    }

    public function add_transaksi()
    {
        $token = $_COOKIE['token'];
        return api_create('transaksi/create', [], $token);
    }

    public function update_transaksi($id)
    {
        $data_sampel = [];
        foreach ($_POST['id_sampel'] as $key => $value) {
            $data_sampel[$key]['id'] = $value;
            $data_sampel[$key]['qty'] = (!empty($_POST['qty_sampel'][$key])) ? $_POST['qty_sampel'][$key] : 0;
            $data_sampel[$key]['jumlah'] = $_POST['tarif_sampel'][$key] * (!empty($_POST['qty_sampel'][$key])) ? $_POST['qty_sampel'][$key] : 0;
        }

        $data_diagnosa = [];
        foreach ($_POST['id_diagnosa'] as $key => $value) {
            $data_diagnosa[$key]['id'] = $value;
            $data_diagnosa[$key]['qty'] = (!empty($_POST['qty_diagnosa'][$key])) ? $_POST['qty_diagnosa'][$key] : 0;
            $data_diagnosa[$key]['jumlah'] = $_POST['tarif_diagnosa'][$key] * (!empty($_POST['qty_diagnosa'][$key])) ? $_POST['qty_diagnosa'][$key] : 0;
        }

        $data = [
            'nama' => $_POST['nama'],
            'alamat' => $_POST['alamat'],
            'tanggal'  => $_POST['tanggal'],
            'data_sampel'  => $data_sampel,
            'data_diagnosa'  => $data_diagnosa,
        ];
        $data = json_encode($data);
        $update = api_update('transaksi/update/'.$id, $data, 'PUT');

        return response()->json($update, 200);

    }
}
