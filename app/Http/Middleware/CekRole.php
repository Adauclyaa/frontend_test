<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CekRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, $role)
    {
        if(isset($_COOKIE['token'])){
            if($role == "internal"){
                $allowed_group = ['superadmin', 'admin'];
            }else{
                $allowed_group = ['tenant', 'kontraktor'];
            }

            if(api_get_user_login() == null){
                return redirect()->route('signin');
            }else{
                if(in_array(api_get_user_login()[0]->role, $allowed_group)){
                    return $next($request);
                }else{
                    return redirect(route('tenant'));
                }
            }
        }else{
			return redirect()->route('signin');
        }
    }
}
