<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CekToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if(isset($_COOKIE['token'])){
            $cek = api_read('get_user', $_COOKIE['token']);
            if(isset($cek->message) && $cek->message == 'Token is Invalid'){
                return redirect()->route('logout')->with('message', $cek->message);
            }else if(isset($cek->message) && $cek->message == 'Token is Expired'){
                return redirect()->route('logout')->with('message', $cek->message);
            }else if(isset($cek->message) && $cek->message == 'Authorization Token not found'){
                return redirect()->route('logout')->with('message', $cek->message);
            }else{
                return $next($request);
            }
        }else{
			return redirect()->route('signin');
        }
    }
}
