<?php

    function api_domain_url(){
        return 'http://127.0.0.1:8001/api/';
    }

    function api_create($point, $post_field, $token, $content=null){

        ($content == NULL) ? $content = "application/json" : $content = $content ;
        if($token != null){
            $auth = ['Authorization: Bearer '.$token, 'Content-Type: '.$content];
        }else{
            $auth = ['Content-Type: '.$content];
        }

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => api_domain_url().$point,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $post_field,
            CURLOPT_HTTPHEADER => $auth,
        ));

        $hasil = curl_exec($curl);
        curl_close($curl);
        $hasil = json_decode($hasil);

        return $hasil;
    }

    function api_read($point, $token=null){

        if($token == null){
            $token = $_COOKIE['token'];
        }else{
            $token = $token;
        }

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => api_domain_url().$point,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_POSTFIELDS =>'{
                "token": "'.$token.'"
            }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));
        $hasil = curl_exec($curl);
        curl_close($curl);
        $hasil = json_decode($hasil);

        return $hasil;
    }

    function api_read_without_token($point){

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => api_domain_url().$point,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));
        $hasil = curl_exec($curl);
        curl_close($curl);
        $hasil = json_decode($hasil);

        return $hasil;
    }

    function api_update($point, $post_field, $request=null, $content=null){

        ($request == NULL) ? $request = "PUT" : $request = $request ;
        ($content == NULL) ? $content = "application/json" : $content = $content ;
        $auth = ['Authorization: Bearer '.$_COOKIE['token'], 'Content-Type: '.$content];

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => api_domain_url().$point,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $request,
            CURLOPT_POSTFIELDS => $post_field,
            CURLOPT_HTTPHEADER => $auth,
        ));

        $hasil = curl_exec($curl);
        curl_close($curl);
        $hasil = json_decode($hasil);

        return $hasil;
    }

    function api_delete($point, $id){

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => api_domain_url().$point.$id,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'DELETE',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer '.$_COOKIE['token']
            ),
        ));

        $hasil = curl_exec($curl);
        curl_close($curl);
        $hasil = json_decode($hasil);

        return $hasil;
    }

    function api_logout(){

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => api_domain_url().'logout',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_POSTFIELDS =>'{
                "token": "'.$_COOKIE['token'].'"
            }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $hasil = curl_exec($curl);
        curl_close($curl);
        return $hasil = json_decode($hasil);
    }

    function api_get_claims(){
        $split = explode('.', $_COOKIE['token']);
        $base_decode = base64_decode($split[1]);
        $data_decode = json_decode($base_decode);

        return $data_decode;
    }

    function api_get_user_login()
    {
        return api_read('get_user', $_COOKIE['token']);
    }

