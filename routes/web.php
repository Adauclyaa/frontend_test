<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('signin', function () {
    return view('auth.login');
})->name('signin');

Route::get('signup', [AuthController::class, 'signup'])->name('signup');


Route::post('login', [AuthController::class, 'login'])->name('login');
Route::get('logout', [AuthController::class, 'logout'])->name('logout');
Route::get('forgot_password', [AuthController::class, 'forgot_password']);
Route::post('register', [AuthController::class, 'register'])->name('register');

Route::group(['middleware' => ['cek_token']], function() {

    Route::group(['middleware' => 'role:internal'], function() {
        Route::get('admin', [AdminController::class, 'index'])->name('admin');

        Route::group(['prefix' => 'admin'], function () {
            Route::post('list_formulir', [AdminController::class, 'list_formulir'])->name('admin/list_formulir');
            Route::post('add_transaksi', [AdminController::class, 'add_transaksi'])->name('admin/add_transaksi');
            Route::post('update_transaksi/{id}', [AdminController::class, 'update_transaksi'])->name('admin/update_transaksi');
        });

        Route::group(['prefix' => 'master'], function () {
            Route::get('users_admin', [MasterController::class, 'users_admin'])->name('users_admin');
            Route::get('users_tenant', [MasterController::class, 'users_tenant'])->name('users_tenant');
            Route::get('users_tenant', [MasterController::class, 'users_tenant'])->name('users_tenant');

            Route::post('add_admin', [MasterController::class, 'add_admin'])->name('master/add_admin');
            Route::get('edit_admin/{id}', [MasterController::class, 'edit_admin'])->name('master/edit_admin');
            Route::post('delete_admin', [MasterController::class, 'delete_admin'])->name('master/delete_admin');

            Route::post('list_users_admin', [MasterController::class, 'list_users_admin'])->name('list_users_admin');
        });

        Route::get('transaksi/formulir_pemeriksaan', [AdminController::class, 'view_list_formulir'])->name('view_list_formulir');
        Route::get('transaksi/add_formulir_pemeriksaan/{id}', [AdminController::class, 'add_formulir_pemeriksaan'])->name('add_formulir_pemeriksaan');

    });
});

