"use strict";
var KTPasswordResetGeneral=function(){
	var t,e,i;
	return{init:function(){
		t=document.querySelector("#kt_password_reset_form"),
		e=document.querySelector("#kt_password_reset_submit"),
		i=FormValidation.formValidation(t,{fields:{email:{validators:{notEmpty:{message:"Email address is required"},emailAddress:{message:"The value is not a valid email address"}}}},plugins:{trigger:new FormValidation.plugins.Trigger,bootstrap:new FormValidation.plugins.Bootstrap5({rowSelector:".fv-row",eleInvalidClass:"",eleValidClass:""})}}),
		e.addEventListener("click",(function(o){
			o.preventDefault(),
			i.validate().then((function(i){
				if("Valid"==i){
					e.setAttribute("data-kt-indicator","on");
					e.disabled=!0;
					var url = "<?php echo base_url().'Welcome/request_change_password';?>";
				    var formData = new FormData($('#kt_password_reset_form')[0]);

				      $.ajax({
				          url : url,
				          type: "POST",
				          data: formData,
				          contentType: false,
				          processData: false,
				          dataType: "JSON",
				          success: function(data)
				          {
				              console.log(data);
				              var html="";
				              if(data.status){
				                
				              }else{
				                
				              }
				          },
				          error: function (jqXHR, textStatus, errorThrown)
				          {
				              alert('Error adding / update data');
				              $('#btnChangePassword').text('Request Password'); 
				              $('#btnChangePassword').attr('disabled',false); 
				          }
				      });
				}else{
					Swal.fire({text:"Sorry, looks like there are some errors detected, please try again.",icon:"error",buttonsStyling:!1,confirmButtonText:"Ok, got it!",customClass:{confirmButton:"btn btn-primary"}});
				}
			}))}));
	}};
}