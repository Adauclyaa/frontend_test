 $(function() {
    var hash = window.location.hash;
    hash && $('ul.nav a[href="' + hash + '"]').tab('show');

   $('.nav-item a').click(function (e) {
      window.location.hash = this.hash;
    });
    
    var current_doc;
    var document_id;
    var expire_date;
    var vendor_id;
    var has_expire_date

    $(document).on('click','.showfile', function(){
      current_doc = $(this).attr('link');
      document_id = $(this).attr('document-id');
      expire_date = $(this).attr('expire-date');
      vendor_id = $(this).attr('vendor-id');
      has_expire_date = $(this).attr('has-expire-date');
      if(has_expire_date =='yes'){
        $('#box-expire').show();
      }else{
        $('#box-expire').hide();
      }

      $('#modal-file').modal({
            backdrop: 'static',
            keyboard: true, 
            show: true
      });
      $('#file_container').attr('src', current_doc);
      $("#expire-text").text(expire_date);
    });  
    
    $('[data-toggle="popover"]').popover({
        container: 'body',
        title: 'Popover Form',
        html: true,
        placement: 'right',
        sanitize: false,
        content: function() {
          return $('.PopoverContent').html();
        }
    });

    $('body').popover({
        selector: '[data-popover]',
        placement: 'right',
        html: true,
        delay: {show: 50, hide: 400},
        sanitize: false,
        content: function(ele) {
            console.log(ele,this);
            return $(this).next(".popover-content").html();
        }
    });

    $('#bank_reject').popover({
        title: 'Rejection Note',
        selector: '[data-popover]',
        placement: 'right',
        html: true,
        delay: {show: 50, hide: 400},
        sanitize: false,
        content: function() {
            return $(".popover-bank").html();
        }
    });

    $(document).on("click", ".btnexpire", function() {
      var txtexpire = $('.popover #txtexpire').val();
      if(txtexpire.length<1){
        toastr.error("Tanggal tidak boleh kosong", 'Warning', {timeOut: 5000});
      }else{
        $.ajax({
          url: '<?php echo base_url()."admin/set_document_expire_date";?>',
          type: "POST",
          data: JSON.stringify({"expire_date": txtexpire, "document_id":document_id, "vendor_id": vendor_id}),
          contentType: false,
          processData: false,
          dataType: "JSON",
          success: function(data)
          {
            console.log(data);
            if(data.status){
              toastr.success(data.message, 'Success', {timeOut: 5000});
              setTimeout(function(){ 
                location.reload();
              }, 1200);
            }else{
              toastr.error(data.message, 'warning', {timeOut: 5000});
            }
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            alert('Error adding / update data');
          }
        });
      }
      
    });

    $(document).on("click", ".rejectWithNote", function() {
      var id=$(this).attr('id');
      var doc_id=$(this).attr('doc_id');
      var vendor_id=$(this).attr('vendor_id');
      var textarea = $('.popover #textarea_'+id).val();
      var type='rejact';
      if(textarea.length<1){
        toastr.warning("Please write your note", 'Warning', {timeOut: 5000});
      }else{
       $.ajax({
            url: '<?php echo base_url()."admin/document_approval";?>',
            type: "POST",
            data: JSON.stringify({"vendor_id": vendor_id,"type":type, "doc_id":doc_id, "document_id":id, 'note':textarea}),
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function(data)
            {
              if(data.status){
                toastr.success(data.message, 'Success', {timeOut: 5000});
                setTimeout(function(){ 
                  location.reload();
                }, 1200);
              }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              alert('Error adding / update data');
            }
        });
      }
      
    });

    $(document).on("click", ".rejectWithNoteBank", function() {
      var id=$(this).attr('id');
      var bank_id=$(this).attr('bank_id');
      var vendor_id=$(this).attr('vendor_id');
      var textarea = $('.popover #reject_bank_note').val();
      alert(textarea);

      if(textarea.length<1){
        toastr.warning("Please write your note", 'Warning', {timeOut: 5000});
      }else{
       $.ajax({
            url: '<?php echo base_url()."admin/bank_data_approval";?>',
            type: "POST",
            data: JSON.stringify({"vendor_id": vendor_id, "type":'reject', "bank_id":bank_id, 'note':textarea}),
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function(data)
            {
              if(data.status){
                toastr.success(data.message, 'Success', {timeOut: 5000});
                setTimeout(function(){ 
                  location.reload();
                }, 1200);
              }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              alert('Error adding / update data');
            }
        });
      }
      
    });

    $('.bank').on('click',function(){
      var bank_id = $(this).attr('bank_id');
      var vendor_id = $(this).attr('vendor_id');
      var type = $(this).attr('type');

      $('#bank_approve').html('<i class="fas fa-1x fa-sync-alt fa-spin"></i> Loading..');
      $('#bank_approve').attr('disabled',true);
        
      $.ajax({
          url: '<?php echo base_url()."admin/bank_data_approval";?>',
          type: "POST",
          data: JSON.stringify({"vendor_id": vendor_id, "type":type, "bank_id":bank_id}),
          contentType: false,
          processData: false,
          dataType: "JSON",
          success: function(data)
          {
            if(data.status){
              toastr.success(data.message, 'Success', {timeOut: 5000});
              setTimeout(function(){ 
                location.reload();
              }, 1200);
            }
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            alert('Error adding / update data');
          }
      });
      
      return false;   
    });

    $('#procurement_approval').on('click',function(){
      var vendor_id = $(this).attr('vendor-id');
      var status = $(this).attr('status');
      $('#procurement_approval').html('<i class="fas fa-1x fa-sync-alt fa-spin"></i> Loading..');
      $('#procurement_approval').attr('disabled',true);
      if(status=='yes'){
        toastr.error("Already approved", 'Warning', {timeOut: 5000});
        $('#procurement_approval').html('<i class="fas fa-check"></i> Procurement Approval');
        $('#procurement_approval').attr('disabled',true);
      }else{
        $.ajax({
            url: '<?php echo base_url()."admin/procurement_approver";?>',
            type: "POST",
            data: JSON.stringify( { "vendor_id": vendor_id } ),
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function(data)
            {
              if(data.status){
                toastr.success(data.message, 'Success', {timeOut: 5000});
                setTimeout(function(){ 
                  location.reload();
                }, 1000);
              }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              alert('Error adding / update data');
              $('#procurement_approval').html('Procurement Approval');
              $('#procurement_approval').attr('disabled',false);
            }
        });
      }
      return false;
    });

    $('#finance_approval').on('click',function(){
      var vendor_id = $(this).attr('vendor-id');
      var status = $(this).attr('status');
      var approver = $(this).attr('approver');
      if(status=='true'){
        toastr.error("Silahkan approve atau reject semua document terkait dengan finance", 'Warning', {timeOut: 5000});
      }else{
        $('#finance_approval').html('<i class="fas fa-1x fa-sync-alt fa-spin"></i> Loading..');
        $('#finance_approval').attr('disabled',true);
        
        $.ajax({
            url: '<?php echo base_url()."admin/finance_approver";?>',
            type: "POST",
            data: JSON.stringify( { "vendor_id": vendor_id, "approver":approver } ),
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function(data)
            {
              if(data.status){
                toastr.success(data.message, 'Success', {timeOut: 5000});
                //setTimeout(function(){ 
                  //location.reload();
                //}, 1000);
              }else{
                toastr.error(data.message, 'Warning', {timeOut: 5000});
                $('#finance_approval').html('Finance Approval');
                $('#finance_approval').attr('disabled',false);
              }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              alert('Error adding / update data');
              $('#finance_approval').html('Finance Approval');
              $('#finance_approval').attr('disabled',false);
            }
        });
        
        return false;  
      }
      
    });

    $('#legal_approval').on('click',function(){
      var vendor_id = $(this).attr('vendor-id');
      var status = $(this).attr('status');
      var approver = $(this).attr('approver');
      $('#legal_approval').html('<i class="fas fa-1x fa-sync-alt fa-spin"></i> Loading..');
      $('#legal_approval').attr('disabled',true);
      if(status=='true'){
        toastr.error("Silahkan approve atau reject semua document terkait dengan Legal", 'Warning', {timeOut: 5000});
      }else{
        $.ajax({
            url: '<?php echo base_url()."admin/legal_approver";?>',
            type: "POST",
            data: JSON.stringify( { "vendor_id": vendor_id, "approver":approver } ),
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function(data)
            {
              if(data.status){
                toastr.success(data.message, 'Success', {timeOut: 5000});
                setTimeout(function(){ 
                  location.reload();
                }, 1000);
              }else{
                toastr.error(data.message, 'Warning', {timeOut: 5000});
                $('#legal_approval').html('Legal Approval');
                $('#legal_approval').attr('disabled',false);
              }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              alert('Error adding / update data');
              $('#legal_approval').html('Legal Approval');
              $('#legal_approval').attr('disabled',false);
            }
        });
      }
      return false;
    });

    $('#tax_approval').on('click',function(){
      var vendor_id = $(this).attr('vendor-id');
      var status = $(this).attr('status');
      var approver = $(this).attr('approver');
      $('#tax_approval').html('<i class="fas fa-1x fa-sync-alt fa-spin"></i> Loading..');
      $('#tax_approval').attr('disabled',true);
      if(status=='true'){
        toastr.error("Silahkan approve atau reject semua document terkait dengan tax", 'Warning', {timeOut: 5000});
      }else{
        $.ajax({
            url: '<?php echo base_url()."admin/tax_approver";?>',
            type: "POST",
            data: JSON.stringify( { "vendor_id": vendor_id, "approver":approver} ),
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function(data)
            {
              if(data.status){
                toastr.success(data.message, 'Success', {timeOut: 5000});
                setTimeout(function(){ 
                  location.reload();
                }, 1000);
              }else{
                toastr.error(data.message, 'Warning', {timeOut: 5000});
                $('#tax_approval').html('Tax Approval');
                $('#tax_approval').attr('disabled',false);
              }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              alert('Error adding / update data');
              $('#tax_approval').html('Tax Approval');
              $('#tax_approval').attr('disabled',false);
            }
        });
      }
      return false;
    });

    $('.document_approve').on('click',function(){
      var vendor_id = $(this).attr('vendor_id');
      var type = $(this).attr('type');
      var doc_id = $(this).attr('doc_id');
      var document_id= $(this).attr('document_id');

      if(type=='approve'){
        $('#approve_'+doc_id).html('<i class="fas fa-1x fa-sync-alt fa-spin"></i> Loading..');
        $('#approve_'+doc_id).attr('disabled',true);
      }else{
        $('#reject_'+doc_id).html('<i class="fas fa-1x fa-sync-alt fa-spin"></i> Loading..');
        $('#reject_'+doc_id).attr('disabled',true);
      }   

      $.ajax({
          url: '<?php echo base_url()."admin/document_approval";?>',
          type: "POST",
          data: JSON.stringify({"vendor_id": vendor_id,"type":type, "doc_id":doc_id, "document_id":document_id}),
          contentType: false,
          processData: false,
          dataType: "JSON",
          success: function(data)
          {
            if(data.status){
              toastr.success(data.message, 'Success', {timeOut: 5000});
              setTimeout(function(){ 
                location.reload(true);
              }, 1000);
            }
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            alert('Error adding / update data');
          }
      });
      
      return false;
    });

 });