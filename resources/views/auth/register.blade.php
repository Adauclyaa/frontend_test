<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Metronic - the world's #1 selling Bootstrap Admin Theme Ecosystem for HTML, Vue, React, Angular & Laravel by Keenthemes</title>
		<meta charset="utf-8" />
		<meta name="description" content="The most advanced Bootstrap Admin Theme on Themeforest trusted by 100,000 beginners and professionals. Multi-demo, Dark Mode, RTL support and complete React, Angular, Vue, Asp.Net Core, Blazor, Django, Flask & Laravel versions. Grab your copy now and get life-time updates for free." />
		<meta name="keywords" content="metronic, bootstrap, bootstrap 5, angular, VueJs, React, Asp.Net Core, Blazor, Django, Flask & Laravel starter kits, admin themes, web design, figma, web development, free templates, free admin themes, bootstrap theme, bootstrap template, bootstrap dashboard, bootstrap dak mode, bootstrap button, bootstrap datepicker, bootstrap timepicker, fullcalendar, datatables, flaticon" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta property="og:locale" content="en_US" />
		<meta property="og:type" content="article" />
		<meta property="og:title" content="Metronic | Bootstrap HTML, VueJS, React, Angular, Asp.Net Core, Blazor, Django, Flask & Laravel Admin Dashboard Theme" />
		<meta property="og:url" content="https://keenthemes.com/metronic" />
		<meta property="og:site_name" content="Keenthemes | Metronic" />
        <link href="{!! asset('theme/demo1/dist/assets/plugins/global/plugins.bundle.css') !!}" rel="stylesheet" type="text/css" />
        <link href="{!! asset('theme/demo1/dist/assets/css/style.bundle.css') !!}" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
        <link rel="shortcut icon" href="{!! asset('theme/logo/favicon.ico') !!}">
        <style>
            select{
                font-size: 13px !important;
            }
            label{
                font-weight: 500 !important;
                color:#494949;
            }
            .form-select{
                height: 50.75px;
            }

            .select2-selection__placeholder{
                color:#353535 !important;
                font-size: 13px !important;
            }

            .form-select{
                padding: 1.05rem !important;
            }
        </style>
	</head>
	<body id="kt_body" class="app-blank app-blank">
		<script>
            var defaultThemeMode = "light";
            var themeMode;
            if ( document.documentElement ) {
                if ( document.documentElement.hasAttribute("data-theme-mode")) {
                    themeMode = document.documentElement.getAttribute("data-theme-mode");
                } else {
                    if ( localStorage.getItem("data-theme") !== null ) {
                        themeMode = localStorage.getItem("data-theme");
                    } else {
                        themeMode = defaultThemeMode;
                    }
                }

                if (themeMode === "system") {
                    themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light";
                }
                document.documentElement.setAttribute("data-theme", themeMode);
            }
        </script>
		<div class="d-flex flex-column flex-root" id="kt_app_root">
			<div class="d-flex flex-column flex-lg-row flex-column-fluid">
				<div class="d-flex flex-column flex-lg-row-fluid w-lg-50 p-10 order-2 order-lg-1">
					<div class="d-flex flex-center flex-column flex-lg-row-fluid">
						<div class="w-lg-600px p-10">
                            <form class="form w-100" id="form_register">
								<div class="text-center mb-11">
                                    <h1 class="text-dark fw-bolder mb-3">Daftar Akun</h1>
								</div>
                                <div class="row">
                                    <div class="col-lg-7">
                                        <div class="form-floating mb-7">
                                            <input type="text" class="form-control" id="companyname" name="companyname" placeholder="companyname" required/>
                                            <label for="companyname">Nama Perusahaan</label>
                                            <span class="fw-semibold text-danger" id="error_companyname"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-5">
                                        <select class="form-select" id="categoryid" name="categoryid" data-control="select2" data-placeholder="Kategori Perusahaan" data-hide-search="true" required >
                                            <option></option>
                                            @foreach($category as $value)
                                                <option value="{{ $value->id }}">{{ $value->namakategori }}</option>
                                            @endforeach
                                        </select>
                                        <span class="fw-semibold text-danger" id="error_categoryid"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-7">
                                        <div class="form-floating mb-7">
                                            <input type="text" class="form-control" id="storename" name="storename" placeholder="storename" required/>
                                            <label for="storename">Nama Tenant</label>
                                            <span class="fw-semibold text-danger" id="error_storename"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-5">
                                        <select class="form-select" name="siteid" id="siteid" data-control="select2" data-placeholder="Lokasi Tenant" data-hide-search="true" required>
                                            <option></option>
                                            @foreach($site as $value)
                                                <option value="{{ $value->siteid }}">{{ '('.$value->siteid.') '.$value->sitename }}</option>
                                            @endforeach
                                        </select>
                                        <span class="fw-semibold text-danger" id="error_siteid"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-7">
                                        <div class="form-floating mb-7">
                                            <input type="text" class="form-control" id="name" name="name" placeholder="name" required/>
                                            <label for="name">PIC Name</label>
                                            <span class="fw-semibold text-danger" id="error_name"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-5">
                                        <select class="form-select" id="position" name="position" data-control="select2" data-placeholder="Pilih Jabatan" data-hide-search="true" required>
                                            <option></option>
                                            <option value="finance">Finance</option>
                                            <option value="accounting">Accounting</option>
                                            <option value="legal">Legal</option>
                                            <option value="tax">Tax</option>
                                            <option value="other">Other</option>
                                        </select>
                                        <span class="fw-semibold text-danger" id="error_position"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-7">
                                        <div class="form-floating mb-7">
                                            <input type="email" class="form-control" id="email" name="email" placeholder="email" required/>
                                            <label for="email">Email</label>
                                            <span class="fw-semibold text-danger" id="error_email"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-5">
                                        <div class="form-floating mb-7">
                                            <input type="number" class="form-control" id="phone" name="phone" placeholder="phone" required/>
                                            <label for="phone">Phone Number</label>
                                        </div>
                                        <span class="fw-semibold text-danger" id="error_phone"></span>
                                    </div>
                                </div>
								<div class="fv-row mb-8" data-kt-password-meter="true">
									<div class="mb-1">
										<div class="position-relative mb-3">
											<input class="form-control bg-transparent" type="password" placeholder="Password" name="password" id="password" autocomplete="off" required/>
											<span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2" data-kt-password-meter-control="visibility">
												<i class="bi bi-eye-slash fs-2"></i>
												<i class="bi bi-eye fs-2 d-none"></i>
											</span>
										</div>
										<div class="d-flex align-items-center mb-3" data-kt-password-meter-control="highlight">
											<div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
											<div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
											<div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
											<div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px"></div>
										</div>
									</div>
									<div class="text-muted">Gunakan 8 karakter atau lebih baik dengan campuran huruf, angka & simbol.</div>
                                    <span class="fw-semibold text-danger" id="error_password"></span>
								</div>
								<div class="d-grid mb-10">
									<button type="submit" id="button_register" class="btn btn-primary">
										<span class="indicator-label">Daftar Akun</span>
										<span class="indicator-progress">Please wait...
										<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
									</button>
								</div>
								<div class="text-gray-500 text-center fw-semibold fs-6">Sudah Memiliki Akun?
								<a href="{{ route('signin') }}" class="link-primary fw-semibold">Masuk Sistem</a></div>
							</form>
						</div>
					</div>
				</div>
				<div class="d-flex flex-lg-row-fluid w-lg-50 bgi-size-cover bgi-position-center order-1 order-lg-2" style="background-image: url({!! asset('theme/demo1/dist/assets/media/misc/auth-bg.png') !!})">
                    <div class="d-flex flex-column flex-center py-7 py-lg-15 px-5 px-md-15 w-100">
                        <a href="../../demo1/dist/index.html" class="mb-0 mb-lg-12">
                            <img alt="Logo" src="{!! asset('theme/img/New Project.png') !!}" class="h-80px h-lg-95px" />
                        </a>
                        <img class="d-none d-lg-block mx-auto w-275px w-md-50 w-xl-500px mb-12 mb-lg-20" src="{!! asset('theme/img/stand2.svg') !!}" alt="" />
                        <h1 class="d-none d-lg-block text-white fs-6 fw-bolder text-center">Copyright  &copy; 2022 IT Software Pakuwon Group</h1>
                    </div>
                </div>
			</div>
		</div>

        <script src="{!! asset('theme/demo1/dist/assets/plugins/global/plugins.bundle.js') !!}"></script>
        <script src="{!! asset('theme/demo1/dist/assets/js/scripts.bundle.js') !!}"></script>
        <script src="{!! asset('theme/demo1/dist/assets/js/custom/authentication/sign-in/general.js') !!}"></script>
	</body>
</html>
<script>
    $(function() {
        $("#form_register").submit(function(){

            $('#button_register').text('Prosessing...');
            $('#button_register').attr('disabled',true);

            var csrf = "{{ csrf_token() }}";
            var url = "{{ route('register') }}";

            $.ajax({
                url : url,
                type: "POST",
                data: $(this).serialize(),
                dataType: "JSON",
                headers: {
                    'X-CSRF-TOKEN': csrf
                },
                success: function(data)
                {
                    if(data.status) {
                        Swal.fire({
                            title: 'Success',
                            html: `${data.message}`,
                            icon: 'success',
                            type: 'success'
                        }).then(function() {
                            location.href = "{{ route('signin') }}"
                        });
                    }else{
                        Swal.fire({
                            title: 'Error',
                            html: `${data.message}`,
                            icon: 'error',
                            type: 'error'
                        });
                    }

                    $('#button_register').text('Sign Up');
                    $('#button_register').attr('disabled',false);

                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error adding / update data');
                    $('#button_register').text('Sign Up');
                    $('#button_register').attr('disabled',false);
                }
            });
            return false;
        });
    });
</script>
