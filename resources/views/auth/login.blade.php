{{-- @extends('layouts.app') --}}

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>DINKES LHOKSEUMAWE</title>
    <meta name="author" content="IT Pakuwon">
    <meta name="description" content="Pakuwon Tenant Management">
    <meta name="keywords" content="Pakuwon, Tenant Management">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="{!! asset('theme/demo1/dist/assets/plugins/global/plugins.bundle.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! asset('theme/demo1/dist/assets/css/style.bundle.css') !!}" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link rel="shortcut icon" href="{!! asset('theme/logo/favicon.ico') !!}">

    <style>
        #overlay {
            box-sizing: border-box;
            position: fixed;
            display: flex;
            flex-flow: column nowrap;
            align-items: center;
            justify-content: center;
            background: rgba(0, 0, 0, 0.8);
            top: 0px;
            left: 0px;
            width: 100%;
            height: 100%;
            z-index: 2147483647;
            opacity: 1;
        }

        /* Center the loader */
        #loader {
            border: 12px solid #f3f3f3;
            border-radius: 50%;
            border-top: 12px solid #009ef7;
            width: 80px;
            height: 80px;
            -webkit-animation: spin 2s linear infinite; /* Safari */
            animation: spin 2s linear infinite;
        }

        /* Safari */
        @-webkit-keyframes spin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }

        /* Extra small devices (portrait phones, less than 576px) */
        @media (max-width: 575.98px) {
            .toolbar-account{
                display: none !important;
            }
        }

        /* Small devices (landscape phones, 576px and up) */
        @media (min-width: 576px) and (max-width: 767.98px) {
            .toolbar-account{
                display: none !important;
            }
        }

        /* Medium devices (tablets, 768px and up) */
        @media (min-width: 768px) and (max-width: 991.98px) {
            .toolbar-account{
                display: none !important;
            }
        }

        /* Large devices (desktops, 992px and up) */
        @media (min-width: 992px) and (max-width: 1199.98px) {

        }

        /* Extra large devices (large desktops, 1200px and up) */
        @media (min-width: 1200px) {

        }
    </style>
    <style>
        .loading:after {
            content: ' .';
            animation: dots 1.7s steps(5, end) infinite;}

        @keyframes dots {
        0%, 20% {
            color: rgba(0,0,0,0);
            text-shadow:
            .25em 0 0 rgba(0,0,0,0),
            .5em 0 0 rgba(0,0,0,0);}
        40% {
            color: white;
            text-shadow:
            .25em 0 0 rgba(0,0,0,0),
            .5em 0 0 rgba(0,0,0,0);}
        60% {
            text-shadow:
            .25em 0 0 white,
            .5em 0 0 rgba(0,0,0,0);}
        80%, 100% {
            text-shadow:
            .25em 0 0 white,
            .5em 0 0 white;}}

    </style>
</head>
<div id="overlay" class="d-none">
    <div id="loader"></div>
    <span class="text-white mt-2" style="font-size:16px;">Memuat Halaman <span class="loading m-0 p-0" style="font-size:40px;"></span></span>
</div>
<body id="kt_body" class="app-blank app-blank">
	<script>var defaultThemeMode = "light";
		var themeMode;
		if ( document.documentElement ) {
		if ( document.documentElement.hasAttribute("data-theme-mode")) {
			themeMode = document.documentElement.getAttribute("data-theme-mode");
		} else {
			if ( localStorage.getItem("data-theme") !== null ) {
				themeMode = localStorage.getItem("data-theme");
			} else { themeMode = defaultThemeMode; }
		}

		if (themeMode === "system") {
			themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light";
		}
		document.documentElement.setAttribute("data-theme", themeMode);
	}
	</script>

	<div class="d-flex flex-column flex-root" id="kt_app_root">
		<div class="d-flex flex-column flex-lg-row flex-column-fluid">
			<div class="d-flex flex-column flex-lg-row-fluid w-lg-50 p-10 order-2 order-lg-1">
				<div class="d-flex flex-center flex-column flex-lg-row-fluid">
					<div class="w-lg-500px p-10">
						<h1 class="fs-2qx fw-bolder text-center mb-12" style="color:#800000;">SILABKES</h1>
						<form class="form_login" id="form_login">
							<div class="text-center mb-11 mt-11">
								<h1 class="text-dark fw-bolder mb-3">Masuk ke Sistem</h1>
								<div class="text-gray-500 fw-semibold fs-6">Masuk untuk Mengelola Akun Anda.</div>
							</div>
							<div class="fv-row mb-8">
								<input type="text" placeholder="Email" name="email" id="email" autocomplete="off" class="form-control bg-transparent" />
                                <span class="fw-semibold text-danger" id="error_email"></span>
							</div>
							<div class="fv-row input-group mb-5">
                                <div class="input-group">
                                    <input type="password" placeholder="Password" name="password" id="password" autocomplete="off" class="form-control bg-transparent" />
                                    <span class="input-group-text" id="basic-addon2">
                                        <a onclick="show_password()" id="show_password" style="cursor: pointer;">
                                            <i class="fas fa-eye fs-4"></i>
                                        </a>
                                    </span>
                                </div>
                                <span class="fw-semibold text-danger" id="error_password"></span>
							</div>
							<div class="d-flex flex-stack flex-wrap gap-3 fs-base fw-semibold mb-8">
								<div></div>
                                <a href="../../demo1/dist/authentication/layouts/corporate/reset-password.html" class="link-primary">Lupa Password ?</a>
							</div>
							<div class="d-grid mb-10">
								<button type="submit" id="btnLogin" class="btn btn-primary">
									<span class="indicator-label">Masuk</span>
									<span class="indicator-progress">Please wait...
									<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
								</button>
							</div>
						</form>
					</div>
				</div>

			</div>
			<div class="d-flex flex-lg-row-fluid w-lg-50 bgi-size-cover bgi-position-center order-1 order-lg-2" style="background-image: url({!! asset('theme/demo1/dist/assets/media/misc/auth-bg.png') !!})">
				<div class="d-flex flex-column flex-center py-7 py-lg-15 px-5 px-md-15 w-100">
                    <br><br><br><br>
					<img class="d-none d-lg-block mx-auto w-275px w-md-50 w-xl-500px mb-12 mb-lg-20" src="{!! asset('theme/logo/medicine.svg') !!}" alt="" />
					<h1 class="d-none d-lg-block text-white fs-6 fw-bolder text-center">Copyright  &copy; 2023 Muhammad Syam Firdaus</h1>
				</div>
			</div>
		</div>
	</div>
	</body>
	<script src="{!! asset('theme/demo1/dist/assets/plugins/global/plugins.bundle.js') !!}"></script>
	<script src="{!! asset('theme/demo1/dist/assets/js/scripts.bundle.js') !!}"></script>
	<script src="{!! asset('theme/demo1/dist/assets/js/custom/authentication/sign-in/general.js') !!}"></script>
	<script type="text/javascript">
		$(function() {
			$("#form_login").submit(function(){
                if(validate() == false){ return false }

                $('#btnLogin').text('Prosessing...');
                $('#btnLogin').attr('disabled',true);

                var csrf = "{{ csrf_token() }}";
                var url = "{{ route('login') }}";

                $.ajax({
                    url : url,
                    type: "POST",
                    data: $(this).serialize(),
                    dataType: "JSON",
                    headers: {
                        'X-CSRF-TOKEN': csrf
                    },
                    beforeSend: function() {
                        $("#overlay").removeClass('d-none');
                    },
                    success: function(data)
                    {
                        if(data.status) {
                            localStorage.setItem('token', data.token);
                            localStorage.setItem('name', data.name);
                            localStorage.setItem('email', data.email);
                            location.href = data.redirect;
                        }else{
                            $("#overlay").addClass('d-none');
                            Swal.fire({
                                title: 'Error',
                                html: `${data.message}`,
                                icon: 'error',
                                type: 'error'
                            });
                        }

                        $('#btnLogin').text('Sign In');
                        $('#btnLogin').attr('disabled',false);

                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        $("#overlay").addClass('d-none');
                        alert('Error adding / update data');
                        $('#btnLogin').text('Sign In');
                        $('#btnLogin').attr('disabled',false);
                    }
                });
                return false;
			});
		});

		function show_password(){
			let input = document.getElementById("password");
			let icon = document.getElementById("show_password");
			if(input.type === "password"){
				input.type = "text";
				icon.innerHTML = `
					<i class="fas fa-eye-slash fs-4"></i>
				`;
			}else{
				input.type = "password";
				icon.innerHTML = `
					<i class="fas fa-eye fs-4"></i>
				`;
			}
		}

        // function setCookie(name,value,days) {
        //     var expires = "";
        //     if (days) {
        //         var date = new Date();
        //         date.setTime(date.getTime() + (days*24*60*60*1000));
        //         expires = "; expires=" + date.toUTCString();
        //     }
        //     document.cookie = name + "=" + (value || "")  + expires + "; path=/";
        // }

        function validate() {
            let email = $('#email').val();
            let password = $('#password').val();

            if(email == ''){
                $('#error_email').html('Email must be filled out')
                if(password != ''){
                    $('#error_password').html('')
                }
                return false
            }else if(password == ''){
                $('#error_password').html('Password must be filled out')
                if(email != ''){
                    $('#error_email').html('')
                }
                return false
            }else{
                return true
            }
        }

	</script>
</html>
