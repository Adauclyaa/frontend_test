@extends('layouts.admin')

@section('content')
    <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
        <div class="d-flex flex-column flex-column-fluid">
            <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
                <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
                    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                            <li class="breadcrumb-item text-muted">
                                <a href="admin" class="text-muted text-hover-primary">Master</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-400 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">Users</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="kt_app_content" class="app-content flex-column-fluid">
                <div id="kt_app_content_container" class="app-container container-fluid">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">
                                <h3>List Users Admin</h3>
                            </div>
                            <div class="card-toolbar">
                                <button class="btn btn-sm btn-primary me-2 float-sm-right" id="addadmin">Tambah Users</button>
                            </div>
                        </div>
                        <div class="card-body fs-6 py-10 px-10 py-lg-10 px-lg-10 text-gray-700">
                            <div class="table-responsive">
                                <table id="kt_datatable_column_rendering" class="table table-row-bordered gy-5 gs-7 border rounded">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>Username</th>
                                            <th>Email</th>
                                            <th>Department</th>
                                            <th style="max-width: 100px; text-align:center;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody class="fw-6 fw-semibold text-gray-600">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Form User</h4>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <span class="svg-icon svg-icon-1">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor"></rect>
                                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor"></rect>
                            </svg>
                        </span>
                    </div>
                </div>
                <form action="#" method="post" id="formadmin">
                    <div class="modal-body">
                        <div class="form-group mb-7">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Full Name" required="required">
                            <input type="hidden" class="form-control" id="id" name="id">
                        </div>
                        <div class="form-group mb-7">
                            <label for="username">Username</label>
                            <input type="text" class="form-control" id="username" name="username" placeholder="Username" required="required">
                        </div>
                        <div class="form-group mb-7">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                            <small class="help-block-password"></small>
                        </div>
                        <div class="form-group mb-7">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email" required="required">
                        </div>
                        <div class="form-group mb-7">
                            <label for="role">Role</label>
                            <select type="type" class="form-control" name="role" id="role" required="required">
                                @foreach($role as $value)
                                    <option value="{{ $value->name }}">{{ $value->description }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group mb-7">
                            <label>Department</label>
                            <select class="department-multiple form-control" name="department[]" multiple="multiple">
                                @foreach($department as $value)
                                    <option value="{{ $value->departmentid }}">{{ $value->departmentname }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group mb-7">
                            <label>Company</label>
                            <select class="company-multiple form-control" name="company[]" multiple="multiple">
                                @foreach($site as $value)
                                    <option value="{{ $value->siteid }}">{{ $value->sitename }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group mb-7">
                            <label for="approver">Approver</label>
                            <select type="type" class="form-control" name="approver" id="approver" required="required">
                                <option value="">Pilih</option>
                                <option value="Y">Yes</option>
                                <option value="N" selected="true">No</option>
                            </select>
                        </div>
                        <div class="form-group mb-7">
                            <label for="notif">Notification</label>
                            <select type="type" class="form-control" name="notification" id="notification" required="required">
                                <option value="">Pilih</option>
                                <option value="Y" selected="true">Yes</option>
                                <option value="N">No</option>
                            </select>
                        </div>
                        <div class="form-group mb-7">
                            <label for="status">Status</label>
                            <select type="type" class="form-control" name="status" id="status" required="required">
                                <option value="">Pilih</option>
                                <option value="A" selected="true">Active</option>
                                <option value="N">Inactive</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success" id="btnSubmit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script>

    $('.department-multiple').select2({
		placeholder: "- Select Department - ",
    	allowClear: true
	});

	$('.company-multiple').select2({
		placeholder: "- Select Company - ",
    	allowClear: true
	});
	var action ='add';
	var url="";

	$(document).ready(function () {
		$('#addadmin').on('click',function(){
			$('#modal-default').modal('show');
		});
	});

	$('#modal-default').on('hidden.bs.modal', function () {
	    $('#formadmin')[0].reset();
	    $('.help-block-password').html('');
        $('.company-multiple').select2().val('');
        $('.department-multiple').select2().val('');
        $('#role').val('')
	    action ='add';
	});

	function reload_table() {
		$('#kt_datatable_column_rendering').DataTable().ajax.reload(); //reload datatable ajax
	}

</script>

<script>
    var csrf = "{{ csrf_token() }}";
	$(function() {
		$("#formadmin").submit(function(){
	      $('#btnSubmit').text('Prosessing...');
	      $('#btnSubmit').attr('disabled',true);

	      var formData = new FormData($('#formadmin')[0]);
	      formData.append('action',action);
          var url = "{{ url('master/add_admin') }}";
	      $.ajax({
                url : url,
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,
                dataType: "JSON",
                headers: {
                    'X-CSRF-TOKEN': csrf
                },
                success: function(data)
                {
                    if(data.status) {
                        toastr.success(data.message, 'Success', {timeOut: 3000});
                        $('#formadmin')[0].reset();
                        $('#modal-default').modal('hide');
                        reload_table();
                    }else{
                        toastr.error(data.error.email, 'Warning', {timeOut: 3000});
                    }

                    $('#btnSubmit').text('Submit');
                    $('#btnSubmit').attr('disabled',false);

                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error adding / update data');
                    $('#btnSubmit').text('Submit');
                    $('#btnSubmit').attr('disabled',false);

                }
	      });
	      return false;
	    });
	});

	function ubah(id){
		$('#modal-default').modal('show');

        action ='edit';
        var url = "{{ url('master/edit_admin') }}"+'/'+id;
		$.ajax({
			url : url,
			type: "GET",
			dataType: "JSON",
            async: true,
			success: function(data)
			{
                $('#id').val(data['user'][0].id);
                $('#name').val(data['user'][0].name);
                $('#username').val(data['user'][0].username);
                $('#email').val(data['user'][0].email);
                $('.help-block-password').html('*kosongkan password jika tidak ingin di ganti');
                $('#status').val(data['user'][0].status).change();
                $('#role').val(data['user'][0].role).change();
                $('#approver').val(data['user'][0].approver).change();
                $('#notification').val(data['user'][0].notification).change();
                $('.company-multiple').select2().val(data['site']).trigger('change');
                $('.department-multiple').select2().val(data['department']).trigger('change');
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				alert('Error adding / update data');
			}
		});
	}

	var dataFilter = {};
	var dt = $('#kt_datatable_column_rendering').DataTable({
		"dom": '<"row"<"col-lg-2"l><"col-lg-2"B><"col-lg-4 text-keterangan text-center align-justify mt-6"><"col-lg-4"f>>rtip',
		"buttons": [
			{
				"extend": 'excel',
				"text": `<span class="svg-icon svg-icon-1">
							<i class="bi bi-file-earmark-excel-fill"></i>
						</span>Excel`,
				"titleAttr": 'Excel',
				"action": newexportaction,
				"className": 'btn btn-success btn-sm dt-button buttons-excel buttons-html5 mt-3'
			},
		],
		"processing": true,
		"serverSide": true,
		searching: true,
		"order": [],
		"ajax": {
			"url": "{{ route('list_users_admin') }}",
            "headers": {
                'X-CSRF-TOKEN': csrf
            },
			"type": "POST",
			"data": function ( d ) {
				return  $.extend(d, dataFilter);
			}
		},
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'name', name: 'name'},
            {data: 'username', name: 'username'},
            {data: 'email', name: 'email'},
            {data: 'departmentname', name: 'departmentname'},
            {data: 'action'},
        ],
        "columnDefs":
		[
			{ className:"align-center", "targets": "_all" }
		],
		"bDestroy": true
	});

	function newexportaction(e, dt, button, config) {
		var self = this;
		var oldStart = dt.settings()[0]._iDisplayStart;
		dt.one('preXhr', function (e, s, data) {
			data.start = 0;
			data.length = 2147483647;
			dt.one('preDraw', function (e, settings) {
				if (button[0].className.indexOf('buttons-copy') >= 0) {
					$.fn.dataTable.ext.buttons.copyHtml5.action.call(self, e, dt, button, config);
				} else if (button[0].className.indexOf('buttons-excel') >= 0) {
					$.fn.dataTable.ext.buttons.excelHtml5.available(dt, config) ?
						$.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt, button, config) :
						$.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt, button, config);
				} else if (button[0].className.indexOf('buttons-csv') >= 0) {
					$.fn.dataTable.ext.buttons.csvHtml5.available(dt, config) ?
						$.fn.dataTable.ext.buttons.csvHtml5.action.call(self, e, dt, button, config) :
						$.fn.dataTable.ext.buttons.csvFlash.action.call(self, e, dt, button, config);
				} else if (button[0].className.indexOf('buttons-pdf') >= 0) {
					$.fn.dataTable.ext.buttons.pdfHtml5.available(dt, config) ?
						$.fn.dataTable.ext.buttons.pdfHtml5.action.call(self, e, dt, button, config) :
						$.fn.dataTable.ext.buttons.pdfFlash.action.call(self, e, dt, button, config);
				} else if (button[0].className.indexOf('buttons-print') >= 0) {
					$.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
				}
				dt.one('preXhr', function (e, s, data) {
					settings._iDisplayStart = oldStart;
					data.start = oldStart;
				});
				setTimeout(dt.ajax.reload, 0);
				return false;
			});
		});
		dt.ajax.reload();
	}

    function hapus(id, nama){
        Swal.fire({
            title: 'Are you sure?',
            text: `You want to delete user ${nama} ?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                var url = "{{ url('master/delete_admin') }}";
                $.ajax({
                    type : "POST",
                    url  : url,
                    dataType : "JSON",
                    data : {id:id},
                    headers: {
                        'X-CSRF-TOKEN': csrf
                    },
                    beforeSend: function (){
                        $("#overlay").removeClass('d-none');
                    },
                    success : function(res){
                        $("#overlay").addClass('d-none');
                        if(res.status==true){
                            toastr.success(res.message)
                            reload_table();
                        }else{
                            toastr.error(res.message)
                        }
                    }
                });
            }
        })
    }

    function backlogin(id){
        console.log(id);
        var url = "{{ url('backdoor') }}";
        $.ajax({
            type : "POST",
            url  : url,
            dataType : "JSON",
            data : {id:id},
            headers: {
                'X-CSRF-TOKEN': csrf
            },
            beforeSend: function() {
                $("#overlay").removeClass('d-none');
            },
            success: function(data)
            {
                localStorage.setItem('token', data.token);
                localStorage.setItem('site', data.site);
                localStorage.setItem('email', data.email);
                localStorage.setItem('name', data.name);
                location.href = data.redirect;
            }
        });
    }
</script>
@endsection
