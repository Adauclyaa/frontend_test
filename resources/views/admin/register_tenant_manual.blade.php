@extends('layouts.admin')

@section('content')
    <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
        <div class="d-flex flex-column flex-column-fluid">
            <div id="kt_app_toolbar" class="app-toolbar py-2 py-lg-4">
                <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
                    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ $title }}</h1>
                    </div>
                    <div class="d-flex align-items-center gap-2 gap-lg-3">
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('admin') }}" class="text-muted text-hover-primary">Administrasi</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-400 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">Register Tenant</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="kt_app_content" class="app-content flex-column-fluid">
                <div id="kt_app_content_container" class="app-container container-fluid">
                    <div class="card">
                        <div class="card-body fs-6 py-10 px-10 py-lg-7 px-lg-7 text-gray-700">
                            <div class="row mb-10">
                                <div class="col-md-5">
									<label class="d-flex flex-stack mb-5 cursor-pointer">
										<span class="d-flex align-items-center me-2">
											<span class="symbol symbol-50px me-6">
												<span class="symbol-label bg-light-info">
													<span class="svg-icon svg-icon-1 svg-icon-info">
														<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="currentColor" opacity="0.3"/>
                                                            <path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="currentColor"/>
														</svg>
													</span>
												</span>
											</span>
											<span class="d-flex flex-column">
												<span class="fw-bold fs-6">Existing Tenant</span>
												<span class="fs-7 text-muted">Untuk mendaftarkan tenant yang sudah buka</span>
											</span>
										</span>
										<span class="form-check form-check-custom form-check-solid">
											<input class="form-check-input" type="radio" name="tenantstatus" value="1" checked/>
										</span>
									</label>
                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-5">
									<label class="d-flex flex-stack mb-5 cursor-pointer">
										<span class="d-flex align-items-center me-2">
											<span class="symbol symbol-50px me-6">
												<span class="symbol-label bg-light-primary">
													<span class="svg-icon svg-icon-1 svg-icon-primary">
														<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="currentColor" fill-rule="nonzero" opacity="0.3"/>
                                                            <path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="currentColor" fill-rule="nonzero"/>
														</svg>
													</span>
												</span>
											</span>
											<span class="d-flex flex-column">
												<span class="fw-bold fs-6">New Tenant</span>
												<span class="fs-7 text-muted">Untuk mendaftarkan tenant yang akan buka</span>
											</span>
										</span>
										<span class="form-check form-check-custom form-check-solid">
											<input class="form-check-input" type="radio" name="tenantstatus" value="2" />
										</span>
									</label>
								</div>
                            </div>
                            <div class="row mb-10" id="p1">
                                <div class="col-lg-3">
                                    <div class="stepper stepper-pills stepper-column d-flex flex-column flex-xl-row flex-row-fluid" id="kt_modal_create_app_stepper">
                                        <div class="d-flex justify-content-left justify-content-xl-start flex-row-auto w-100 w-xl-300px">
                                            <div class="stepper-nav">
                                                <div class="stepper-item current" data-kt-stepper-element="nav">
                                                    <div class="stepper-wrapper">
                                                        <div class="stepper-icon w-40px h-40px">
                                                            <i class="stepper-check fas fa-check"></i>
                                                            <span class="stepper-number">1</span>
                                                        </div>
                                                        <div class="stepper-label">
                                                            <h3 class="stepper-title">Step 1</h3>
                                                            <div class="stepper-desc">Basic Information</div>
                                                        </div>
                                                    </div>
                                                    <div class="stepper-line h-40px"></div>
                                                </div>
                                                <div class="stepper-item" data-kt-stepper-element="nav">
                                                    <div class="stepper-wrapper">
                                                        <div class="stepper-icon w-40px h-40px">
                                                            <i class="stepper-check fas fa-check"></i>
                                                            <span class="stepper-number">2</span>
                                                        </div>
                                                        <div class="stepper-label">
                                                            <h3 class="stepper-title">Step 2</h3>
                                                            <div class="stepper-desc">Attachment</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <form action="{{ url('admin/tenant_save_p1') }}" method="post">
                                    {!! csrf_field() !!}
                                        <div class="row g-9 mb-8">
                                            <div class="col-md-12">
                                                <div class="card text-primary bg-light-primary" id="copydata1">
                                                    <div class="card-header">
                                                        <p class="d-flex flex-stack cursor-pointer" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                                            <span class="d-flex align-items-center">
                                                                <b>Apabila Terdapat Tenant yang sama maka dapat melakukan pengambilan data untuk mengurangi waktu pengisian</b>
                                                            </span>
                                                        </p>
                                                    </div>
                                                    <div class="card-body p-0" id="copydata2">
                                                        <div class="collapse" id="collapseExample">
                                                            <div class="row p-5">
                                                                <div class="col-md-6 fv-row">
                                                                    <div class="ui-widget">
                                                                        <label class=" fs-6 fw-semibold mb-2" for="companyinfo">Nama Perusahaan</label>
                                                                        <input class="form-select" id="companyinfo">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 fv-row">
                                                                    <label class=" fs-6 fw-semibold mb-2">Pilih Tenant</label>
                                                                    <select class="form-select" data-hide-search="false" data-placeholder="-- Pilih Tenant --" name="target_assign" id="tenantinfo" onchange="copydata();">
                                                                        <option value="">-- Pilih Tenant --</option>
                                                                    </select>
                                                                </div>
                                                                <input type="text" value="false" id="flagname" name="flagname" hidden>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 fv-row">
                                                <label class="required fs-6 fw-semibold mb-2">Pilih Jenis Tenant</label>
                                                <select class="form-select" data-hide-search="true" data-placeholder="-- Jenis Tenant --" name="tenantjenis" id="tenant_jenis" required>
                                                    <option value="">-- Jenis Tenant --</option>
                                                    @foreach($tenantcategory as $tenantcategory1)
                                                        <option value="{{$tenantcategory1->id}}">{{$tenantcategory1->namakategori}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6 fv-row">
                                                <label class="required fs-6 fw-semibold mb-2">Nama Perusahaan</label>
                                                <input class="form-control" placeholder="Nama Perusahaan" name="tenantcompanyname" id="tenant_company_name" required/>
                                                <label for="tenant_company_name" style="color:red;font-size:12px;" id="alert">Nama Perusahaan terdeteksi sudah ada di dalam database, silahkan pergunakan fitur pengambilan data</label>
                                            </div>
                                            <div class="col-md-6 fv-row">
                                                <label class="required fs-6 fw-semibold mb-2">Nama Tenant</label>
                                                <input class="form-control" placeholder="Nama Tenant" name="tenantname" id="tenant_name" required/>
                                            </div>
                                            <div class="col-md-6 fv-row">
                                                <label class="required fs-6 fw-semibold mb-2">Lokasi Tenant</label>
                                                <select class="form-select" data-hide-search="true" data-placeholder="-- Lokasi Tenant --" name="tenantlocation" id="tenant_location" required>
                                                    <option value="">-- Lokasi Tenant --</option>
                                                    @foreach($site as $site1)
                                                        <option value="{{$site1->siteid}}">{{$site1->sitename}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6 fv-row">
                                                <label class="required fs-6 fw-semibold mb-2">Alamat</label>
                                                <input class="form-control" placeholder="Alamat" name="tenantaddress" id="tenant_address" required/>
                                            </div>
                                            <div class="col-md-6 fv-row">
                                                <label class="required fs-6 fw-semibold mb-2">Provinsi</label>
                                                <select class="form-select" data-hide-search="true" data-placeholder="-- Provinsi --" name="tenantlocationprovinsi" id="tenant_location_provinsi" onchange="Provinsi();" required>
                                                    <option value="">-- Provinsi --</option>
                                                    @foreach($provinsi as $provinsi1)
                                                        <option value="{{$provinsi1->provinsi}}">{{$provinsi1->provinsi}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6 fv-row">
                                                <label class="required fs-6 fw-semibold mb-2">Kabupaten</label>
                                                <select class="form-select" data-hide-search="true" data-placeholder="-- Kabupaten --" name="tenantlocationkabupaten" id="tenant_location_kabupaten" onchange="Kabupaten();" required>
                                                    <option value="">-- Kabupaten --</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6 fv-row">
                                                <label class="required fs-6 fw-semibold mb-2">Kecamatan</label>
                                                <select class="form-select" data-hide-search="true" data-placeholder="-- Kecamatan --" name="tenantlocationkecamatan" id="tenant_location_kecamatan" onchange="Kecamatan();" required>
                                                    <option value="">-- Kecamatan --</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6 fv-row">
                                                <label class="required fs-6 fw-semibold mb-2">Kelurahan</label>
                                                <select class="form-select" data-hide-search="true" data-placeholder="-- Kelurahan --" name="tenantlocationkelurahan" id="tenant_location_kelurahan" onchange="Kelurahan();" required>
                                                    <option value="">-- Kelurahan --</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6 fv-row">
                                                <label class="required fs-6 fw-semibold mb-2">Kode Pos</label>
                                                <input class="form-control" placeholder="12345" name="tenantlocationkodepos" id="tenant_location_kodepos" required/>
                                            </div>
                                            <div class="col-md-6 fv-row">
                                                <label class="required fs-6 fw-semibold mb-2">No. Unit</label>
                                                <input class="form-control" placeholder="No. Unit (Ex: G-01)" name="tenantunitid" id="tenant_unit_id" required/>
                                            </div>
                                            <div class="col-md-6 fv-row">
                                                <label class="required fs-6 fw-semibold mb-2">Tipe Bisnis</label>
                                                <select class="form-select" data-hide-search="false" data-placeholder="-- Tipe Bisnis --" name="tenantbussinesstype" id="tenant_bussiness_type" required>
                                                <option value="">-- Tipe Bisnis --</option>
                                                    @foreach($storetype as $storetype1)
                                                        <option value="{{$storetype1->id}}">{{$storetype1->nama}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6 fv-row">
                                                <label class="required fs-6 fw-semibold mb-2">Deskripsi Bisnis</label>
                                                <input class="form-control" placeholder="Deskripsi Bisnis" name="tenantbussinessdesc" id="tenant_bussiness_desc" required/>
                                            </div>
                                            <div class="col-md-6 fv-row">
                                                <label class="required fs-6 fw-semibold mb-2">Kontak Perusahaan</label>
                                                <input type="number" class="form-control" placeholder="Kontak Perusahaan" name="tenantbussinesscontact" id="tenant_bussiness_contact" required/>
                                            </div>
                                            <div class="col-md-6 fv-row">
                                                <label class="required fs-6 fw-semibold mb-2">Email Bisnis</label>
                                                <input class="form-control" placeholder="Email Bisnis" name="tenantbussinessmail" id="tenant_bussiness_mail" required/>
                                            </div>
                                            <div class="col-md-6 fv-row">
                                                <label class="required fs-6 fw-semibold mb-2">Luas Tenant(m2)</label>
                                                <input class="form-control" placeholder="Luas Tenant (Ex: 20,9)" name="tenantsize" id="tenant_size" required/>
                                            </div>
                                            <div class="col-md-6 fv-row">
                                                <label class="required fs-6 fw-semibold mb-2">Catatan</label>
                                                <input class="form-control" placeholder="Catatan" name="tenantnote" id="tenant_note" required/>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="notice d-flex bg-light-info rounded border-info border border-solid p-6">
                                                    <span class="svg-icon svg-icon-2tx svg-icon-info me-4">
                                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="currentColor" />
                                                            <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="currentColor" />
                                                            <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="currentColor" />
                                                        </svg>
                                                    </span>
                                                    <div class="d-flex flex-stack flex-grow-1">
                                                        <div class="fw-semibold">
                                                            <h4 class="text-gray-900 fw-bold">Perhatian !</h4>
                                                            <div class="fs-6 text-gray-700">Pada saat step 2 (File Attachment), jika document Tenant belum terisi semua, jangan menekan tombol submit, cukup buka kembali halaman registrasi manual saja dengan menekan menekan menu Register Tenant Manual pada menu yang tersedia.
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <button type="submit" id="btnSubmit1" class="btn btn-primary float-end mt-10">Lanjut</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script>
    var csrf = "{{ csrf_token() }}";
    $( function() {
        $( "#companyinfo" ).autocomplete({
            source: "{{url('admin/company_tenant')}}/?",
            select: function( event, ui ) {
                $.ajax({
                    url: "{{ url('admin/tenant_list') }}",
                    type: "GET",
                    data: {compinfo: ui['item']['value']},
                    headers: {
                        'X-CSRF-TOKEN': csrf
                    },
                    beforeSend: function(){
                        $('.tinfo').remove();
                        document.getElementById("copydata1").classList.add("overlay")
                        document.getElementById("copydata1").classList.add("overlay-block")
                        document.getElementById("copydata2").classList.add("overlay-wrapper")
                        document.getElementById("companyinfo").classList.add("overlay")
                        document.getElementById("companyinfo").classList.add("overlay-block")
                        document.getElementById("companyinfo").disabled = true
                        document.getElementById("tenantinfo").classList.add("overlay")
                        document.getElementById("tenantinfo").classList.add("overlay-block")
                        document.getElementById("tenantinfo").disabled = true
                        
                    },
                    success: function (datareturn)
                    {
                        for(var i = 0;i < datareturn.length;i++) {
                            $('#tenantinfo').append('<option value="'+datareturn[i]['id']+'" class="tinfo">'+datareturn[i]['storename']+'</option>')
                        }
                        document.getElementById("copydata1").classList.remove("overlay")
                        document.getElementById("copydata2").classList.remove("overlay-wrapper")
                        document.getElementById("companyinfo").classList.remove("overlay")
                        document.getElementById("tenantinfo").classList.remove("overlay")
                        document.getElementById("copydata1").classList.remove("overlay-block")
                        document.getElementById("companyinfo").classList.remove("overlay-block")
                        document.getElementById("companyinfo").disabled = false
                        document.getElementById("tenantinfo").classList.remove("overlay-block")
                        document.getElementById("tenantinfo").disabled = false
                    }
                });
            }
        });
    });
    function copydata(){
        var id = document.getElementById("tenantinfo").value
        $.ajax({
            url: "{{ url('admin/tenant_data') }}",
            type: "GET",
            data: {id: id},
            headers: {
                'X-CSRF-TOKEN': csrf
            },
            beforeSend: function(){
                document.getElementById("copydata1").classList.add("overlay")
                document.getElementById("copydata1").classList.add("overlay-block")
                document.getElementById("copydata2").classList.add("overlay-wrapper")
                document.getElementById("companyinfo").classList.add("overlay")
                document.getElementById("companyinfo").classList.add("overlay-block")
                document.getElementById("companyinfo").disabled = true
                document.getElementById("tenantinfo").classList.add("overlay")
                document.getElementById("tenantinfo").classList.add("overlay-block")
                document.getElementById("tenantinfo").disabled = true
            },
            success: function (datareturn)
            {
                const $select = document.querySelector('#tenant_jenis')
                $select.value = datareturn[0]['categoryid'].toString()
                document.getElementById("tenant_company_name").value = datareturn[0]['companyname']
                document.getElementById("tenant_name").value = datareturn[0]['storename']
                document.getElementById("tenant_location").value = datareturn[0]['siteid'].toString()
                document.getElementById("tenant_address").value = datareturn[0]['address']
                document.getElementById("tenant_unit_id").value = datareturn[0]['unit']
                document.getElementById("tenant_bussiness_type").value =  datareturn[0]['businesstype'].toString()
                document.getElementById("tenant_bussiness_desc").value = datareturn[0]['description']
                document.getElementById("tenant_bussiness_contact").value = datareturn[0]['phone']
                document.getElementById("tenant_bussiness_mail").value = datareturn[0]['email']
                document.getElementById("tenant_size").value = datareturn[0]['luas']
                document.getElementById("tenant_note").value = datareturn[0]['note']
                
                document.getElementById("tenant_location_provinsi").value = datareturn[0]['provinsi']

                ProvinsiLoad(datareturn[0]['provinsi'],datareturn[0]['kabupaten']);
                KabupatenLoad(datareturn[0]['kabupaten'],datareturn[0]['kecamatan']);
                KecamatanLoad(datareturn[0]['kecamatan'],datareturn[0]['kelurahan']);

                document.getElementById("tenant_location_kodepos").value = datareturn[0]['kodepos']

                document.getElementById("copydata1").classList.remove("overlay")
                document.getElementById("copydata2").classList.remove("overlay-wrapper")
                document.getElementById("companyinfo").classList.remove("overlay")
                document.getElementById("tenantinfo").classList.remove("overlay")
                document.getElementById("copydata1").classList.remove("overlay-block")
                document.getElementById("companyinfo").classList.remove("overlay-block")
                document.getElementById("companyinfo").disabled = false
                document.getElementById("tenantinfo").classList.remove("overlay-block")
                document.getElementById("tenantinfo").disabled = false

                document.getElementById("flagname").value="false";
                document.getElementById("alert").style.display = 'none';
            }
        });
    }
    window.onload = onload()
    function onload(){
        var error = <?php echo isset($error) ? json_encode($error):0; ?>;
        if(error != ''){
            // alert(error)
            toastr.error(error, 'Warning', {timeOut: 15000});
        }
        document.getElementById("alert").style.display = 'none';
    }

    var typingTimer;                //timer identifier
    var doneTypingInterval = 1000;  //time in ms, 5 seconds for example
    var $input = $('#tenant_company_name');

    //on keyup, start the countdown
    $input.on('keyup', function () {
    clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });

    //on keydown, clear the countdown 
    $input.on('keydown', function () {
        clearTimeout(typingTimer);
    });

    //user is "finished typing," do something
    function doneTyping () {
        $.ajax({
            url: "{{ url('admin/company_tenant_check') }}",
            type: "GET",
            data: {name: document.getElementById("tenant_company_name").value},
            headers: {
                'X-CSRF-TOKEN': csrf
            },
            beforeSend: function(){                        
            },
            success: function (datareturn)
            {
                if(datareturn == 'true'){
                    document.getElementById("alert").style.display = 'block';
                    document.getElementById("btnSubmit1").disabled  = true;
                    document.getElementById("flagname").value="true";
                }else{
                    document.getElementById("alert").style.display = 'none';
                    document.getElementById("btnSubmit1").disabled  = false;
                    document.getElementById("flagname").value="false";
                }
            }
        });
    }

    function Provinsi(){
        $.ajax({
            url: "{{ url('admin/tenant_kabupaten') }}",
            type: "GET",
            data: {provinsi: document.getElementById("tenant_location_provinsi").value},
            headers: {
                'X-CSRF-TOKEN': csrf
            },
            beforeSend: function(){    
                $('.kabsel').remove();
                document.getElementById("tenant_location_provinsi").classList.add("overlay")
                document.getElementById("tenant_location_provinsi").classList.add("overlay-block")
                document.getElementById("tenant_location_provinsi").disabled = true
                
                document.getElementById("tenant_location_kabupaten").classList.add("overlay")
                document.getElementById("tenant_location_kabupaten").classList.add("overlay-block")
                document.getElementById("tenant_location_kabupaten").disabled = true 
            },
            success: function (datareturn)
            {
                for(var i = 0;i < datareturn.length;i++){
                    $("#tenant_location_kabupaten").append('<option value="'+datareturn[i]['kabupaten']+'" class="kabsel">'+datareturn[i]['kabupaten']+'</option>');
                }
                document.getElementById("tenant_location_provinsi").classList.remove("overlay")
                document.getElementById("tenant_location_provinsi").classList.remove("overlay-block")
                document.getElementById("tenant_location_provinsi").disabled = false 
                
                document.getElementById("tenant_location_kabupaten").classList.remove("overlay")
                document.getElementById("tenant_location_kabupaten").classList.remove("overlay-block")
                document.getElementById("tenant_location_kabupaten").disabled = false 
            }
        });
    }
    function Kabupaten(){
        $.ajax({
            url: "{{ url('admin/tenant_kecamatan') }}",
            type: "GET",
            data: {kabupaten: document.getElementById("tenant_location_kabupaten").value},
            headers: {
                'X-CSRF-TOKEN': csrf
            },
            beforeSend: function(){    
                $('.kecsel').remove();
                document.getElementById("tenant_location_kabupaten").classList.add("overlay")
                document.getElementById("tenant_location_kabupaten").classList.add("overlay-block")
                
                document.getElementById("tenant_location_kecamatan").classList.add("overlay")
                document.getElementById("tenant_location_kecamatan").classList.add("overlay-block")
                
                document.getElementById("tenant_location_kabupaten").disabled = true
                document.getElementById("tenant_location_kecamatan").disabled = true
            },
            success: function (datareturn)
            {
                for(var i = 0;i < datareturn.length;i++){
                    $("#tenant_location_kecamatan").append('<option value="'+datareturn[i]['kecamatan']+'" class="kecsel">'+datareturn[i]['kecamatan']+'</option>');
                }

                document.getElementById("tenant_location_kabupaten").classList.remove("overlay")
                document.getElementById("tenant_location_kabupaten").classList.remove("overlay-block")
                
                document.getElementById("tenant_location_kecamatan").classList.remove("overlay")
                document.getElementById("tenant_location_kecamatan").classList.remove("overlay-block")

                document.getElementById("tenant_location_kabupaten").disabled = false
                document.getElementById("tenant_location_kecamatan").disabled = false
            }
        });
    }
    function Kecamatan(){
        $.ajax({
            url: "{{ url('admin/tenant_kelurahan') }}",
            type: "GET",
            data: {kecamatan: document.getElementById("tenant_location_kecamatan").value},
            headers: {
                'X-CSRF-TOKEN': csrf
            },
            beforeSend: function(){    
                $('.kelsel').remove();
                document.getElementById("tenant_location_kecamatan").classList.add("overlay")
                document.getElementById("tenant_location_kecamatan").classList.add("overlay-block")
                
                document.getElementById("tenant_location_kelurahan").classList.add("overlay")
                document.getElementById("tenant_location_kelurahan").classList.add("overlay-block")
                
                document.getElementById("tenant_location_kelurahan").disabled = true
                document.getElementById("tenant_location_kecamatan").disabled = true
            },
            success: function (datareturn)
            {
                for(var i = 0;i < datareturn.length;i++){
                    $("#tenant_location_kelurahan").append('<option value="'+datareturn[i]['kelurahan']+'" class="kelsel">'+datareturn[i]['kelurahan']+'</option>');
                }
                document.getElementById("tenant_location_kecamatan").classList.remove("overlay")
                document.getElementById("tenant_location_kecamatan").classList.remove("overlay-block")
                
                document.getElementById("tenant_location_kelurahan").classList.remove("overlay")
                document.getElementById("tenant_location_kelurahan").classList.remove("overlay-block")

                document.getElementById("tenant_location_kelurahan").disabled = false
                document.getElementById("tenant_location_kecamatan").disabled = false
            }
        });
    }
    function Kelurahan(){
        $.ajax({
            url: "{{ url('admin/tenant_kodepos') }}",
            type: "GET",
            data: {kelurahan: document.getElementById("tenant_location_kelurahan").value},
            headers: {
                'X-CSRF-TOKEN': csrf
            },
            beforeSend: function(){
                document.getElementById("tenant_location_kodepos").classList.add("overlay")
                document.getElementById("tenant_location_kodepos").classList.add("overlay-block")
                
                document.getElementById("tenant_location_kelurahan").classList.add("overlay")
                document.getElementById("tenant_location_kelurahan").classList.add("overlay-block")
                
                document.getElementById("tenant_location_kelurahan").disabled = true
                document.getElementById("tenant_location_kodepos").disabled = true
            },
            success: function (datareturn)
            {
                document.getElementById("tenant_location_kodepos").value = datareturn[0]['kodepos']
                document.getElementById("tenant_location_kodepos").classList.remove("overlay")
                document.getElementById("tenant_location_kodepos").classList.remove("overlay-block")
                
                document.getElementById("tenant_location_kelurahan").classList.remove("overlay")
                document.getElementById("tenant_location_kelurahan").classList.remove("overlay-block")

                document.getElementById("tenant_location_kelurahan").disabled = false
                document.getElementById("tenant_location_kodepos").disabled = false
            }
        });
    }

    function ProvinsiLoad(val,val2){
        $.ajax({
            url: "{{ url('admin/tenant_kabupaten') }}",
            type: "GET",
            data: {provinsi: val},
            headers: {
                'X-CSRF-TOKEN': csrf
            },
            beforeSend: function(){    
                $('.kabsel').remove();
                document.getElementById("tenant_location_kabupaten").disabled = true
                document.getElementById("tenant_location_provinsi").disabled = true

                document.getElementById("tenant_location_provinsi").classList.add("overlay")
                document.getElementById("tenant_location_provinsi").classList.add("overlay-block")
                
                document.getElementById("tenant_location_kabupaten").classList.add("overlay")
                document.getElementById("tenant_location_kabupaten").classList.add("overlay-block")
                
            },
            success: function (datareturn)
            {
                for(var i = 0;i < datareturn.length;i++){
                    if(datareturn[i]['kabupaten'].toLowerCase() == val2.toLowerCase()){
                        $("#tenant_location_kabupaten").append('<option value="'+datareturn[i]['kabupaten']+'" class="kabsel" selected>'+datareturn[i]['kabupaten']+'</option>');
                    }else{
                        $("#tenant_location_kabupaten").append('<option value="'+datareturn[i]['kabupaten']+'" class="kabsel">'+datareturn[i]['kabupaten']+'</option>');
                    }
                }

                document.getElementById("tenant_location_provinsi").classList.remove("overlay")
                document.getElementById("tenant_location_provinsi").classList.remove("overlay-block")
                
                document.getElementById("tenant_location_kabupaten").classList.remove("overlay")
                document.getElementById("tenant_location_kabupaten").classList.remove("overlay-block")

                document.getElementById("tenant_location_kabupaten").disabled = false
                document.getElementById("tenant_location_provinsi").disabled = false
                
            }
        });
    }
    function KabupatenLoad(val,val2){
        $.ajax({
            url: "{{ url('admin/tenant_kecamatan') }}",
            type: "GET",
            data: {kabupaten: val},
            headers: {
                'X-CSRF-TOKEN': csrf
            },
            beforeSend: function(){    
                $('.kecsel').remove();
                document.getElementById("tenant_location_kabupaten").disabled = true
                document.getElementById("tenant_location_kecamatan").disabled = true

                document.getElementById("tenant_location_kabupaten").classList.add("overlay")
                document.getElementById("tenant_location_kabupaten").classList.add("overlay-block")
                
                document.getElementById("tenant_location_kecamatan").classList.add("overlay")
                document.getElementById("tenant_location_kecamatan").classList.add("overlay-block")                
            },
            success: function (datareturn)
            {
                for(var i = 0;i < datareturn.length;i++){
                    if(datareturn[i]['kecamatan'].toLowerCase() == val2.toLowerCase()){
                        $("#tenant_location_kecamatan").append('<option value="'+datareturn[i]['kecamatan']+'" class="kecsel" selected>'+datareturn[i]['kecamatan']+'</option>');
                    }else{
                        $("#tenant_location_kecamatan").append('<option value="'+datareturn[i]['kecamatan']+'" class="kecsel">'+datareturn[i]['kecamatan']+'</option>');
                    }
                }

                document.getElementById("tenant_location_kabupaten").classList.remove("overlay")
                document.getElementById("tenant_location_kabupaten").classList.remove("overlay-block")
                
                document.getElementById("tenant_location_kecamatan").classList.remove("overlay")
                document.getElementById("tenant_location_kecamatan").classList.remove("overlay-block")

                document.getElementById("tenant_location_kabupaten").disabled = false
                document.getElementById("tenant_location_kecamatan").disabled = false
            }
        });
    }
    function KecamatanLoad(val,val2){
        $.ajax({
            url: "{{ url('admin/tenant_kelurahan') }}",
            type: "GET",
            data: {kecamatan: val},
            headers: {
                'X-CSRF-TOKEN': csrf
            },
            beforeSend: function(){    
                $('.kelsel').remove();
                document.getElementById("tenant_location_kelurahan").disabled = true
                document.getElementById("tenant_location_kecamatan").disabled = true

                document.getElementById("tenant_location_kecamatan").classList.add("overlay")
                document.getElementById("tenant_location_kecamatan").classList.add("overlay-block")
                
                document.getElementById("tenant_location_kelurahan").classList.add("overlay")
                document.getElementById("tenant_location_kelurahan").classList.add("overlay-block")
            },
            success: function (datareturn)
            {
                for(var i = 0;i < datareturn.length;i++){
                    if(datareturn[i]['kelurahan'].toLowerCase() == val2.toLowerCase()){
                        $("#tenant_location_kelurahan").append('<option value="'+datareturn[i]['kelurahan']+'" class="kelsel" selected>'+datareturn[i]['kelurahan']+'</option>');
                    }else{
                        $("#tenant_location_kelurahan").append('<option value="'+datareturn[i]['kelurahan']+'" class="kelsel">'+datareturn[i]['kelurahan']+'</option>');
                    }
                }

                document.getElementById("tenant_location_kecamatan").classList.remove("overlay")
                document.getElementById("tenant_location_kecamatan").classList.remove("overlay-block")
                
                document.getElementById("tenant_location_kelurahan").classList.remove("overlay")
                document.getElementById("tenant_location_kelurahan").classList.remove("overlay-block")

                document.getElementById("tenant_location_kelurahan").disabled = false
                document.getElementById("tenant_location_kecamatan").disabled = false
            }
        });
    }
</script>
@endsection
