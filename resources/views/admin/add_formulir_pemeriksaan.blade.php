@extends('layouts.admin')

@section('content')
    <style>
        table, tr, td{
            border:1px solid gray !important;
        }
    </style>
    <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
        <div class="d-flex flex-column flex-column-fluid">
            <div id="kt_app_toolbar" class="app-toolbar py-2 py-lg-4">
                <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
                    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ $title }}</h1>
                    </div>
                    <div class="d-flex align-items-center gap-2 gap-lg-3">
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('admin') }}" class="text-muted text-hover-primary">Administrasi</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-400 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">Tenant</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="kt_app_content" class="app-content flex-column-fluid">
                <div id="kt_app_content_container" class="app-container container-fluid">
                    <div class="card">
                        <form action="#" method="POST" id="add_transaksi">
                            <div class="card-body fs-6 py-10 px-10 py-lg-10 px-lg-10 text-gray-700">
                                <table class="table table-row-bordered table-striped gy-5 gs-7 border rounded" width="100%" style="font-size:12px !important;">
                                    <tr>
                                        <td>PEMERINTAH KOTA LHOKSEUMAWE</td>
                                        <td>FORMULIR PEMERIKSAAN SAMPEL</td>
                                        <td>NOMOR REGISTRASI: {{ $transaksi[0]->nomor_registrasi }}</td>
                                    </tr>
                                    <tr>
                                        <td>NAMA CUSTOMER</td>
                                        <td colspan="2"><input type="text" name="nama" id="nama" class="form-control"></td>
                                    </tr>
                                    <tr>
                                        <td>ALAMAT CUSTOMER</td>
                                        <td colspan="2"><input type="text" name="alamat" id="alamat" class="form-control"></td>
                                    </tr>
                                    <tr>
                                        <td>TANGGAL REGISTRASI</td>
                                        <td colspan="2"><input type="date" name="tanggal" id="tanggal" class="form-control"></td>
                                    </tr>
                                    <tr>
                                        <td>OPERATOR PENERIMA</td>
                                        <td colspan="2"><input type="text" name="operator" id="operator" class="form-control" disabled="disabled"></td>
                                    </tr>
                                </table>
                                <div class="row">
                                    <div class="col-lg-12">
                                        1. PEMERIKSAAN SAMPEL AIR
                                    </div>
                                </div>
                                <table class="table table-row-bordered table-striped gy-5 gs-7 border rounded" width="100%" style="font-size:12px !important;">
                                    <thead>
                                        <tr>
                                            <th>NO</th>
                                            <th>PARAMETER</th>
                                            <th>QTY</th>
                                            <th>TARIF</th>
                                            <th>JUMLAH</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($mastersampel as $key => $value)
                                            <input type="hidden" name="id_sampel[]" value="{{$value->id}}">
                                            <input type="hidden" name="tarif_sampel[]" value="{{$value->tarif}}">
                                            <tr>
                                                <td>{{ $key+1 }}</td>
                                                <td>{{ $value->parameter }}</td>
                                                <td><input type="number" name="qty_sampel[]" id="qty_sampel" class="form-control"></td>
                                                <td>Rp. {{ number_format($value->tarif,2,',','.') }}</td>
                                                <td></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="row">
                                    <div class="col-lg-12">
                                        2. PEMERIKSAAN SAMPEL MAKANAN / MINUMAN / SWAB
                                    </div>
                                </div>
                                <table class="table table-row-bordered table-striped gy-5 gs-7 border rounded" width="100%" style="font-size:12px !important;">
                                    <thead>
                                        <tr>
                                            <th>NO</th>
                                            <th>PARAMETER</th>
                                            <th>QTY</th>
                                            <th>TARIF</th>
                                            <th>JUMLAH</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($masterdiagnosa as $key => $value)
                                            <input type="hidden" name="id_diagnosa[]" value="{{$value->id}}">
                                            <input type="hidden" name="tarif_diagnosa[]" value="{{$value->tarif}}">
                                            <tr>
                                                <td>{{ $key+1 }}</td>
                                                <td>{{ $value->diagnosa }}</td>
                                                <td><input type="number" name="qty_diagnosa[]" id="qty_diagnosa" class="form-control"></td>
                                                <td>Rp. {{ number_format($value->tarif,2,',','.') }}</td>
                                                <td></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="row mt-10">
                                    <div class="col-lg-12">
                                        <button class="btn btn-sm btn-primary me-2 float-end" id="savechange">Simpan Perubahan</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        var csrf = "{{ csrf_token() }}";
        var id_transaksi = "{{$transaksi[0]->id}}";
        $(document).ready(function () {
            $('#btn_modal_add').on('click',function(){
                $('#quickForm')[0].reset(); // reset form on modals
                $('#modal-default').modal({
                    backdrop: 'static',
                    keyboard: true,
                    show: true
                });
            });
        });

        $(document).ready(function(){
            $("#add_transaksi").submit(function(e){
                e.preventDefault();

                var url = `{{ url('admin/update_transaksi') }}/${id_transaksi}`;
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: $(this).serialize(),
                    dataType: "JSON",
                    headers: {
                        'X-CSRF-TOKEN': csrf
                    },
                    success: function(data) {
                        if(data.status){
                            $('#file-input').val("");
                            $('#input_text').val("");
                            $('#span_label').text("");
                            $('i.remove').hide();

                            toastr.success(data.message, 'Success', {timeOut: 5000});
                            $('#send_message').html('Send <i class="fas fa-paper-plane" style="margin-left:7px;"></i>');
                            $('#send_message').attr('disabled',false);

                            location.href="{{ url('transaksi/formulir_pemeriksaan') }}";

                        }else{
                            Swal.fire({
                                title: 'Error',
                                html: data.message,
                                icon: 'error',
                                type: 'error'
                            });

                            $('#send_message').html('Send <i class="fas fa-paper-plane" style="margin-left:7px;"></i>');
                            $('#send_message').attr('disabled',false);
                        }
                    }
                });
            });
        });
    </script>
@endsection
