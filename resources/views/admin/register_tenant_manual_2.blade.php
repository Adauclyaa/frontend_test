@extends('layouts.admin')

@section('content')
<style>
    .box__dragndrop,
    .box__uploading,
    .box__success,
    .box__error {
        display: none;
    }
</style>
    <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
        <div class="d-flex flex-column flex-column-fluid">
            <div id="kt_app_toolbar" class="app-toolbar py-2 py-lg-4">
                <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
                    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ $title }}</h1>
                    </div>
                    <div class="d-flex align-items-center gap-2 gap-lg-3">
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('admin') }}" class="text-muted text-hover-primary">Administrasi</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-400 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">Register Tenant</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="kt_app_content" class="app-content flex-column-fluid">
                <div id="kt_app_content_container" class="app-container container-fluid">
                    <div class="card">
                        <div class="card-body fs-6 py-10 px-10 py-lg-7 px-lg-7 text-gray-700">
                            <div class="row mb-10">
                                <div class="col-md-5">
									<label class="d-flex flex-stack mb-5 cursor-pointer">
										<span class="d-flex align-items-center me-2">
											<span class="symbol symbol-50px me-6">
												<span class="symbol-label bg-light-info">
													<span class="svg-icon svg-icon-1 svg-icon-info">
														<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="currentColor" opacity="0.3"/>
                                                            <path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="currentColor"/>
														</svg>
													</span>
												</span>
											</span>
											<span class="d-flex flex-column">
												<span class="fw-bold fs-6">Existing Tenant</span>
												<span class="fs-7 text-muted">Untuk mendaftarkan tenant yang sudah buka</span>
											</span>
										</span>
										<span class="form-check form-check-custom form-check-solid">
											<input class="form-check-input" type="radio" name="tenantstatus" value="1" checked/>
										</span>
									</label>
                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-5">
									<label class="d-flex flex-stack mb-5 cursor-pointer">
										<span class="d-flex align-items-center me-2">
											<span class="symbol symbol-50px me-6">
												<span class="symbol-label bg-light-primary">
													<span class="svg-icon svg-icon-1 svg-icon-primary">
														<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="currentColor" fill-rule="nonzero" opacity="0.3"/>
                                                            <path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="currentColor" fill-rule="nonzero"/>
														</svg>
													</span>
												</span>
											</span>
											<span class="d-flex flex-column">
												<span class="fw-bold fs-6">New Tenant</span>
												<span class="fs-7 text-muted">Untuk mendaftarkan tenant yang akan buka</span>
											</span>
										</span>
										<span class="form-check form-check-custom form-check-solid">
											<input class="form-check-input" type="radio" name="tenantstatus" value="2" />
										</span>
									</label>
								</div>
                            </div>
                            <div class="row mb-10" id="p1">
                                <div class="col-lg-3">
                                    <div class="stepper stepper-pills stepper-column d-flex flex-column flex-xl-row flex-row-fluid" id="kt_modal_create_app_stepper">
                                        <div class="d-flex justify-content-left justify-content-xl-start flex-row-auto w-100 w-xl-300px">
                                            <div class="stepper-nav">
                                                <div class="stepper-item" data-kt-stepper-element="nav">
                                                    <div class="stepper-wrapper">
                                                        <div class="stepper-icon w-40px h-40px">
                                                            <i class="stepper-check fas fa-check"></i>
                                                            <span class="stepper-number">1</span>
                                                        </div>
                                                        <div class="stepper-label">
                                                            <h3 class="stepper-title">Step 1</h3>
                                                            <div class="stepper-desc">Basic Information</div>
                                                        </div>
                                                    </div>
                                                    <div class="stepper-line h-40px"></div>
                                                </div>
                                                <div class="stepper-item current" data-kt-stepper-element="nav">
                                                    <div class="stepper-wrapper">
                                                        <div class="stepper-icon w-40px h-40px">
                                                            <i class="stepper-check fas fa-check"></i>
                                                            <span class="stepper-number">2</span>
                                                        </div>
                                                        <div class="stepper-label">
                                                            <h3 class="stepper-title">Step 2</h3>
                                                            <div class="stepper-desc">Attachment</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <form id="form_upload_doc" name="form_upload_doc" role="form" enctype="multipart/form-data">
                                        <div class="row g-9 mb-8">
                                            @if($flag)
                                            <div class="col-md-12">
                                                <div class="card text-primary bg-light-primary p-7">
                                                    <div class="card-body p-0 row">
                                                        <div class="col-md-6 fv-row">
                                                            <div class="form-check form-check-custom form-check-solid">
                                                                Pilih Dokumen yang ingin di Copy
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 fv-row">
                                                            <div class="form-check form-check-custom form-check-solid">
                                                                <input class="form-check-input" type="checkbox" value="1" name="ktpcheck" id="ktpcheck1"/>
                                                                <label class="form-check-label" for="flexCheckDefault">
                                                                    KTP
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 fv-row">
                                                            <label class="form-check form-check-custom form-check-solid">
                                                                <input class="form-check-input" type="checkbox" value="1" name="npwpcheck" id="npwpcheck1"/>
                                                                <span class="form-check-label">
                                                                    NPWP
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="text" name="idSearch" value="{{$idSearch}}" hidden>
                                            @endif
                                            <div class="col-md-12">
                                                <div class="card w-100">
                                                    <div class="card-body">
                                                        <div class="row">
                                                        @foreach($doclist as $list)
                                                            <div class="col-md-6">
                                                                <div class="card">
                                                                    <div class="card-body">
                                                                        <div class="row mx-2 form-group">
                                                                            <label for="file" class="col-lg-12 col-md-12" id="label_add_document">{{$list->documentname}}</label>
                                                                            <div class="col-lg-12 col-md-12" id="hide">
                                                                                <input type="file" name="file[]" id="file[]" class="dropify" data-height="160" data-max-file-size="15M" data-allowed-file-extensions="pdf" accept=".pdf">
                                                                            </div>
                                                                        </div>
                                                                        <input type="text" value="{{$list->documentid}}" name="docid[]" hidden>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                        </div>
                                                    </div>
                                                    <div class="card-footer">
                                                        <input type="text" value="{{$id}}" name="tenantid" hidden>
                                                        <input type="submit" name="submit" id="card_submit_btn1" value="Save Document" class="btn btn-success px-3">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <a href="{{ url('admin/register_tenant_manual') }}" id="btnSubmit1" class="btn btn-primary float-end mt-10" onclick="submit();">Lanjut</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script>
    var ktp = false;
    var npwp = false;
    var csrf = "{{ csrf_token() }}";
    function copydata(){
        alert('success')
    }
    $("#form_upload_doc").submit(function(e){
        e.preventDefault();
        $('#card_submit_btn1').text('Prosessing...');
        $('#card_submit_btn1').attr('disabled',true);
        var url = "{{ url('admin/tenant_ktp') }}";
        var formData = new FormData($("#form_upload_doc")[0]);
        $.ajax({
            url : url,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
            headers: {
                'X-CSRF-TOKEN': csrf
            },
            success: function(data)
            {
                if(data.status){
                    toastr.success(data.message, 'Success', {timeOut: 15000});
                    $('#card_submit_btn1').text('Save Document');
                    $('#card_submit_btn1').attr('disabled',false);
                    reload()
                }else{
                    toastr.error(data.message, 'Warning', {timeOut: 5000});
                    $('#card_submit_btn1').text('Save Document');
                    $('#card_submit_btn1').attr('disabled',false);
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                toastr.error('No data updated', 'Warning', {timeOut: 5000});
                $('#card_submit_btn1').text('Save Document');
                $('#card_submit_btn1').attr('disabled',false);
            }
        });
        return false;
	});
    function submit(){
        toastr.success('Success', 'Success', {timeOut: 15000});
    }
    $(document).ready(function(){
        $('.dropify').dropify({
            messages: {
                default: '<div style="font-size:12px; font-weight:bold;">Drag and drop or click to select document</div>',
                replace: 'Ganti',
                remove:  'Hapus',
                error:   'error'
            }
        });
    });
</script>
@endsection
