@extends('layouts.admin')

@section('content')
    <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
        <div class="d-flex flex-column flex-column-fluid">
            <div id="kt_app_toolbar" class="app-toolbar py-2 py-lg-4">
                <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
                    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ $title }}</h1>
                    </div>
                    <div class="d-flex align-items-center gap-2 gap-lg-3">
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('admin') }}" class="text-muted text-hover-primary">Administrasi</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-400 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">Tenant</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="kt_app_content" class="app-content flex-column-fluid">
                <div id="kt_app_content_container" class="app-container container-fluid">
                    <div class="card">
                        <div class="card-body fs-6 py-10 px-10 py-lg-10 px-lg-10 text-gray-700">
                            <div class="row mb-10">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="filtercategory"><i class="fas fa-filter"></i> Tenant Category</label>
                                        <select class="form-control" name="filtercategory" id="filtercategory">
                                            <option value="">All Category</option>
                                            @foreach($category as $value)
                                                <option value="{{ $value->id }}">{{ $value->namakategori }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row table-responsive">
                                <div class="d-flex flex-stack">
                                    <div class="d-flex justify-content-end align-items-center d-none" data-kt-docs-table-toolbar="selected">
                                        <div class="fw-bold me-5">
                                            <span class="me-2" data-kt-docs-table-select="selected_count"></span> Selected
                                        </div>
                                        <button type="button" class="btn btn-danger" data-bs-toggle="tooltip" title="Coming Soon">
                                            Selection Action
                                        </button>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                    <table id="data_store" class="table table-row-bordered table-striped gy-5 gs-7 border rounded" width="100%" style="font-size:12px !important;">
                                        <thead>
                                        <tr>
                                            <th>Store Name</th>
                                            <th>Company Name</th>
                                            <th>Category</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th style="width: 70px">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        var csrf = "{{ csrf_token() }}";
        $(document).ready(function () {
            $('#btn_modal_add').on('click',function(){
                $('#quickForm')[0].reset(); // reset form on modals
                $('#modal-default').modal({
                    backdrop: 'static',
                    keyboard: true,
                    show: true
                });
            });

            var dataFilter ={};
            dataFilter.jenis = 'finish';
            var table = $('#data_store').DataTable({
                "dom": '<"row"<"col-lg-2"l><"col-lg-2"B><"col-lg-4 text-keterangan text-center align-justify mt-6"><"col-lg-4"f>>rtip',
                "buttons": [
                    {
                        "extend": 'excel',
                        "text": `<span class="svg-icon svg-icon-1">
                                    <i class="bi bi-file-earmark-excel-fill"></i>
                                </span>Excel`,
                        "titleAttr": 'Excel',
                        "action": newexportaction,
                        "className": 'btn btn-success btn-sm dt-button buttons-excel buttons-html5 mt-3'
                    },
                ],
                "processing": true,
                "serverSide": true,
                searching: true,
                "order": [],
                "ajax": {
                    "url": "{{ route('admin/tenant_list_finish') }}",
                    "type": "POST",
                    "headers": {
                        'X-CSRF-TOKEN': csrf
                    },
                    "data": function ( d ) {
                        return  $.extend(d, dataFilter);
                    }
                },
                "columnDefs":
                [
                    { "targets": [0, -1], "orderable": false, "autoWidth": true},
                    { className:"align-middle", "targets": "_all" }
                ],
                columns: [
                    {data: 'storename', name: 'storename'},
                    {data: 'companyname', name: 'companyname'},
                    {data: 'namakategori', name: 'namakategori'},
                    {data: 'email', name: 'email'},
                    {data: 'phone', name: 'phone'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ],
                "bDestroy": true
            });

            $("#ExportReporttoExcel").on("click", function() {
                dt.button( '.buttons-excel' ).trigger();
            });

            $('#filtercategory').on( 'change', function () {
                dataFilter.filtercategory = $(this).val();
                table.ajax.reload();
            });

            function newexportaction(e, dt, button, config) {
                var self = this;
                var oldStart = dt.settings()[0]._iDisplayStart;
                dt.one('preXhr', function (e, s, data) {
                    data.start = 0;
                    data.length = 2147483647;
                    dt.one('preDraw', function (e, settings) {
                        if (button[0].className.indexOf('buttons-copy') >= 0) {
                            $.fn.dataTable.ext.buttons.copyHtml5.action.call(self, e, dt, button, config);
                        } else if (button[0].className.indexOf('buttons-excel') >= 0) {
                            $.fn.dataTable.ext.buttons.excelHtml5.available(dt, config) ?
                                $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt, button, config) :
                                $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt, button, config);
                        } else if (button[0].className.indexOf('buttons-csv') >= 0) {
                            $.fn.dataTable.ext.buttons.csvHtml5.available(dt, config) ?
                                $.fn.dataTable.ext.buttons.csvHtml5.action.call(self, e, dt, button, config) :
                                $.fn.dataTable.ext.buttons.csvFlash.action.call(self, e, dt, button, config);
                        } else if (button[0].className.indexOf('buttons-pdf') >= 0) {
                            $.fn.dataTable.ext.buttons.pdfHtml5.available(dt, config) ?
                                $.fn.dataTable.ext.buttons.pdfHtml5.action.call(self, e, dt, button, config) :
                                $.fn.dataTable.ext.buttons.pdfFlash.action.call(self, e, dt, button, config);
                        } else if (button[0].className.indexOf('buttons-print') >= 0) {
                            $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
                        }
                        dt.one('preXhr', function (e, s, data) {
                            settings._iDisplayStart = oldStart;
                            data.start = oldStart;
                        });
                        setTimeout(dt.ajax.reload, 0);
                        return false;
                    });
                });
                dt.ajax.reload();
            }
        });
    </script>
@endsection
