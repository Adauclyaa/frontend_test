@extends('layouts.admin')

@section('content')

    <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
        <div class="d-flex flex-column flex-column-fluid">
            <div id="kt_app_toolbar" class="app-toolbar py-2 py-lg-4">
                <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
                    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ $title }}</h1>
                    </div>
                    <div class="d-flex align-items-center gap-2 gap-lg-3">
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('/') }}" class="text-muted text-hover-primary">Administrasi</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-400 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">Tenant</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="kt_app_content" class="app-content flex-column-fluid">
                <!--begin::Content container-->
                <div id="kt_app_content_container" class="app-container container-xxl">
                    <!--begin::Card-->
                    <div class="card card-flush">
                        <div class="card-header">
                            <div class="card-title">
                                <h3><?php $company=explode(',', $tenant->companyname) ?><?= ucwords(strtolower($company[0])) ?></h3>
                            </div>
                            <div class="card-toolbar">
                                <h3>{{$tenant->namakategori}}</h3>
                            </div>
                        </div>
                        <!--begin::Card body-->
                        <div class="card-body">
                            <!--begin:::Tabs-->
                            <ul class="nav nav-tabs nav-line-tabs nav-line-tabs-2x border-transparent fs-4 fw-semibold mb-15">
                                <!--begin:::Tab item-->
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary pb-5 active" data-bs-toggle="tab" href="#kt_ecommerce_settings_general">
                                    <!--begin::Svg Icon | path: icons/duotune/general/gen001.svg-->
                                    <span class="svg-icon svg-icon-2 me-2">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M11 2.375L2 9.575V20.575C2 21.175 2.4 21.575 3 21.575H9C9.6 21.575 10 21.175 10 20.575V14.575C10 13.975 10.4 13.575 11 13.575H13C13.6 13.575 14 13.975 14 14.575V20.575C14 21.175 14.4 21.575 15 21.575H21C21.6 21.575 22 21.175 22 20.575V9.575L13 2.375C12.4 1.875 11.6 1.875 11 2.375Z" fill="currentColor" />
                                        </svg>
                                    </span>
                                    <!--end::Svg Icon-->General</a>
                                </li>
                                <!--end:::Tab item-->
                                <!--begin:::Tab item-->
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary pb-5" data-bs-toggle="tab" href="#kt_ecommerce_settings_store">
                                    <!--begin::Svg Icon | path: icons/duotune/ecommerce/ecm004.svg-->
                                    <span class="svg-icon svg-icon-2 me-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <polygon points="0 0 24 0 24 24 0 24"/>
                                                <path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" fill="currentColor" fill-rule="nonzero" opacity="0.3"/>
                                                <rect fill="#000000" x="6" y="11" width="9" height="2" rx="1"/>
                                                <rect fill="#000000" x="6" y="15" width="5" height="2" rx="1"/>
                                            </g>
                                        </svg>
                                    </span>
                                    <!--end::Svg Icon-->Document</a>
                                </li>
                                <!--end:::Tab item-->
                                <!--begin:::Tab item-->
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary pb-5" data-bs-toggle="tab" href="#kt_ecommerce_settings_localization">
                                    <!--begin::Svg Icon | path: icons/duotune/maps/map004.svg-->
                                    <span class="svg-icon svg-icon-2 me-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"/>
                                                <path d="M5.5,2 L18.5,2 C19.3284271,2 20,2.67157288 20,3.5 L20,6.5 C20,7.32842712 19.3284271,8 18.5,8 L5.5,8 C4.67157288,8 4,7.32842712 4,6.5 L4,3.5 C4,2.67157288 4.67157288,2 5.5,2 Z M11,4 C10.4477153,4 10,4.44771525 10,5 C10,5.55228475 10.4477153,6 11,6 L13,6 C13.5522847,6 14,5.55228475 14,5 C14,4.44771525 13.5522847,4 13,4 L11,4 Z" fill="currentColor" opacity="0.3"/>
                                                <path d="M5.5,9 L18.5,9 C19.3284271,9 20,9.67157288 20,10.5 L20,13.5 C20,14.3284271 19.3284271,15 18.5,15 L5.5,15 C4.67157288,15 4,14.3284271 4,13.5 L4,10.5 C4,9.67157288 4.67157288,9 5.5,9 Z M11,11 C10.4477153,11 10,11.4477153 10,12 C10,12.5522847 10.4477153,13 11,13 L13,13 C13.5522847,13 14,12.5522847 14,12 C14,11.4477153 13.5522847,11 13,11 L11,11 Z M5.5,16 L18.5,16 C19.3284271,16 20,16.6715729 20,17.5 L20,20.5 C20,21.3284271 19.3284271,22 18.5,22 L5.5,22 C4.67157288,22 4,21.3284271 4,20.5 L4,17.5 C4,16.6715729 4.67157288,16 5.5,16 Z M11,18 C10.4477153,18 10,18.4477153 10,19 C10,19.5522847 10.4477153,20 11,20 L13,20 C13.5522847,20 14,19.5522847 14,19 C14,18.4477153 13.5522847,18 13,18 L11,18 Z" fill="currentColor"/>
                                            </g>
                                        </svg>
                                    </span>
                                    <!--end::Svg Icon-->Activity</a>
                                </li>
                                <!--end:::Tab item-->
                                <!--begin:::Tab item-->
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary pb-5" data-bs-toggle="tab" href="#kt_ecommerce_settings_customers">
                                    <!--begin::Svg Icon | path: icons/duotune/communication/com014.svg-->
                                    <span class="svg-icon svg-icon-2 me-2">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M16.0173 9H15.3945C14.2833 9 13.263 9.61425 12.7431 10.5963L12.154 11.7091C12.0645 11.8781 12.1072 12.0868 12.2559 12.2071L12.6402 12.5183C13.2631 13.0225 13.7556 13.6691 14.0764 14.4035L14.2321 14.7601C14.2957 14.9058 14.4396 15 14.5987 15H18.6747C19.7297 15 20.4057 13.8774 19.912 12.945L18.6686 10.5963C18.1487 9.61425 17.1285 9 16.0173 9Z" fill="currentColor" />
                                            <rect opacity="0.3" x="14" y="4" width="4" height="4" rx="2" fill="currentColor" />
                                            <path d="M4.65486 14.8559C5.40389 13.1224 7.11161 12 9 12C10.8884 12 12.5961 13.1224 13.3451 14.8559L14.793 18.2067C15.3636 19.5271 14.3955 21 12.9571 21H5.04292C3.60453 21 2.63644 19.5271 3.20698 18.2067L4.65486 14.8559Z" fill="currentColor" />
                                            <rect opacity="0.3" x="6" y="5" width="6" height="6" rx="3" fill="currentColor" />
                                        </svg>
                                    </span>
                                    <!--end::Svg Icon-->Users Tenant</a>
                                </li>
                                <!--end:::Tab item-->
                                <!--begin:::Tab item-->
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary pb-5" data-bs-toggle="tab" href="#chat_internal">
                                    <!--begin::Svg Icon | path: icons/duotune/communication/com014.svg-->
                                    <span class="svg-icon svg-icon-2 me-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"/>
                                                <path d="M16,15.6315789 L16,12 C16,10.3431458 14.6568542,9 13,9 L6.16183229,9 L6.16183229,5.52631579 C6.16183229,4.13107011 7.29290239,3 8.68814808,3 L20.4776218,3 C21.8728674,3 23.0039375,4.13107011 23.0039375,5.52631579 L23.0039375,13.1052632 L23.0206157,17.786793 C23.0215995,18.0629336 22.7985408,18.2875874 22.5224001,18.2885711 C22.3891754,18.2890457 22.2612702,18.2363324 22.1670655,18.1421277 L19.6565168,15.6315789 L16,15.6315789 Z" fill="currentColor"/>
                                                <path d="M1.98505595,18 L1.98505595,13 C1.98505595,11.8954305 2.88048645,11 3.98505595,11 L11.9850559,11 C13.0896254,11 13.9850559,11.8954305 13.9850559,13 L13.9850559,18 C13.9850559,19.1045695 13.0896254,20 11.9850559,20 L4.10078614,20 L2.85693427,21.1905292 C2.65744295,21.3814685 2.34093638,21.3745358 2.14999706,21.1750444 C2.06092565,21.0819836 2.01120804,20.958136 2.01120804,20.8293182 L2.01120804,18.32426 C1.99400175,18.2187196 1.98505595,18.1104045 1.98505595,18 Z M6.5,14 C6.22385763,14 6,14.2238576 6,14.5 C6,14.7761424 6.22385763,15 6.5,15 L11.5,15 C11.7761424,15 12,14.7761424 12,14.5 C12,14.2238576 11.7761424,14 11.5,14 L6.5,14 Z M9.5,16 C9.22385763,16 9,16.2238576 9,16.5 C9,16.7761424 9.22385763,17 9.5,17 L11.5,17 C11.7761424,17 12,16.7761424 12,16.5 C12,16.2238576 11.7761424,16 11.5,16 L9.5,16 Z" fill="currentColor" opacity="0.3"/>
                                            </g>
                                        </svg>
                                    </span>
                                    <!--end::Svg Icon-->Chat Internal</a>
                                </li>
                                <!--end:::Tab item-->
                            </ul>
                            <!--end:::Tabs-->
                            <!--begin:::Tab content-->
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="kt_ecommerce_settings_general" role="tabpanel">
                                    <div class="row mb-7">
                                        <label class="col-lg-4 fw-semibold text-muted">Company Name</label>
                                        <div class="col-lg-8">
                                            <span class="fw-bold fs-6 text-gray-800">{{ $tenant->companyname }}</span>
                                        </div>
                                    </div>
                                    <div class="row mb-7">
                                        <label class="col-lg-4 fw-semibold text-muted">Company Category</label>
                                        <div class="col-lg-8">
                                            <span class="fw-bold fs-6 text-gray-800">{{ $tenant->namakategori }}</span>
                                        </div>
                                    </div>
                                    <div class="row mb-7">
                                        <label class="col-lg-4 fw-semibold text-muted">Store Name</label>
                                        <div class="col-lg-8">
                                            <span class="fw-bold fs-6 text-gray-800">{{ $tenant->storename }}</span>
                                        </div>
                                    </div>
                                    <div class="row mb-7">
                                        <label class="col-lg-4 fw-semibold text-muted">Unit Number</label>
                                        <div class="col-lg-8">
                                            <span class="fw-bold fs-6 text-gray-800">{{ $tenant->nomorunit }}</span>
                                        </div>
                                    </div>
                                    <div class="row mb-7">
                                        <label class="col-lg-4 fw-semibold text-muted">Rental Lease (m<sup>2</sup>)</label>
                                        <div class="col-lg-8">
                                            <span class="fw-bold fs-6 text-gray-800">{{ $tenant->luas }}</span>
                                        </div>
                                    </div>
                                    <div class="row mb-7">
                                        <label class="col-lg-4 fw-semibold text-muted">Rental Price (Rp)</label>
                                        <div class="col-lg-8">
                                            <span class="fw-bold fs-6 text-gray-800">-</span>
                                        </div>
                                    </div>
                                    <div class="row mb-7">
                                        <label class="col-lg-4 fw-semibold text-muted">Address</label>
                                        <div class="col-lg-8">
                                            <span class="fw-bold fs-6 text-gray-800">{{ $tenant->address }}</span>
                                        </div>
                                    </div>
                                    <div class="row mb-7">
                                        <label class="col-lg-4 fw-semibold text-muted">Business Type</label>
                                        <div class="col-lg-8 fv-row">
                                            <span class="fw-bold text-gray-800 fs-6">{{ $tenant->store_type }}</span>
                                        </div>
                                    </div>
                                    <div class="row mb-7">
                                        <label class="col-lg-4 fw-semibold text-muted">Company Phone</label>
                                        <div class="col-lg-8 fv-row">
                                            <span class="fw-bold text-gray-800 fs-6">{{ $tenant->phone }}</span>
                                        </div>
                                    </div>
                                    <div class="row mb-7">
                                        <label class="col-lg-4 fw-semibold text-muted">Contact Email
                                            <!-- <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" aria-label="Phone number must be active" data-kt-initialized="1"></i> -->
                                        </label>
                                        <div class="col-lg-8 d-flex align-items-center">
                                            <span class="fw-bold fs-6 text-gray-800 me-2">{{ $tenant->email }}</span>
                                            {{-- <span class="badge badge-success">Verified</span> --}}
                                        </div>
                                    </div>
                                    <div class="row mb-7">
                                        <label class="col-lg-4 fw-semibold text-muted">Description</label>
                                        <div class="col-lg-8">
                                            <a href="#" class="fw-bold fs-6 text-gray-800 text-hover-primary">{{ $tenant->description }}</a>
                                        </div>
                                    </div>
                                    {{-- <div class="row mb-7">
                                        <label class="col-lg-4 fw-semibold text-muted">Type</label>
                                        <div class="col-lg-8">
                                            <a href="#" class="fw-bold fs-6 text-gray-800 text-hover-primary"><?= $tenant->type?></a>
                                        </div>
                                    </div>
                                    <div class="row mb-7">
                                        <label class="col-lg-4 fw-semibold text-muted">Note</label>
                                        <div class="col-lg-8">
                                            <a href="#" class="fw-bold fs-6 text-gray-800 text-hover-primary"><?= $tenant->note?></a>
                                        </div>
                                    </div> --}}
                                    {{-- <div class="notice d-flex bg-light-warning rounded border-warning border border-dashed p-6">
                                        <span class="svg-icon svg-icon-2tx svg-icon-warning me-4">
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="currentColor"></rect>
                                                <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="currentColor"></rect>
                                                <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="currentColor"></rect>
                                            </svg>
                                        </span>
                                        <div class="d-flex flex-stack flex-grow-1">
                                            <div class="fw-semibold">
                                                <h4 class="text-gray-900 fw-bold">Informasi</h4>
                                                <div class="fs-6 text-gray-700">Tenant yang di daftarkan oleh admin akan bertype "manual" dan jika tenant mendaftar sendiri maka akan bertype "self"
                                                </div>
                                            </div>
                                        </div>
                                    </div> --}}
                                </div>

                                <div class="tab-pane fade" id="kt_ecommerce_settings_store" role="tabpanel">
                                    <table class="table table-row-bordered gy-5 gs-7 border rounded" id="data_document" style="width:100%;">
                                        <thead>
                                            <tr>
                                                <th style="width: 45%">Document Name</th>
                                                <th style="width: 15%">Approved By</th>
                                                <th style="width: 15%">Approved Date Time</th>
                                                <th style="width: 15%">Expired Date </th>
                                                <th style="width: 10%">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                if(!empty($document)){
                                                foreach ($document as $key => $value) { ?>
                                                    <tr>
                                                        <td>
                                                            <a href="javascript:void(0)" class="showfile" link="{{$value->url.$tenant->tenantcompanyid.'/'.$value->file}}"
                                                                docid="{{$value->id}}" docdate="{{$value->created_at}}" expdate="{{$value->expireddate}}"
                                                                hasexpiredate="{{$value->hasexpireddate}}" totalrelease="{{$value->release}}">
                                                                <img src="{!! asset('theme/img/pdf.svg') !!}" style="width:35px; height:35px;"> {{ $value->documentname }}
                                                            </a>
                                                        </td>
                                                        <td><?= $value->name_approval?></td>
                                                        <td><?= $value->documentapprovaldate?></td>
                                                        <td><?= ($value->expireddate != null) ? $value->expireddate : "-" ?></td>
                                                        <td><?= $value->status?></td>
                                                    </tr>
                                            <?php } } ?>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="tab-pane fade" id="kt_ecommerce_settings_localization" role="tabpanel">
                                    <div class="card">
                                        <!--end::Card head-->
                                        <!--begin::Card body-->
                                        <div class="card-body">
                                            <!--begin::Tab Content-->
                                            <div class="tab-content">
                                                <!--begin::Tab panel-->
                                                <div id="kt_activity_today" class="card-body p-0 tab-pane fade show active" role="tabpanel" aria-labelledby="kt_activity_today_tab">
                                                    <!--begin::Timeline-->
                                                    <div class="timeline">
                                                        <?php if(isset($getDocumentActivity)){ foreach($getDocumentActivity as $key => $value) {?>
                                                        <!--begin::Timeline item-->
                                                        <div class="timeline-item">
                                                            <!--begin::Timeline line-->
                                                            <div class="timeline-line w-40px"></div>
                                                            <!--end::Timeline line-->
                                                            <!--begin::Timeline icon-->
                                                            <div class="timeline-icon symbol symbol-circle symbol-40px">
                                                                <div class="symbol-label bg-light">
                                                                    <!--begin::Svg Icon | path: icons/duotune/communication/com009.svg-->
                                                                    <span class="svg-icon svg-icon-2 svg-icon-gray-500">
                                                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                            <path opacity="0.3" d="M5.78001 21.115L3.28001 21.949C3.10897 22.0059 2.92548 22.0141 2.75004 21.9727C2.57461 21.9312 2.41416 21.8418 2.28669 21.7144C2.15923 21.5869 2.06975 21.4264 2.0283 21.251C1.98685 21.0755 1.99507 20.892 2.05201 20.7209L2.886 18.2209L7.22801 13.879L10.128 16.774L5.78001 21.115Z" fill="currentColor" />
                                                                            <path d="M21.7 8.08899L15.911 2.30005C15.8161 2.2049 15.7033 2.12939 15.5792 2.07788C15.455 2.02637 15.3219 1.99988 15.1875 1.99988C15.0531 1.99988 14.92 2.02637 14.7958 2.07788C14.6717 2.12939 14.5589 2.2049 14.464 2.30005L13.74 3.02295C13.548 3.21498 13.4402 3.4754 13.4402 3.74695C13.4402 4.01849 13.548 4.27892 13.74 4.47095L14.464 5.19397L11.303 8.35498C10.1615 7.80702 8.87825 7.62639 7.62985 7.83789C6.38145 8.04939 5.2293 8.64265 4.332 9.53601C4.14026 9.72817 4.03256 9.98855 4.03256 10.26C4.03256 10.5315 4.14026 10.7918 4.332 10.984L13.016 19.667C13.208 19.859 13.4684 19.9668 13.74 19.9668C14.0115 19.9668 14.272 19.859 14.464 19.667C15.3575 18.77 15.9509 17.618 16.1624 16.3698C16.374 15.1215 16.1932 13.8383 15.645 12.697L18.806 9.53601L19.529 10.26C19.721 10.452 19.9814 10.5598 20.253 10.5598C20.5245 10.5598 20.785 10.452 20.977 10.26L21.7 9.53601C21.7952 9.44108 21.8706 9.32825 21.9221 9.2041C21.9737 9.07995 22.0002 8.94691 22.0002 8.8125C22.0002 8.67809 21.9737 8.54505 21.9221 8.4209C21.8706 8.29675 21.7952 8.18392 21.7 8.08899Z" fill="currentColor" />
                                                                        </svg>
                                                                    </span>
                                                                    <!--end::Svg Icon-->
                                                                </div>
                                                            </div>
                                                            <!--end::Timeline icon-->
                                                            <!--begin::Timeline content-->
                                                            <div class="timeline-content mb-10 mt-n2">
                                                                <!--begin::Timeline heading-->
                                                                <div class="overflow-auto pe-3">
                                                                    <!--begin::Title-->
                                                                    <div class="fs-5 fw-semibold mb-2"><?php echo $value->activity; ?></div>
                                                                    <!--end::Title-->
                                                                    <!--begin::Description-->
                                                                    <div class="d-flex align-items-center mt-1 fs-6">
                                                                        <!--begin::Info-->
                                                                        <div class="text-muted me-2 fs-7">Activity at <?php echo $value->Tgl; ?> <?php echo $value->Jam; ?> by <?php echo $value->name; ?></div>
                                                                        <!--end::Info-->
                                                                    </div>
                                                                    <!--end::Description-->
                                                                </div>
                                                                <!--end::Timeline heading-->
                                                            </div>
                                                            <!--end::Timeline content-->
                                                        </div>
                                                        <?php }}?>
                                                        <!--end::Timeline item-->
                                                    </div>
                                                    <!--end::Timeline-->
                                                </div>
                                                <!--end::Tab panel-->
                                            </div>
                                            <!--end::Tab Content-->
                                        </div>
                                        <!--end::Card body-->
                                    </div>
                                </div>
                                <!--end:::Tab pane-->
                                <!--begin:::Tab pane-->
                                <div class="tab-pane fade" id="kt_ecommerce_settings_customers" role="tabpanel">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <h5>Daftar User Tenant</h5>
                                        </div>
                                        <div class="col-lg-6">
                                            <button class="btn btn-primary btn-sm float-right" style="float:right !important;" data-bs-toggle="modal" data-bs-target="#add_pic"><i class="fas fa-plus"></i> Tambah User Tenant</button>
                                        </div>
                                    </div>
                                    <div class="row mt-3" id="list_pic">
                                    </div>
                                </div>
                                <!--end:::Tab pane-->
                                <div class="tab-pane fade" id="chat_internal" role="tabpanel">
                                    <div class="flex-lg-row-fluid ms-lg-7 ms-xl-10">
                                        <div class="card" id="kt_chat_messenger">
                                        <form action="#" method="POST" id="add_message_global" enctype="multipart/form-data">
                                            <div class="card-body" id="kt_chat_messenger_body">
                                                <div class="scroll-y me-n5 pe-5 h-300px h-lg-auto" id="isi_message" data-kt-element="messages" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_header, #kt_toolbar, #kt_footer, #kt_chat_messenger_header, #kt_chat_messenger_footer" data-kt-scroll-wrappers="#kt_content, #kt_chat_messenger_body" data-kt-scroll-offset="5px">

                                                </div>
                                            </div>
                                            <div class="card-footer pt-4" id="kt_chat_messenger_footer">
                                                <div class="row">
                                                    <div class="col-lg-12 p-0 m-0">
                                                        <textarea class="form-control mb-3" rows="2" id="input_text" name="input_text" data-kt-element="input" placeholder="Type a message"></textarea>
                                                    </div>
                                                </div>
                                                <div class="row p-0">
                                                    <div class="col-lg-6 p-0 wrapper">
                                                        <table style="width:100%;">
                                                            <tr>
                                                                <td>
                                                                    <input type="file" id="file-input">
                                                                    <label for="file-input">
                                                                        <i class="fa-solid fa-paperclip"></i>
                                                                        <span style="font-weight:normal; font-size:12px; margin;0;" id="span_label"></span>
                                                                    </label>
                                                                    <i class="fa fa-times-circle remove"></i>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <table class="float-end" style="width:100%;">
                                                            <tr>
                                                                <td>To:</td>
                                                                <td>
                                                                <select class="form-control select_option_users" data-control="select2" style="width:100%; font-size:14px;" name="to" id="to">
                                                                    <option value="0">All User</option>
                                                                    {{-- <?php foreach($this->data['list_user_admin'] as $value){?>
                                                                        <option value="<?= $value->id?>"><?= $value->name?></option>
                                                                    <?php }?> --}}
                                                                </select>
                                                                </td>
                                                            </tr>
                                                        </table>

                                                    </div>
                                                    <div class="col-lg-2">
                                                        <table style="width:100%;">
                                                            <tr>
                                                                <td>
                                                                    <button class="btn btn-success float-right float-end" id="send_message" style="font-size:14px;">Send <i class="fas fa-paper-plane" style="margin-left:7px;"></i></button>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end:::Tab content-->
                        </div>
                        <!--end::Card body-->
                    </div>
                    <!--end::Card-->
                </div>
                <!--end::Content container-->
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-delete">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Delete Document</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="#" name="deleteform" id="deleteform" method="post" enctype='multipart/form-data'>
                    <div class="modal-body">
                        <div class="form-group">
                            <h5 style="text-align:center;color:red">Are you sure to delete this document ?</h5>
                            <input type="hidden" name="idDeleteDoc" value="" id="idDeleteDoc" >
                            <input type="hidden" name="fileName" value="fileName">
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-danger" id="btnDelete">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-parent">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Additional Document</h4>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <span class="svg-icon svg-icon-1">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor"></rect>
                                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor"></rect>
                            </svg>
                        </span>
                    </div>
                </div>
                <form id="quickForm" name="quickForm" role="form">
                    <div class="modal-body">
                        <div class="form-group">
                            {{-- <input type="hidden" name="user_id" value="<?=$this->session->userdata('vendor_id') ?>"> --}}
                            <input type="hidden" id="vendor_id" name="vendor_id" value="<?= $tenant->id ?>">
                            <label>Document Name</label>

                            <select class="form-control" id="document_id" name="document_id" required="required">
                                {{-- <?php foreach ($getlistDoc as $value) {?>
                                    <option value="<?=$value->document_id?>"><?=$value->document_name ?></option>
                                <?php }?> --}}
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success" id="btn_save_attach">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-file">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" style="padding:10px 20px 10px 20px;">
                    <h4 class="modal-title">File</h4>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <span class="svg-icon svg-icon-1">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor"></rect>
                                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor"></rect>
                            </svg>
                        </span>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="card-tools">
                        <ul class="nav nav-tabs nav-line-tabs nav-line-tabs-2x border-transparent fs-4 fw-semibold mb-15">
                            <li class="nav-item">
                                <a class="nav-link text-active-primary pb-5 active" data-bs-toggle="tab" href="#tab_current_file" id="button_file">
                                    <span class="svg-icon svg-icon-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <polygon points="0 0 24 0 24 24 0 24"/>
                                                <path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" fill="currentColor" fill-rule="nonzero" opacity="0.3"/>
                                                <path d="M8.95128003,13.8153448 L10.9077535,13.8153448 L10.9077535,15.8230161 C10.9077535,16.0991584 11.1316112,16.3230161 11.4077535,16.3230161 L12.4310522,16.3230161 C12.7071946,16.3230161 12.9310522,16.0991584 12.9310522,15.8230161 L12.9310522,13.8153448 L14.8875257,13.8153448 C15.1636681,13.8153448 15.3875257,13.5914871 15.3875257,13.3153448 C15.3875257,13.1970331 15.345572,13.0825545 15.2691225,12.9922598 L12.3009997,9.48659872 C12.1225648,9.27584861 11.8070681,9.24965194 11.596318,9.42808682 C11.5752308,9.44594059 11.5556598,9.46551156 11.5378061,9.48659872 L8.56968321,12.9922598 C8.39124833,13.2030099 8.417445,13.5185067 8.62819511,13.6969416 C8.71848979,13.773391 8.8329684,13.8153448 8.95128003,13.8153448 Z" fill="currentColor"/>
                                            </g>
                                        </svg>
                                    </span>
                                    Current File
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link text-active-primary pb-5" data-bs-toggle="tab" href="#tab_history" id="button_history">
                                    <span class="svg-icon svg-icon-2 me-2">
                                        <span class="svg-icon svg-icon-2">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"/>
                                                    <path d="M12,22 C7.02943725,22 3,17.9705627 3,13 C3,8.02943725 7.02943725,4 12,4 C16.9705627,4 21,8.02943725 21,13 C21,17.9705627 16.9705627,22 12,22 Z" fill="currentColor" opacity="0.3"/>
                                                    <path d="M11.9630156,7.5 L12.0475062,7.5 C12.3043819,7.5 12.5194647,7.69464724 12.5450248,7.95024814 L13,12.5 L16.2480695,14.3560397 C16.403857,14.4450611 16.5,14.6107328 16.5,14.7901613 L16.5,15 C16.5,15.2109164 16.3290185,15.3818979 16.1181021,15.3818979 C16.0841582,15.3818979 16.0503659,15.3773725 16.0176181,15.3684413 L11.3986612,14.1087258 C11.1672824,14.0456225 11.0132986,13.8271186 11.0316926,13.5879956 L11.4644883,7.96165175 C11.4845267,7.70115317 11.7017474,7.5 11.9630156,7.5 Z" fill="currentColor"/>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                    History
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content" id="myTabContentModal">
                        <div class="tab-pane fade show active" id="tab_current_file" role="tabpanel">
                            <div style="margin-bottom: 10px;" id="box-expire">
                                <span>Expire Date : <span id="expire-text" class="text-bold"></span></span>
                                <button type="button" class="btn btn-sm btn-danger float-end" data-toggle="popover" title="Expire date form">change expire date</button>
                                <div id="PopoverContent" class="d-none PopoverContent">
                                    <div class="input-group" id="inputexpire">

                                    </div>
                                </div>
                            </div>
                            <p>Release ke : <span id="released" class="badge badge-danger"></span>&nbsp;<a id="linkdownload" href="#" target="_blank">Download</a></p><div class="my-2" id="tanggal_dokumen"></div>
                            <iframe src="" id="file_container" style="width: 100%; height:700px; border:0px;"></iframe>
                        </div>
                    </div>
                    <div class="d-none tab-pane fade" id="tab_history" role="tabpanel">
                        <table class="table" id="history-data">
                            <thead>
                                <tr>
                                    <th>Document Name</th>
                                    <th>Filename</th>
                                    <th>Release Ke</th>
                                    <th>Prev Status</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade text-left" id="add_pic" tabindex="-1" role="dialog" aria-labelledby="add_pic" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="#" method="POST" id="add_form_pic">
                    <div class="modal-header">
                        <h4 class="modal-title" id="add_pic">Tambah User Tenant</h4>
                        <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                            <span class="svg-icon svg-icon-1">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor"></rect>
                                    <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor"></rect>
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div class="modal-body">
                        <fieldset class="form-group mb-4">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label for="topik">First Name <span class="danger">*</span></label>
                                    <input type="text" name="kt_docs_repeater_basic[0][first_name]" id="first_name" class="form-control" placeholder="First Name" required>
                                </div>
                                <div class="col-lg-6">
                                    <label for="topik">Last Name <span class="danger">*</span></label>
                                    <input type="text" name="kt_docs_repeater_basic[0][last_name]" id="last_name" class="form-control" placeholder="Last Name" required>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group mb-4">
                            <label for="phone">Email</label>
                            <input type="email" name="kt_docs_repeater_basic[0][email]" id="email" class="form-control" placeholder="Email" required>
                        </fieldset>
                        <fieldset class="form-group mb-4">
                            <label class="form-label">Position:</label>
                            <select class="form-control" name="kt_docs_repeater_basic[0][position]" id="position">
                                <option value="">-- Choose Position --</option>
                                <option value="finance">Finance</option>
                                <option value="accounting">Accounting</option>
                                <option value="legal">Legal</option>
                                <option value="tax">Tax</option>
                                <option value="other">Other</option>
                            </select>
                        </fieldset>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button id="button_save_pic" class="btn btn-success px-3">Save Data</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).on('click', '.showfile', function () {

            current_doc = $(this).attr('link');
            split_url = current_doc.split('/');
            doc_name = split_url[split_url.length - 1];
            docid = $(this).attr('docid');
            docdate = $(this).attr('docdate');
            expdate = $(this).attr('expdate');
            tenantid = "{{ $tenant->id }}";
            hasexpiredate = $(this).attr('hasexpiredate');
            totalrelease = $(this).attr('totalrelease');
            if (hasexpiredate == 'yes' ) {
                $('#box-expire').show();
                let html = `
                    <input type="date" class="form-control" id="txtexpire" placeholder="expre Date" value="${expdate}">
                    <div class="input-group-append" id="button-addon1">
                        <button class="btn btn-danger btnexpire" type="button">
                            Change
                        </button>
                    </div>
                `;

                $("#inputexpire").html(html);
            } else {
                $('#box-expire').hide();
            }

            $('#modal-file').modal('show');

            $('#tanggal_dokumen').html(`Upload At: ${moment(docdate).utc().format('YYYY-MM-DD')}`);
            $('#file_container').attr('src', current_doc);
            $("#expire-text").text(expdate);
            $('#released').text(totalrelease);
            $('#linkdownload').attr('href', current_doc);
            $('#linkdownload').attr('download', doc_name);

            var table = $('#history-data').DataTable({
                "processing": true,
                "serverSide": true,
                "destroy": true,
                "searching": false,
                "ajax": {
                    "url": uri_history + '/' + vendor_id + '/' + docid,
                    "type": "POST"
                },
                "columnDefs": [{
                        "targets": [-1],
                    }, ]
            });
        });
    </script>
@endsection
