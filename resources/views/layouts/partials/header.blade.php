<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>{{ $title }} ~ SILABKES</title>
<meta name="author" content="IT Pakuwon Group">
<meta name="description" content="Tenant Management System Pakuwon Group">
<meta name="keywords" content="Pakuwon, Tenant Management">
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="shortcut icon" href="{!! asset('theme/logo/favicon.ico') !!}">
<!--begin::Fonts-->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700" />
<!--end::Fonts-->
<!--begin::Vendor Stylesheets(used by this page)-->
<link href="{!! asset('theme/datatables/datatables.min.css') !!}" rel="stylesheet" type="text/css" />

<link href="{!! asset('theme/admin/plugins/global/plugins.bundle.css') !!}" rel="stylesheet" type="text/css" />
<link href="{!! asset('theme/admin/css/style.bundle.css') !!}" rel="stylesheet" type="text/css" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<link rel="stylesheet" href="{!! asset('theme/js/jquery-ui/jquery-ui.css') !!}">

<link rel="stylesheet" type="text/css" href="{!! asset('theme/dropify/css/dropify.css') !!}">
<link rel="stylesheet" type="text/css" href="{!! asset('theme/dropify/css/dropify.min.css') !!}">
<style>

table tr { font-size: 12px !important; }
table tr td { font-size: 12px !important; }

.user {
    display: inline-block;
    width: 220px;
    height: 220px;
    border-radius: 50%;
    background-repeat: no-repeat;
    background-position: center center;
    background-size: cover;
    object-fit: cover;
}

.user-pic {
    display: inline-block;
    width: 50px;
    height: 50px;
    border-radius: 50%;
    background-repeat: no-repeat;
    background-position: center center;
    background-size: cover;
    object-fit: cover;
}

.user-pic-sm {
    display: inline-block;
    width: 30px;
    height: 30px;
    border-radius: 50%;
    background-repeat: no-repeat;
    background-position: center center;
    background-size: cover;
    object-fit: cover;
}

.cyan.darken-1 {
    color : #00ACC1 !important;
}

.icon-bg-circle {
    color: #FFFFFF;
    padding: 0.5rem;
    border-radius: 50%;
}

.bg-cyan{background-color:#00BCD4!important}

.wrapper #file-input{
    display:none;
}

.wrapper label[for='file-input'] *{
    vertical-align:middle;
    cursor:pointer;
    display: inline-flex;
    align-items: center;
    border-radius: 4px;
    font-size: 14px;
    font-weight: 600;
    color: #fff;
    font-size: 14px;
    padding: 10px 12px;
    background-color: #4245a8;
    box-shadow: 0px 0px 2px rgba(0, 0, 0, 0.25);
}

.wrapper label[for='file-input'] span{
    margin-left: 10px;
    background-color: transparent;
    border:none;
    box-shadow: none;
    color:#4245a8;
}

.wrapper i.remove{
    vertical-align:middle;
    margin-left: 5px;
    cursor:pointer;
    display:none;
}

.btn-group-xs > .btn, .btn-xs {
    padding: .25rem .4rem;
    font-size: .875rem;
    line-height: .5;
    border-radius: .2rem;
}
</style>
<style>
	#overlay {
		box-sizing: border-box;
		position: fixed;
		display: flex;
		flex-flow: column nowrap;
		align-items: center;
		justify-content: center;
		background: rgba(0, 0, 0, 0.8);
		top: 0px;
		left: 0px;
		width: 100%;
		height: 100%;
		z-index: 2147483647;
		opacity: 1;
	}

	/* Center the loader */
	#loader {
		border: 12px solid #f3f3f3;
		border-radius: 50%;
		border-top: 12px solid #009ef7;
		width: 80px;
		height: 80px;
		-webkit-animation: spin 2s linear infinite; /* Safari */
		animation: spin 2s linear infinite;
	}

	/* Safari */
	@-webkit-keyframes spin {
		0% { -webkit-transform: rotate(0deg); }
		100% { -webkit-transform: rotate(360deg); }
	}

	@keyframes spin {
		0% { transform: rotate(0deg); }
		100% { transform: rotate(360deg); }
	}

	/* Extra small devices (portrait phones, less than 576px) */
	@media (max-width: 575.98px) {
		.toolbar-account{
			display: none !important;
		}
	}

	/* Small devices (landscape phones, 576px and up) */
	@media (min-width: 576px) and (max-width: 767.98px) {
		.toolbar-account{
			display: none !important;
		}
	}

	/* Medium devices (tablets, 768px and up) */
	@media (min-width: 768px) and (max-width: 991.98px) {
		.toolbar-account{
			display: none !important;
		}
	}

	/* Large devices (desktops, 992px and up) */
	@media (min-width: 992px) and (max-width: 1199.98px) {

	}

	/* Extra large devices (large desktops, 1200px and up) */
	@media (min-width: 1200px) {

	}
</style>
<style>
    .loading:after {
        content: ' .';
        animation: dots 1.7s steps(5, end) infinite;}

    @keyframes dots {
    0%, 20% {
        color: rgba(0,0,0,0);
        text-shadow:
        .25em 0 0 rgba(0,0,0,0),
        .5em 0 0 rgba(0,0,0,0);}
    40% {
        color: white;
        text-shadow:
        .25em 0 0 rgba(0,0,0,0),
        .5em 0 0 rgba(0,0,0,0);}
    60% {
        text-shadow:
        .25em 0 0 white,
        .5em 0 0 rgba(0,0,0,0);}
    80%, 100% {
        text-shadow:
        .25em 0 0 white,
        .5em 0 0 white;}}

</style>
<div id="overlay" class="d-none">
	<div id="loader"></div>
    <span class="text-white mt-2" style="font-size:16px;">Memuat Halaman <span class="loading m-0 p-0" style="font-size:40px;"></span></span>
</div>

