
            <div id="kt_app_footer" class="app-footer">
                <div class="app-container container-fluid d-flex flex-column flex-md-row flex-center flex-md-stack py-3">
                    <div class="text-dark order-2 order-md-1">
                        <span class="text-muted fw-semibold me-1">Copyright &copy; 2022 - <?= date('Y')?></span>
                        <a href="" target="_blank" class="text-gray-800 text-hover-primary">Muhammad Syam Firdaus</a>
                    </div>
                    <ul class="menu menu-gray-600 menu-hover-primary fw-semibold order-1">
                        <span class="float-md-right d-block d-md-inline-block d-none d-lg-block text-muted fw-semibold">Hand-crafted &amp; Made with
                            <i class="bi bi-suit-heart text-danger fs-5"></i>
                        </span>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
