<script src="{!! asset('theme/admin/plugins/global/plugins.bundle.js') !!}"></script>
<script src="{!! asset('theme/admin/js/scripts.bundle.js') !!}"></script>
<!--end::Global Javascript Bundle-->

<!--begin::Vendors Javascript(used by this page)-->
<script src="{!! asset('theme/datatables/datatables.min.js') !!}"></script>
<script src="{!! asset('theme/datatables/dataTables.buttons.min.js') !!}"></script>
<script src="{!! asset('theme/datatables/buttons.flash.min.js') !!}"></script>
<script src="{!! asset('theme/datatables/jszip.min.js') !!}"></script>
<script src="{!! asset('theme/datatables/vfs_fonts.js') !!}"></script>
<script src="{!! asset('theme/datatables/buttons.html5.min.js') !!}"></script>

<script src="{!! asset('theme/admin/js/custom/apps/customers/list/list.js') !!}"></script>

<script src="{!! asset('theme/admin/js/widgets.bundle.js') !!}"></script>
<script src="{!! asset('theme/admin/js/custom/widgets.js') !!}"></script>
<script src="{!! asset('theme/admin/js/custom/apps/chat/chat.js') !!}"></script>
<script src="{!! asset('theme/admin/js/custom/utilities/modals/upgrade-plan.js') !!}"></script>
<script src="{!! asset('theme/admin/js/custom/utilities/modals/create-app.js') !!}"></script>
<script src="{!! asset('theme/admin/js/custom/utilities/modals/users-search.js') !!}"></script>

<script src="{!! asset('theme/dropify/js/dropify.js') !!}" type="text/javascript"></script>
<script src="{!! asset('theme/dropify/js/dropify.min.js') !!}" type="text/javascript"></script>

<script src="{!! asset('theme/js/jquery-ui/jquery-ui.js') !!}"></script>
<script src="{!! asset('theme/js/jquery-ui/jquery-ui.min.js') !!}"></script>

<script>
    get_local();
    function get_local() {
        var name = localStorage.getItem('name');
        var email = localStorage.getItem('email');
        $('#navbar_user_name').html(name);
        $('#navbar_user_email').html(email);
    }
</script>
