@extends('layouts.tenant')

@section('content')
<style>
    .size1{
        font-size: 14px;
    }
</style>
<div class="app-main flex-column flex-row-fluid" id="kt_app_main">
	<div class="d-flex flex-column flex-column-fluid">
		<div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
			<div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
				<div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
					<h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0"><?= $title?></h1>
					<ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
						<li class="breadcrumb-item text-muted">
							<a href="{{ url('/') }}" class="text-muted text-hover-primary">Home</a>
						</li>
						<li class="breadcrumb-item">
							<span class="bullet bg-gray-400 w-5px h-2px"></span>
						</li>
						<li class="breadcrumb-item text-muted"><?= $title?></li>
					</ul>
				</div>
			</div>
		</div>
		<div id="kt_app_content" class="app-content flex-column-fluid">
			<div id="kt_app_content_container" class="app-container container-fluid">
				<div class="row g-5 g-xl-10">
					<div class="col-xl-12 mb-xl-12">
                        <form action="{{ url('tenant/adendum_pengalihan_save') }}" method="post">
                        {!! csrf_field() !!}
                            <div class="card">
                                <div class="card-body">
                                    <div class="col-12">
                                        <h1 class="d-flex text-dark fs-2 fw-bold flex-column justify-content-center my-0 mb-10" style="text-align: center;">Mohon di print di atas Kop Surat PIHAK PERTAMA</h1>
                                        <div class="row fv-row mb-5">
                                            <div class="col-md-2 col-4">
                                                <label class="fs-6 fw-semibold form-label mt-3">
                                                    <span class="page-heading d-flex text-dark fw-bold fs-4 flex-column justify-content-center my-0">Jakarta, </span>
                                                </label>
                                            </div>
                                            <div class="col-md-2 col-8">
                                                <div class="position-relative d-flex align-items-center">
                                                    <input type="date" class="form-control form-control-solid" placeholder="Select a date" name="date" id="date" required/> 
                                                </div>
                                            </div>
                                            <div class="col-md-8 col-12"></div>
                                            <div class="col-12 mt-10">
                                                <ul style="list-style-type: none;" class="m-0 p-0">
                                                    <li class="d-flex text-dark fs-5 flex-column justify-content-center my-0">Kepada Yth.</li>
                                                    <li class="d-flex text-dark fs-5 fw-bold flex-column justify-content-center my-0">PT</li>
                                                    <li class="d-flex text-dark fs-5 fw-bold flex-column justify-content-center my-0">(Management Mal Plaza Blok M)</li>
                                                    <li class="d-flex text-dark fs-5 flex-column justify-content-center my-0">Jl. Sultan Iskandar Muda</li>
                                                    <li class="d-flex text-dark fs-5 flex-column justify-content-center my-0">Kebayoran Lama</li>
                                                    <li class="d-flex text-dark fs-5 flex-column justify-content-center my-0">Jakarta Selatan 12240</li>
                                                    <li class="d-flex text-dark fs-5 flex-column justify-content-center my-0">Indonesia</li>
                                                </ul>
                                            </div>
                                            <div class="col-md-1 col-2 mt-5">
                                                <label class="fs-6 form-label mt-3">
                                                    <span class="page-heading d-flex text-dark fs-5 flex-column justify-content-center my-0">Up:</span>
                                                </label>
                                            </div>
                                            <div class="col-md-3 col-10 mt-5">
                                                <div class="position-relative d-flex align-items-center">
                                                    <input type="text" class="form-control form-control-solid" name="up" id="up" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-8 col-12 mt-5"></div>
                                            <div class="col-12 mt-10">
                                                <ul style="list-style-type: none;" class="m-0 p-0">
                                                    <li class="d-flex text-dark fs-4 flex-column justify-content-center my-0" style="text-align: center;text-decoration: underline;">Perihal: Permohonan Pengalihan</li>
                                                    <li class="d-flex text-dark fs-5 flex-column justify-content-center my-0 mt-5">Dengan hormat,</li>
                                                    <li class="d-flex text-dark fs-5 flex-column justify-content-center my-0 mb-5">Yang bertanda tangan dibawah ini:</li>
                                                </ul>
                                            </div>
                                            <!-- Nama Perorangan/Perusahaan :	 -->
                                            <div class="col-md-4 col-12">
                                                <label class="fs-6 form-label mt-3">
                                                    <span class="page-heading d-flex text-dark fs-5 flex-column justify-content-center my-0">Nama Perorangan/Perusahaan :	</span>
                                                </label>
                                            </div>
                                            <div class="col-md-4 col-12">
                                                <div class="position-relative d-flex align-items-center">
                                                    <input type="text" class="form-control form-control-solid" name="nama_perusahaan" id="nama_perusahaan" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-4"></div>
                                            <!-- Diwakili oleh(*) : -->
                                            <div class="col-md-4 col-12 mt-2">
                                                <label class="fs-6 form-label mt-3">
                                                    <span class="page-heading d-flex text-dark fs-5 flex-column justify-content-center my-0">Diwakili oleh(*) :</span>
                                                </label>
                                            </div>
                                            <div class="col-md-4 col-12 mt-2">
                                                <div class="position-relative d-flex align-items-center">
                                                    <input type="text" class="form-control form-control-solid" name="perwakilan_perusahaan" id="perwakilan_perusahaan" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-4 mt-2"></div>
                                            <!-- Jabatan(*) : -->
                                            <div class="col-md-4 col-12 mt-2">
                                                <label class="fs-6 form-label mt-3">
                                                    <span class="page-heading d-flex text-dark fs-5 flex-column justify-content-center my-0">Jabatan(*) :</span>
                                                </label>
                                            </div>
                                            <div class="col-md-4 col-12 mt-2">
                                                <div class="position-relative d-flex align-items-center">
                                                    <input type="text" class="form-control form-control-solid" name="jabatan" id="jabatan" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-4 mt-2"></div>
                                            <!-- Alamat korespondensi : -->
                                            <div class="col-md-4 col-12 mt-2">
                                                <label class="fs-6 form-label mt-3">
                                                    <span class="page-heading d-flex text-dark fs-5 flex-column justify-content-center my-0">Alamat korespondensi :</span>
                                                </label>
                                            </div>
                                            <div class="col-md-4 col-12 mt-2">
                                                <div class="position-relative d-flex align-items-center">
                                                    <input type="text" class="form-control form-control-solid" name="alamat_korespondensi" id="alamat_korespondensi" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-4 mt-2"></div>
                                            <!-- Telepon/Fax : -->
                                            <div class="col-md-4 col-12 mt-2">
                                                <label class="fs-6 form-label mt-3">
                                                    <span class="page-heading d-flex text-dark fs-5 flex-column justify-content-center my-0">Telepon/Fax :</span>
                                                </label>
                                            </div>
                                            <div class="col-md-4 col-12 mt-2">
                                                <div class="position-relative d-flex align-items-center">
                                                    <input type="text" class="form-control form-control-solid" name="kontak_pihak_pertama" id="kontak_pihak_pertama" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-4 mt-2"></div>
                                            <!-- Pihak Pertama -->
                                            <div class="col-md-12 col-12">
                                                <label class="fs-6 form-label mt-3">
                                                    <span class="text-dark fs-5 justify-content-center my-0">(selanjutnya disebut <b>“PIHAK PERTAMA atau Yang Mengalihkan”</b>)</span>
                                                </label>
                                            </div>
                                            <!-- Pengajuan Permohonan -->
                                            <div class="col-md-12 col-12 mt-5">
                                                <label class="fs-6 form-label mt-3">
                                                    <span class="text-dark fs-5 justify-content-center my-0">Dengan ini mengajukan permohonan untuk mengalihkan hak dan kewajiban atas unit toko sebagai berikut:</span>
                                                </label>
                                            </div>
                                            <!-- Nama Toko : -->
                                            <div class="col-md-4 col-12">
                                                <label class="fs-6 form-label mt-3">
                                                    <span class="page-heading d-flex text-dark fs-5 flex-column justify-content-center my-0">Nama Toko :</span>
                                                </label>
                                            </div>
                                            <div class="col-md-4 col-12">
                                                <div class="position-relative d-flex align-items-center">
                                                    <input type="text" class="form-control form-control-solid" name="nama_toko" id="nama_toko" value="{{$Data->storename}}" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-4"></div>
                                            <!-- Lantai/Unit : -->
                                            <div class="col-md-4 col-12 mt-2">
                                                <label class="fs-6 form-label mt-3">
                                                    <span class="page-heading d-flex text-dark fs-5 flex-column justify-content-center my-0">Lantai/Unit :</span>
                                                </label>
                                            </div>
                                            <div class="col-md-4 col-12 mt-2">
                                                <div class="position-relative d-flex align-items-center">
                                                    <input type="text" class="form-control form-control-solid" name="lantai_unit" id="lantai_unit" value="{{$Data->lantai}} - {{$Data->unit}}" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-4 mt-2"></div>
                                            <!-- Unit Toko -->
                                            <div class="col-md-12 col-12">
                                                <label class="fs-6 form-label mt-3">
                                                    <span class="text-dark fs-5 justify-content-center my-0">(selanjutnya disebut <b>“Unit Toko”</b>)</span>
                                                </label>
                                            </div>
                                            <!-- Kepada : -->
                                            <div class="col-md-12 col-12 mt-5">
                                                <label class="fs-6 form-label mt-3">
                                                    <span class="text-dark fs-5 justify-content-center my-0">Kepada :</span>
                                                </label>
                                            </div>
                                            <!-- Nama : -->
                                            <div class="col-md-4 col-12 mt-2">
                                                <label class="fs-6 form-label mt-3">
                                                    <span class="page-heading d-flex text-dark fs-5 flex-column justify-content-center my-0">Nama :</span>
                                                </label>
                                            </div>
                                            <div class="col-md-4 col-12 mt-2">
                                                <div class="position-relative d-flex align-items-center">
                                                    <input type="text" class="form-control form-control-solid" name="pihak_kedua_nama" id="pihak_kedua_nama" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-4 mt-2"></div>
                                            <!-- Perusahaan : -->
                                            <div class="col-md-4 col-12 mt-2">
                                                <label class="fs-6 form-label mt-3">
                                                    <span class="page-heading d-flex text-dark fs-5 flex-column justify-content-center my-0">Perusahaan :</span>
                                                </label>
                                            </div>
                                            <div class="col-md-4 col-12 mt-2">
                                                <div class="position-relative d-flex align-items-center">
                                                    <input type="text" class="form-control form-control-solid" name="pihak_kedua_perusahaan" id="pihak_kedua_perusahaan" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-4 mt-2"></div>
                                            <!-- Diwakili oleh : -->
                                            <div class="col-md-4 col-12 mt-2">
                                                <label class="fs-6 form-label mt-3">
                                                    <span class="page-heading d-flex text-dark fs-5 flex-column justify-content-center my-0">Diwakili oleh :</span>
                                                </label>
                                            </div>
                                            <div class="col-md-4 col-12 mt-2">
                                                <div class="position-relative d-flex align-items-center">
                                                    <input type="text" class="form-control form-control-solid" name="pihak_kedua_perwakilan" id="pihak_kedua_perwakilan" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-4 mt-2"></div>
                                            <!-- Jabatan : -->
                                            <div class="col-md-4 col-12 mt-2">
                                                <label class="fs-6 form-label mt-3">
                                                    <span class="page-heading d-flex text-dark fs-5 flex-column justify-content-center my-0">Jabatan :</span>
                                                </label>
                                            </div>
                                            <div class="col-md-4 col-12 mt-2">
                                                <div class="position-relative d-flex align-items-center">
                                                    <input type="text" class="form-control form-control-solid" name="pihak_kedua_jabatan" id="pihak_kedua_jabatan" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-4 mt-2"></div>
                                            <!-- Alamat : -->
                                            <div class="col-md-4 col-12 mt-2">
                                                <label class="fs-6 form-label mt-3">
                                                    <span class="page-heading d-flex text-dark fs-5 flex-column justify-content-center my-0">Alamat :</span>
                                                </label>
                                            </div>
                                            <div class="col-md-4 col-12 mt-2">
                                                <div class="position-relative d-flex align-items-center">
                                                    <input type="text" class="form-control form-control-solid" name="pihak_kedua_alamat" id="pihak_kedua_alamat" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-4 mt-2"></div>
                                            <!-- Telepon/ Fax : -->
                                            <div class="col-md-4 col-12 mt-2">
                                                <label class="fs-6 form-label mt-3">
                                                    <span class="page-heading d-flex text-dark fs-5 flex-column justify-content-center my-0">Telepon/ Fax :</span>
                                                </label>
                                            </div>
                                            <div class="col-md-4 col-12 mt-2">
                                                <div class="position-relative d-flex align-items-center">
                                                    <input type="text" class="form-control form-control-solid" name="pihak_kedua_kontak" id="pihak_kedua_kontak" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-4 mt-2"></div>
                                            <!-- Pihak Kedua -->
                                            <div class="col-md-12 col-12">
                                                <label class="fs-6 form-label mt-3">
                                                    <span class="text-dark fs-5 justify-content-center my-0">(selanjutnya disebut <b>“PIHAK KEDUA atau Penerima Pengalihan”</b>)</span>
                                                </label>
                                            </div>
                                            <!-- Isi P1 -->
                                            <div class="col-md-12 col-12 mt-5" style="text-align: justify">
                                                <label class="fs-6 form-label mt-3">
                                                    <span class="text-dark fs-5 justify-content-center my-0">Sehubungan dengan pengalihan hak dan kewajiban sewa atas Unit Toko tersebut, PIHAK PERTAMA dengan ini bersedia untuk melunasi seluruh tunggakan kewajiban pembayaran sampai dengan tanggal efektif pengalihan yang disetujui oleh PT dan baik PIHAK PERTAMA maupun PIHAK KEDUA bersedia melengkapi dokumen-dokumen pendukung yang diperlukan oleh PT (**) terkait pengalihan tersebut, termasuk mengembalikan seluruh pejanjian yang telah dikirimkan oleh PT kepada PIHAK PERTAMA sebelum tanggal efektif pengalihan. Bilamana sampai tanggal efektif tersebut kami belum melunasi tunggakan kewajiban pembayaran dan belum melengkapi/mengembalikan dokumen yang diperlukan, maka PT berhak menolak pemohonan pengalihan ini.</span>
                                                </label>
                                            </div>
                                            <!-- Isi P2 -->
                                            <div class="col-md-12 col-12" style="text-align: justify">
                                                <label class="fs-6 form-label mt-3">
                                                    <span class="text-dark fs-5 justify-content-center my-0">Kami dengan ini mengerti dan menerima bahwa proses pengalihan akan dilakukan sesuai syarat-syarat dan ketentuan-ketentuan yang ditentukan oleh PT dan sebelum tanggal efektif pengalihan yang disetujui oleh PT , maka invoice dan faktur pajak yang telah diterbitkan atas nama PIHAK PERTAMA tidak dapat di ubah.</span>
                                                </label>
                                            </div>
                                            <!-- Isi P3 -->
                                            <div class="col-md-12 col-12" style="text-align: justify">
                                                <label class="fs-6 form-label mt-3">
                                                    <span class="text-dark fs-5 justify-content-center my-0">Kami dengan ini menyatakan bahwa kami tidak pernah mengalihkan hak sewa dan kewajiban atas Tempat Yang Disewakan tersebut kepada pihak lain tanpa persetujuan dan sepengetahuan PT . Dalam hal terdapat klaim/gugatan/tuntutan dari pihak lain sehubungan dengan pengalihan hak dan kewajiban yang dilakukan oleh pihak kami, maka dengan ini kami membebaskan PT dari segala klaim/gugatan/tuntutan baik secara perdata maupun pidana yang mungkin timbul dikemudian hari dari pihak manapun juga.</span>
                                                </label>
                                            </div>
                                            <!-- Isi P4 -->
                                            <div class="col-md-12 col-12" style="text-align: justify">
                                                <label class="fs-6 form-label mt-3">
                                                    <span class="text-dark fs-5 justify-content-center my-0">Demikianlah surat permohonan ini kami sampaikan untuk medapatkan persetujuan. Terima kasih atas perhatian dan kerjasamanya.</span>
                                                </label>
                                            </div>
                                            <!-- Pihak pertama -->
                                            <div class="col-md-6 col-12 row">
                                                <div class="col-12 mt-10" style="text-align: center;">
                                                    <label class="fs-6 form-label mt-3">
                                                        <span class="text-dark fs-5 justify-content-center my-0">Pihak Pertama,</span>
                                                    </label>
                                                </div>
                                                <div class="col-12" style="text-align: center;">
                                                    <label class="fs-6 form-label mt-3">
                                                        <span class="text-dark fs-5 justify-content-center my-0"><b>PT</b></span>
                                                    </label>
                                                </div>
                                                <div class="col-12 px-md-20" style="text-align: center;">
                                                    <input type="text" class="form-control form-control-solid" name="pt_pihak_pertama" id="pt_pihak_pertama" style="text-align: center;" required/>
                                                </div>
                                                <div class="col-12 my-10" style="text-align: center">
                                                    <label class="fs-6 form-label my-3">
                                                        <span class="text-dark fs-5 justify-content-center my-0">Materai Rp.10.000,-</span>
                                                    </label>
                                                </div>
                                                <div class="col-12" style="text-align: center;">
                                                    <label class="fs-6 form-label my-3">
                                                        <span class="text-dark fs-5 justify-content-center my-0">Nama :</span>
                                                    </label>
                                                </div>
                                                <div class="col-12 px-md-20" style="text-align: center;">
                                                    <input type="text" class="form-control form-control-solid" name="nama_pt_pihak_pertama" id="nama_pt_pihak_pertama" style="text-align: center;" required/>
                                                </div>
                                                <div class="col-12" style="text-align: center;">
                                                    <label class="fs-6 form-label mt-3">
                                                        <span class="text-dark fs-5 justify-content-center my-0">Jabatan :</span>
                                                    </label>
                                                </div>
                                                <div class="col-12 px-md-20" style="text-align: center;">
                                                    <input type="text" class="form-control form-control-solid" name="jabatan_pt_pihak_pertama" id="jabatan_pt_pihak_pertama" style="text-align: center;" required/>
                                                </div>
                                            </div>
                                            <!-- pihak kedua -->
                                            <div class="col-md-6 col-12 row">
                                                <div class="col-12 mt-10" style="text-align: center">
                                                    <label class="fs-6 form-label mt-3">
                                                        <span class="text-dark fs-5 justify-content-center my-0">Pihak Kedua,</span>
                                                    </label>
                                                </div>
                                                <div class="col-12" style="text-align: center">
                                                    <label class="fs-6 form-label mt-3">
                                                        <span class="text-dark fs-5 justify-content-center my-0"><b>PT</b></span>
                                                    </label>
                                                </div>
                                                <div class="col-12 px-md-20" style="text-align: center">
                                                    <input type="text" class="form-control form-control-solid" name="pt_kedua_kontak"  id="pt_kedua_kontak" style="text-align: center;" required/>
                                                </div>
                                                <div class="col-12 my-10" style="text-align: center">
                                                    <label class="fs-6 form-label my-3">
                                                        <span class="text-dark fs-5 justify-content-center my-0"></span>
                                                    </label>
                                                </div>
                                                <div class="col-12" style="text-align: center">
                                                    <label class="fs-6 form-label mt-3">
                                                        <span class="text-dark fs-5 justify-content-center my-0">Nama :</span>
                                                    </label>
                                                </div>
                                                <div class="col-12 px-md-20" style="text-align: center">
                                                    <input type="text" class="form-control form-control-solid" name="nama_pt_kedua_kontak" id="nama_pt_kedua_kontak" style="text-align: center;" required/>
                                                </div>
                                                <div class="col-12" style="text-align: center">
                                                    <label class="fs-6 form-label mt-3">
                                                        <span class="text-dark fs-5 justify-content-center my-0">Jabatan :</span>
                                                    </label>
                                                </div>
                                                <div class="col-12 px-md-20" style="text-align: center">
                                                    <input type="text" class="form-control form-control-solid" name="jabatan_pt_kedua_kontak" id="jabatan_pt_kedua_kontak" style="text-align: center;" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-12 mt-5">
                                                <label class="fs-6 form-label mt-3">
                                                    <span class="text-dark fs-5 justify-content-center my-0">Catatan :</span>
                                                </label>
                                            </div>
                                            <div class="col-md-12 col-12 mt-5">
                                                <label class="fs-6 form-label mt-3">
                                                    <span class="text-dark fs-5 justify-content-center my-0">(*) Di isi sesuai anggaran dasar perusahaan.</span>
                                                </label>
                                            </div>
                                            <div class="col-md-12 col-12">
                                                <label class="fs-6 form-label pt-0">
                                                    <span class="text-dark fs-5 justify-content-center my-0">(**) List dokumen yang diperlukan terlampir.</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col-6">
                                            <a class="btn btn-danger float-end mt-10" onclick="refresh();">Clear</a>
                                        </div>
                                        <div class="col-6">
                                            <button type="submit" class="btn btn-primary mt-10">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
    <script>
        window.onload = onload()
        function onload(){
            var error = <?php echo isset($error) ? json_encode($error):0; ?>;
            if(error != ''){
                toastr.error(error, 'Warning', {timeOut: 15000});
            }
        }
        function refresh(){
            document.getElementById("date").value = ""
            document.getElementById("up").value = ""
            document.getElementById("nama_perusahaan").value = ""
            document.getElementById("perwakilan_perusahaan").value = ""
            document.getElementById("jabatan").value = ""
            document.getElementById("alamat_korespondensi").value = ""
            document.getElementById("kontak_pihak_pertama").value = ""
            document.getElementById("nama_toko").value = ""
            document.getElementById("lantai_unit").value = ""
            document.getElementById("pihak_kedua_nama").value = ""
            document.getElementById("pihak_kedua_perusahaan").value = ""
            document.getElementById("pihak_kedua_perwakilan").value = ""
            document.getElementById("pihak_kedua_jabatan").value = ""
            document.getElementById("pihak_kedua_alamat").value = ""
            document.getElementById("pihak_kedua_kontak").value = ""
            document.getElementById("pt_pihak_pertama").value = ""
            document.getElementById("pt_kedua_kontak").value = ""
            document.getElementById("nama_pt_pihak_pertama").value = ""
            document.getElementById("nama_pt_kedua_kontak").value = ""
            document.getElementById("jabatan_pt_pihak_pertama").value = ""
            document.getElementById("jabatan_pt_kedua_kontak").value = ""
        }
    </script>
    <script>
        var typingTimer;                //timer identifier
        var doneTypingInterval = 500;  //time in ms, 5 seconds for example
        var $input = $('#nama_perusahaan');

        //on keyup, start the countdown
        $input.on('keyup', function () {
        clearTimeout(typingTimer);
            typingTimer = setTimeout(doneTyping, doneTypingInterval);
        });

        //on keydown, clear the countdown 
        $input.on('keydown', function () {
            clearTimeout(typingTimer);
        });

        //user is "finished typing," do something
        function doneTyping () {
            document.getElementById("pt_pihak_pertama").value=document.getElementById("nama_perusahaan").value;
        }
    </script>
    <script>
        var typingTimer2;                //timer identifier
        var doneTypingInterval2 = 500;  //time in ms, 5 seconds for example
        var $input2 = $('#perwakilan_perusahaan');

        //on keyup, start the countdown
        $input2.on('keyup', function () {
        clearTimeout(typingTimer2);
            typingTimer2 = setTimeout(doneTyping2, doneTypingInterval2);
        });

        //on keydown, clear the countdown 
        $input2.on('keydown', function () {
            clearTimeout(typingTimer2);
        });

        //user is "finished typing," do something
        function doneTyping2 () {
            document.getElementById("nama_pt_pihak_pertama").value=document.getElementById("perwakilan_perusahaan").value;
        }
    </script>
    <script>
        var typingTimer3;                //timer identifier
        var doneTypingInterval3 = 500;  //time in ms, 5 seconds for example
        var $input3 = $('#jabatan');

        //on keyup, start the countdown
        $input3.on('keyup', function () {
        clearTimeout(typingTimer3);
            typingTimer3 = setTimeout(doneTyping3, doneTypingInterval3);
        });

        //on keydown, clear the countdown 
        $input3.on('keydown', function () {
            clearTimeout(typingTimer3);
        });

        //user is "finished typing," do something
        function doneTyping3 () {
            document.getElementById("jabatan_pt_pihak_pertama").value=document.getElementById("jabatan").value;
        }
    </script>
    <script>
        var typingTimer4;                //timer identifier
        var doneTypingInterval4 = 500;  //time in ms, 5 seconds for example
        var $input4 = $('#pihak_kedua_perusahaan');

        //on keyup, start the countdown
        $input4.on('keyup', function () {
        clearTimeout(typingTimer4);
            typingTimer4 = setTimeout(doneTyping4, doneTypingInterval4);
        });

        //on keydown, clear the countdown 
        $input4.on('keydown', function () {
            clearTimeout(typingTimer4);
        });

        //user is "finished typing," do something
        function doneTyping4 () {
            document.getElementById("pt_kedua_kontak").value=document.getElementById("pihak_kedua_perusahaan").value;
        }
    </script>
    <script>
        var typingTimer5;                //timer identifier
        var doneTypingInterval5 = 500;  //time in ms, 5 seconds for example
        var $input5 = $('#pihak_kedua_jabatan');

        //on keyup, start the countdown
        $input5.on('keyup', function () {
        clearTimeout(typingTimer5);
            typingTimer5 = setTimeout(doneTyping5, doneTypingInterval5);
        });

        //on keydown, clear the countdown 
        $input5.on('keydown', function () {
            clearTimeout(typingTimer5);
        });

        //user is "finished typing," do something
        function doneTyping5 () {
            document.getElementById("jabatan_pt_kedua_kontak").value=document.getElementById("pihak_kedua_jabatan").value;
        }
    </script>
    <script>
        var typingTimer6;                //timer identifier
        var doneTypingInterval6 = 500;  //time in ms, 5 seconds for example
        var $input6 = $('#pihak_kedua_nama');

        //on keyup, start the countdown
        $input6.on('keyup', function () {
        clearTimeout(typingTimer6);
            typingTimer6 = setTimeout(doneTyping6, doneTypingInterval6);
        });

        //on keydown, clear the countdown 
        $input6.on('keydown', function () {
            clearTimeout(typingTimer6);
        });

        //user is "finished typing," do something
        function doneTyping6 () {
            document.getElementById("nama_pt_kedua_kontak").value=document.getElementById("pihak_kedua_nama").value;
        }
    </script>
@endsection
