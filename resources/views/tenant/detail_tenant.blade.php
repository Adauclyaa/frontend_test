@extends('layouts.tenant')

@section('content')
    <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
        <div class="d-flex flex-column flex-column-fluid">
            <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
                <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
                    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0"><?= $title?></h1>
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('/') }}" class="text-muted text-hover-primary">Home</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-400 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted"><?= $title?></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="kt_app_content" class="app-content flex-column-fluid">
                <div id="kt_app_content_container" class="app-container container-fluid">
                    <div class="row g-5 g-xl-10">
                        <div class="col-xl-12 mb-xl-10">
                            <div class="card">
                                <div class="card-body">
                                    <ul class="nav nav-tabs nav-line-tabs nav-line-tabs-2x border-transparent fs-4 fw-semibold mb-15" role="tablist">
                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link text-active-primary pb-5 active" data-bs-toggle="tab" href="#kt_ecommerce_settings_general" aria-selected="true" role="tab">
                                            <span class="svg-icon svg-icon-2 me-2">
                                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M11 2.375L2 9.575V20.575C2 21.175 2.4 21.575 3 21.575H9C9.6 21.575 10 21.175 10 20.575V14.575C10 13.975 10.4 13.575 11 13.575H13C13.6 13.575 14 13.975 14 14.575V20.575C14 21.175 14.4 21.575 15 21.575H21C21.6 21.575 22 21.175 22 20.575V9.575L13 2.375C12.4 1.875 11.6 1.875 11 2.375Z" fill="currentColor"></path>
                                                </svg>
                                            </span>
                                            Informasi Tenant</a>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link text-active-primary pb-5" data-bs-toggle="tab" href="#kt_ecommerce_settings_store" aria-selected="false" tabindex="-1" role="tab">
                                            <span class="svg-icon svg-icon-2 me-2">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"/>
                                                        <path d="M5.5,2 L18.5,2 C19.3284271,2 20,2.67157288 20,3.5 L20,6.5 C20,7.32842712 19.3284271,8 18.5,8 L5.5,8 C4.67157288,8 4,7.32842712 4,6.5 L4,3.5 C4,2.67157288 4.67157288,2 5.5,2 Z M11,4 C10.4477153,4 10,4.44771525 10,5 C10,5.55228475 10.4477153,6 11,6 L13,6 C13.5522847,6 14,5.55228475 14,5 C14,4.44771525 13.5522847,4 13,4 L11,4 Z" fill="currentColor" opacity="0.3"/>
                                                        <path d="M5.5,9 L18.5,9 C19.3284271,9 20,9.67157288 20,10.5 L20,13.5 C20,14.3284271 19.3284271,15 18.5,15 L5.5,15 C4.67157288,15 4,14.3284271 4,13.5 L4,10.5 C4,9.67157288 4.67157288,9 5.5,9 Z M11,11 C10.4477153,11 10,11.4477153 10,12 C10,12.5522847 10.4477153,13 11,13 L13,13 C13.5522847,13 14,12.5522847 14,12 C14,11.4477153 13.5522847,11 13,11 L11,11 Z M5.5,16 L18.5,16 C19.3284271,16 20,16.6715729 20,17.5 L20,20.5 C20,21.3284271 19.3284271,22 18.5,22 L5.5,22 C4.67157288,22 4,21.3284271 4,20.5 L4,17.5 C4,16.6715729 4.67157288,16 5.5,16 Z M11,18 C10.4477153,18 10,18.4477153 10,19 C10,19.5522847 10.4477153,20 11,20 L13,20 C13.5522847,20 14,19.5522847 14,19 C14,18.4477153 13.5522847,18 13,18 L11,18 Z" fill="currentColor"/>
                                                    </g>
                                                </svg>
                                            </span>
                                            Dokumen Legalitas</a>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link text-active-primary pb-5" data-bs-toggle="tab" href="#kt_ecommerce_settings_localization" aria-selected="false" tabindex="-1" role="tab">
                                            <span class="svg-icon svg-icon-2 me-2">
                                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M16.0173 9H15.3945C14.2833 9 13.263 9.61425 12.7431 10.5963L12.154 11.7091C12.0645 11.8781 12.1072 12.0868 12.2559 12.2071L12.6402 12.5183C13.2631 13.0225 13.7556 13.6691 14.0764 14.4035L14.2321 14.7601C14.2957 14.9058 14.4396 15 14.5987 15H18.6747C19.7297 15 20.4057 13.8774 19.912 12.945L18.6686 10.5963C18.1487 9.61425 17.1285 9 16.0173 9Z" fill="currentColor"></path>
                                                    <rect opacity="0.3" x="14" y="4" width="4" height="4" rx="2" fill="currentColor"></rect>
                                                    <path d="M4.65486 14.8559C5.40389 13.1224 7.11161 12 9 12C10.8884 12 12.5961 13.1224 13.3451 14.8559L14.793 18.2067C15.3636 19.5271 14.3955 21 12.9571 21H5.04292C3.60453 21 2.63644 19.5271 3.20698 18.2067L4.65486 14.8559Z" fill="currentColor"></path>
                                                    <rect opacity="0.3" x="6" y="5" width="6" height="6" rx="3" fill="currentColor"></rect>
                                                </svg>
                                            </span>
                                            Penanggung Jawab Tenant</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show active" id="kt_ecommerce_settings_general" role="tabpanel">
                                            <form id="kt_ecommerce_settings_general_form" class="form fv-plugins-bootstrap5 fv-plugins-framework" action="#">
                                                <div class="row mb-7">
                                                    <div class="col-md-9">
                                                        <h2>Detail Tenant</h2>
                                                    </div>
                                                </div>
                                                <div class="row fv-row mb-7 fv-plugins-icon-container">
                                                    <div class="col-md-3">
                                                        <label class="fs-6 fw-semibold form-label mt-3">
                                                            <span class="required">Nama Tenant</span>
                                                            <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" aria-label="Set the title of the store for SEO." data-kt-initialized="1"></i>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control form-control-solid" name="tenantname" value="{{ $tenant->storename }}" disabled>
                                                    <div class="fv-plugins-message-container invalid-feedback"></div></div>
                                                </div>
                                                <div class="row fv-row mb-7">
                                                    <div class="col-md-3">
                                                        <label class="fs-6 fw-semibold form-label mt-3">
                                                            <span>Lokasi Tenant</span>
                                                            <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" aria-label="Set the description of the store for SEO." data-kt-initialized="1"></i>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control form-control-solid" name="meta_keywords" value="{{ $tenant->sitename }}" disabled>
                                                    </div>
                                                </div>
                                                <div class="row fv-row mb-7">
                                                    <div class="col-md-3">
                                                        <label class="fs-6 fw-semibold form-label mt-3">
                                                            <span>Nomor Unit</span>
                                                            <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" aria-label="Set keywords for the store separated by a comma." data-kt-initialized="1"></i>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control form-control-solid" name="meta_keywords" value="{{ $tenant->lantai.'-'.$tenant->unit }}" disabled>
                                                    </div>
                                                </div>
                                                <div class="row fv-row mb-7">
                                                    <div class="col-md-3">
                                                        <label class="fs-6 fw-semibold form-label mt-3">
                                                            <span>Jenis Tenant</span>
                                                            <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" aria-label="Set theme style for the store." data-kt-initialized="1"></i>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <div class="w-100">
                                                            <input type="text" class="form-control form-control-solid" name="meta_keywords" value="{{ $tenant->store_type }}" disabled>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row fv-row mb-7 fv-plugins-icon-container">
                                                    <div class="col-md-3">
                                                        <label class="fs-6 fw-semibold form-label mt-3">
                                                            <span class="required">Luas</span>
                                                            <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" aria-label="Set the title of the store for SEO." data-kt-initialized="1"></i>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control form-control-solid" name="meta_keywords" value="{{ $tenant->luas }}" disabled>
                                                    </div>
                                                </div>
                                                <div class="row fv-row mb-7 fv-plugins-icon-container">
                                                    <div class="col-md-3">
                                                        <label class="fs-6 fw-semibold form-label mt-3">
                                                            <span class="required">Deskripsi</span>
                                                            <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" aria-label="Set the title of the store for SEO." data-kt-initialized="1"></i>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <textarea class="form-control form-control-solid" name="meta_description">{{ $tenant->description }}</textarea>
                                                    </div>
                                                </div>
                                                <div class="row py-5">
                                                    <div class="col-md-9 offset-md-3">
                                                        <div class="d-flex">
                                                            <button type="submit" data-kt-ecommerce-settings-type="submit" class="btn btn-primary">
                                                                <span class="indicator-label">Kembali ke Daftar Tenant</span>
                                                                <span class="indicator-progress">Please wait...
                                                                <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                        <div class="tab-pane fade" id="kt_ecommerce_settings_store" role="tabpanel">
                                            <div class="row mb-7">
                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                        <table class="table table-row-dashed table-row-gray-300 gy-4" id="list_document">
                                                            <thead>
                                                                <tr class="fw-bold fs-6 text-gray-800">
                                                                    <th style="width:30%;">Nama Dokumen</th>
                                                                    <th>Tanggal Upload</th>
                                                                    <th>Tanggal Ulas</th>
                                                                    <th>Tanggal Kadaluarsa</th>
                                                                    <th>Status</th>
                                                                    <th style="width:5%;">Aksi</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody></tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row py-5">
                                                <div class="col-md-9">
                                                    <div class="d-flex">
                                                        <button type="submit" data-kt-ecommerce-settings-type="submit" class="btn btn-primary">
                                                            <span class="indicator-label">Kembali ke Daftar Tenant</span>
                                                            <span class="indicator-progress">Please wait...
                                                            <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end:::Tab pane-->
                                        <!--begin:::Tab pane-->
                                        <div class="tab-pane fade" id="kt_ecommerce_settings_localization" role="tabpanel">
                                            <div class="row py-5">
                                                <div class="col-md-9">
                                                    <div class="d-flex">
                                                        <button type="submit" data-kt-ecommerce-settings-type="submit" class="btn btn-primary">
                                                            <span class="indicator-label">Kembali ke Daftar Tenant</span>
                                                            <span class="indicator-progress">Please wait...
                                                            <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end:::Tab pane-->
                                    </div>
                                    <!--end:::Tab content-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade text-left" id="add_dokumen_modal" tabindex="-1" role="dialog" aria-labelledby="add_dokumen" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form id="form_add_dokumen" name="form_add_dokumen" role="form" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="add_dokumen_label"></h4>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <span class="svg-icon svg-icon-1">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor"></rect>
                                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor"></rect>
                            </svg>
                        </span>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row mx-2 form-group">
                        <label for="file" class="col-lg-12 col-md-12" id="label_add_document"></label>
                        <div class="col-lg-12 col-md-12" id="hide">
                            <input type="file" name="file" id="file" class="dropify" data-height="160" data-max-file-size="20M" data-allowed-file-extensions="pdf" accept=".pdf">
                        </div>
                        <div class="col-12" id="expired_date">

                        </div>
                        <div id="hiddeninput"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="modal">Tutup</button>
                    <input type="hidden" id="jenis" name="jenis">
                    <button type="submit" name="submit" id="modal_submit_btn" class="btn btn-success btn-sm">Unggah</button>
                </div>
            </div>
        </form>
        </div>
    </div>

    <div class="modal fade" id="modal-file">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" style="padding:10px 20px 10px 20px;">
                    <h4 class="modal-title">File</h4>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <span class="svg-icon svg-icon-1">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor"></rect>
                                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor"></rect>
                            </svg>
                        </span>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="card-tools">
                        <ul class="nav nav-tabs nav-line-tabs nav-line-tabs-2x border-transparent fs-4 fw-semibold mb-15">
                            <li class="nav-item">
                                <a class="nav-link text-active-primary pb-5 active" data-bs-toggle="tab" href="#tab_current_file" id="button_file">
                                    <span class="svg-icon svg-icon-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <polygon points="0 0 24 0 24 24 0 24"/>
                                                <path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" fill="currentColor" fill-rule="nonzero" opacity="0.3"/>
                                                <path d="M8.95128003,13.8153448 L10.9077535,13.8153448 L10.9077535,15.8230161 C10.9077535,16.0991584 11.1316112,16.3230161 11.4077535,16.3230161 L12.4310522,16.3230161 C12.7071946,16.3230161 12.9310522,16.0991584 12.9310522,15.8230161 L12.9310522,13.8153448 L14.8875257,13.8153448 C15.1636681,13.8153448 15.3875257,13.5914871 15.3875257,13.3153448 C15.3875257,13.1970331 15.345572,13.0825545 15.2691225,12.9922598 L12.3009997,9.48659872 C12.1225648,9.27584861 11.8070681,9.24965194 11.596318,9.42808682 C11.5752308,9.44594059 11.5556598,9.46551156 11.5378061,9.48659872 L8.56968321,12.9922598 C8.39124833,13.2030099 8.417445,13.5185067 8.62819511,13.6969416 C8.71848979,13.773391 8.8329684,13.8153448 8.95128003,13.8153448 Z" fill="currentColor"/>
                                            </g>
                                        </svg>
                                    </span>
                                    Current File
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link text-active-primary pb-5" data-bs-toggle="tab" href="#tab_history" id="button_history">
                                    <span class="svg-icon svg-icon-2 me-2">
                                        <span class="svg-icon svg-icon-2">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"/>
                                                    <path d="M12,22 C7.02943725,22 3,17.9705627 3,13 C3,8.02943725 7.02943725,4 12,4 C16.9705627,4 21,8.02943725 21,13 C21,17.9705627 16.9705627,22 12,22 Z" fill="currentColor" opacity="0.3"/>
                                                    <path d="M11.9630156,7.5 L12.0475062,7.5 C12.3043819,7.5 12.5194647,7.69464724 12.5450248,7.95024814 L13,12.5 L16.2480695,14.3560397 C16.403857,14.4450611 16.5,14.6107328 16.5,14.7901613 L16.5,15 C16.5,15.2109164 16.3290185,15.3818979 16.1181021,15.3818979 C16.0841582,15.3818979 16.0503659,15.3773725 16.0176181,15.3684413 L11.3986612,14.1087258 C11.1672824,14.0456225 11.0132986,13.8271186 11.0316926,13.5879956 L11.4644883,7.96165175 C11.4845267,7.70115317 11.7017474,7.5 11.9630156,7.5 Z" fill="currentColor"/>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                    History
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content" id="myTabContentModal">
                        <div class="tab-pane fade show active" id="tab_current_file" role="tabpanel">
                            <div style="margin-bottom: 10px;" id="box-expire">
                                <span>Expire Date : <span id="expire-text" class="text-bold"></span></span>
                                <button type="button" class="btn btn-sm btn-danger float-end" data-toggle="popover" title="Expire date form">change expire date</button>
                                <div id="PopoverContent" class="d-none PopoverContent">
                                    <div class="input-group" id="inputexpire">

                                    </div>
                                </div>
                            </div>
                            <p>Release ke : <span id="released" class="badge badge-danger"></span>&nbsp;<a id="linkdownload" href="#" target="_blank">Download</a></p><div class="my-2" id="tanggal_dokumen"></div>
                            <iframe src="" id="file_container" style="width: 100%; height:700px; border:0px;"></iframe>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab_history" role="tabpanel">
                        <table class="table" id="historydata">
                            <thead>
                                <tr>
                                    <th>Nama Dokumen</th>
                                    <th>Release Ke</th>
                                    <th>Status Sebelumnya</th>
                                    <th>Tanggal Penggantian</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        var csrf = "{{ csrf_token() }}";
        var tenantid = "{{ $tenant->id }}";
        var tenantcompanyid = "{{ $tenant->tenantcompanyid }}";
        var categoryid = "{{ $tenant->categoryid }}";

        function reload() {
            $('#list_document').DataTable().ajax.reload();
        }

        $(document).ready(function(){
            $('.dropify').dropify({
                messages: {
                    default: '<div style="font-size:12px; font-weight:bold;">Drag and drop or click to select document</div>',
                    replace: 'Ganti',
                    remove:  'Hapus',
                    error:   'error'
                }
            });
        });

        $(document).ready(function(){
            var table = $('#list_document').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Indonesian.json"
                },
                "processing": true,
                "serverSide": true,
                "order": [],
                "columnDefs": [
                    { "searchable": false, "targets": [0, 3] },  // Disable search on first and last columns
                    { className: 'text-center', targets: [1,2,3,4,5] }
                ],
                "ajax": {
                    "url": "{{ route('tenant/list_document') }}",
                    "type": "POST",
                    "data": {tenantid: tenantid, categoryid: categoryid, tenantcompanyid: tenantcompanyid},
                    "headers": {
                        'X-CSRF-TOKEN': csrf
                    }
                },
                bFilter: false,
                ordering: false,
                paging: false,
                ordering: false,
                info: false,
                columns: [
                    {data: 'document', name: 'document'},
                    {data: 'expireddate', name: 'expireddate'},
                    {data: 'expireddate', name: 'expireddate'},
                    {data: 'expireddate', name: 'expireddate'},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
        });


        function modal_upload(id, documentid, hasexpired, documentname) {

            var html = '';
            var exp = '';
            if(id == null || id == 0){
                $('#add_dokumen_label').html(`Unggah Dokumen`);
                $('#label_add_document').html(`Unggah ${documentname}<span class="text-danger">*</span>`);
                $('#add_dokumen_modal').modal('show');

                html = `<input name="type" type="hidden" id="type" value="tambah">
                        <input name="documentid" type="hidden" id="documentid" value="${documentid}">
                        <input name="tenantid" type="hidden" id="tenantid" value="${tenantid}">`;
                $('#hiddeninput').html(html);

                if(hasexpired == 'yes'){
                    exp = `<label for="expireddocument" class="col-lg-12 col-md-12 mt-6">Expired Document</label>
                            <input type="date" class="form-control" name="expireddocument" id="expireddocument" required>`;
                }else{
                    exp = '';
                }
                $('#expired_date').html(exp);
            }
        }

        function modal_update(id, exp, documentname){
            $('#add_dokumen_label').html(`Perbarui Dokumen`);
            $('#label_add_document').html(`Unggah ${documentname}<span class="text-danger">*</span>`);
            $('#add_dokumen_modal').modal('show');

            html = `<input name="type" type="hidden" id="type" value="update">
                    <input name="docid" type="hidden" id="docid" value="${id}">`;
                $('#hiddeninput').html(html);

            if(exp == "yes"){
                exp = `<label for="expireddocument" class="col-lg-12 col-md-12 mt-6">Expired Document</label>
                        <input type="date" class="form-control" name="expireddocument" id="expireddocument" required>`;
            }else{
                exp = '';
            }
            $('#expired_date').html(exp);
        }

        function modal_revisi(id, exp, documentname){
            $('#add_dokumen_label').html(`Revisi Dokumen`);
            $('#label_add_document').html(`Unggah ${documentname}<span class="text-danger">*</span>`);
            $('#add_dokumen_modal').modal('show');

            html = `<input name="type" type="hidden" id="type" value="update">
                    <input name="docid" type="hidden" id="docid" value="${id}">`;
                $('#hiddeninput').html(html);

            if(exp == "yes"){
                exp = `<label for="expireddocument" class="col-lg-12 col-md-12 mt-6">Expired Document</label>
                        <input type="date" class="form-control" name="expireddocument" id="expireddocument" required>`;
            }else{
                exp = '';
            }
            $('#expired_date').html(exp);
        }

        $(function() {
            $("#form_add_dokumen").submit(function(e){
                e.preventDefault();

                $('#modal_submit_btn').html('<span class="spinner-border spinner-border-sm align-middle ms-2"></span> Memproses...');
                $('#modal_submit_btn').attr('disabled',true);
                var url = "{{ url('tenant/upload_document_legal') }}";
                var formData = new FormData($("#form_add_dokumen")[0]);

                $.ajax({
                    url : url,
                    type: "POST",
                    data: formData,
                    contentType: false,
                    processData: false,
                    dataType: "JSON",
                    headers: {
                        'X-CSRF-TOKEN': csrf
                    },
                    success: function(data)
                    {
                    if(data.status){
                        toastr.success(data.message, 'Success', {timeOut: 15000});
                        $("#add_dokumen_modal").modal('hide');
                        $('#modal_submit_btn').html('Unggah');
                        $('#modal_submit_btn').attr('disabled',false);
                        reload()
                    }else{
                        toastr.error(data.message, 'Warning', {timeOut: 5000});
                        $('#modal_submit_btn').html('Unggah');
                        $('#modal_submit_btn').attr('disabled',false);
                    }
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        toastr.error('No data updated', 'Warning', {timeOut: 5000});
                        $('#btn_save').html('Unggah');
                        $('#btn_save').attr('disabled',false);

                    }
                });
                return false;
            });
        });
        $(document).on('click', '.showfile', function () {
            current_doc = $(this).attr('link');
            split_url = current_doc.split('/');
            doc_name = split_url[split_url.length - 1];
            docid = $(this).attr('docid');
            docdate = $(this).attr('docdate');
            expdate = $(this).attr('expdate');
            tenantid = "{{ $tenant->id }}";
            hasexpiredate = $(this).attr('hasexpiredate');
            totalrelease = $(this).attr('totalrelease');
            if (hasexpiredate == 'yes' ) {
                $('#box-expire').show();
                let html = `
                    <input type="date" class="form-control" id="txtexpire" placeholder="expre Date" value="${expdate}">
                    <div class="input-group-append" id="button-addon1">
                        <button class="btn btn-danger btnexpire" type="button">
                            Change
                        </button>
                    </div>
                `;

                $("#inputexpire").html(html);
            } else {
                $('#box-expire').hide();
            }

            $('#modal-file').modal('show');

            $('#tanggal_dokumen').html(`Upload At: ${moment(docdate).utc().format('YYYY-MM-DD')}`);
            $('#file_container').attr('src', current_doc);
            $("#expire-text").text(expdate);
            $('#released').text(totalrelease);
            $('#linkdownload').attr('href', current_doc);
            $('#linkdownload').attr('download', doc_name);

            var table = $('#historydata').DataTable({
            "processing": true,
            "serverSide": true,
            "destroy": true,
            "searching": false,
            "ajax": {
                "url": `{{ url('tenant/documenthistory') }}`,
                "headers": {
                    'X-CSRF-TOKEN': csrf
                },
                "type": "POST",
                "data": {tenantid: tenantid, tenantcompanyid: tenantcompanyid, docid: docid},
            },

            "columns": [
                {data: 'document', name: 'document'},
                {data: 'release', name: 'release'},
                {data: 'status', name: 'status'},
                {data: 'tanggal', name: 'tanggal'},
            ],
            });
        });
    </script>
@endsection
