@extends('layouts.tenant')

@section('content')
<style>
    .image-input-placeholder {
        background-image: url('svg/avatars/blank.svg');
    }

    [data-bs-theme="dark"] .image-input-placeholder {
        background-image: url('svg/avatars/blank-dark.svg');
    }
</style>
<div class="app-main flex-column flex-row-fluid" id="kt_app_main">
	<div class="d-flex flex-column flex-column-fluid">
		<div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
			<div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
				<div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
					<h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0"><?= $title?></h1>
				</div>
			</div>
		</div>
		<div id="kt_app_content" class="app-content flex-column-fluid">
			<div id="kt_app_content_container" class="app-container container-fluid">
				<div class="row g-5 g-xl-10">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="stepper stepper-pills stepper-column d-flex flex-column flex-lg-row" id="kt_stepper_example_vertical">
                                    <div class="d-flex flex-row-auto w-100 w-lg-300px">
                                    <div class="stepper-nav flex-cente">
                                        <div class="stepper-item me-5 current" data-kt-stepper-element="nav">
                                            <div class="stepper-wrapper d-flex align-items-center">
                                                <div class="stepper-icon w-40px h-40px">
                                                    <i class="stepper-check fas fa-check"></i>
                                                    <span class="stepper-number">1</span>
                                                </div>
                                                <div class="stepper-label">
                                                    <h3 class="stepper-title">
                                                        Step 1
                                                    </h3>
                                                    <div class="stepper-desc">
                                                        Intro
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="stepper-line h-40px"></div>
                                        </div>

                                        <div class="stepper-item me-5" data-kt-stepper-element="nav">
                                            <div class="stepper-wrapper d-flex align-items-center">
                                                <div class="stepper-icon w-40px h-40px">
                                                    <i class="stepper-check fas fa-check"></i>
                                                    <span class="stepper-number">2</span>
                                                </div>

                                                <div class="stepper-label">
                                                    <h3 class="stepper-title">
                                                        Step 2
                                                    </h3>

                                                    <div class="stepper-desc">
                                                        Detail Perusahaan
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="stepper-line h-40px"></div>
                                        </div>

                                        <div class="stepper-item me-5" data-kt-stepper-element="nav">
                                            <div class="stepper-wrapper d-flex align-items-center">
                                                <div class="stepper-icon w-40px h-40px">
                                                    <i class="stepper-check fas fa-check"></i>
                                                    <span class="stepper-number">3</span>
                                                </div>

                                                <div class="stepper-label">
                                                    <h3 class="stepper-title">
                                                        Step 3
                                                    </h3>

                                                    <div class="stepper-desc">
                                                        Detail Tenant
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="stepper-line h-40px"></div>
                                        </div>

                                        <div class="stepper-item me-5" data-kt-stepper-element="nav">
                                            <div class="stepper-wrapper d-flex align-items-center">
                                                <div class="stepper-icon w-40px h-40px">
                                                    <i class="stepper-check fas fa-check"></i>
                                                    <span class="stepper-number">4</span>
                                                </div>

                                                <div class="stepper-label">
                                                    <h3 class="stepper-title">
                                                        Step 4
                                                    </h3>
                                                    <div class="stepper-desc">
                                                        Dokumen
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="d-block d-md-none">
                                        <div class="mt-12"></div>
                                    </div>
                                    <div class="flex-row-fluid">
                                        <form class="form w-lg-700px" id="getstartedform" name="getstartedform" novalidate="novalidate">
                                            <div class="mb-5">
                                                <div class="flex-column current" data-kt-stepper-element="content">
                                                    <div class="row mb-5">
                                                        <div class="col-lg-12">
                                                            <h4>Tentang Tenant Portal</h4>
                                                            <span>Anda dapat melihat panduan sistem tenant portal secara lengkap <a href="">disini</a></span>
                                                        </div>
                                                    </div>
                                                    <div class="row mt-5 mb-5">
                                                        <div class="col-lg-12">
                                                            <div class="m-0">
																<div class="d-flex align-items-center collapsible py-3 toggle collapsed mb-0" data-bs-toggle="collapse" data-bs-target="#kt_job_8_1">
																	<div class="btn btn-sm btn-icon mw-20px btn-active-color-primary me-5">
																		<span class="svg-icon toggle-on svg-icon-primary svg-icon-1">
																			<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
																				<rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="currentColor"></rect>
																				<rect x="6.0104" y="10.9247" width="12" height="2" rx="1" fill="currentColor"></rect>
																			</svg>
																		</span>
																		<span class="svg-icon toggle-off svg-icon-1">
																			<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
																				<rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="currentColor"></rect>
																				<rect x="10.8891" y="17.8033" width="12" height="2" rx="1" transform="rotate(-90 10.8891 17.8033)" fill="currentColor"></rect>
																				<rect x="6.01041" y="10.9247" width="12" height="2" rx="1" fill="currentColor"></rect>
																			</svg>
																		</span>
																	</div>
																	<h4 class="text-gray-700 fw-bold cursor-pointer mb-0">Apa saja yang bisa dilakukan pada sistem tenant portal ?</h4>
																</div>
																<div id="kt_job_8_1" class="collapse fs-6 ms-1">
																	<div class="mb-4 text-gray-600 fw-semibold fs-6 ps-10">First, a disclaimer – the entire process of writing a blog post often takes more than a couple of hours, even if you can type eighty words as per minute and your writing skills are sharp.</div>
																</div>
																<div class="separator separator-dashed"></div>
															</div>
                                                            <div class="m-0">
																<div class="d-flex align-items-center collapsible py-3 toggle collapsed mb-0" data-bs-toggle="collapse" data-bs-target="#kt_job_8_2">
																	<div class="btn btn-sm btn-icon mw-20px btn-active-color-primary me-5">
																		<span class="svg-icon toggle-on svg-icon-primary svg-icon-1">
																			<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
																				<rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="currentColor"></rect>
																				<rect x="6.0104" y="10.9247" width="12" height="2" rx="1" fill="currentColor"></rect>
																			</svg>
																		</span>
																		<span class="svg-icon toggle-off svg-icon-1">
																			<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
																				<rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="currentColor"></rect>
																				<rect x="10.8891" y="17.8033" width="12" height="2" rx="1" transform="rotate(-90 10.8891 17.8033)" fill="currentColor"></rect>
																				<rect x="6.01041" y="10.9247" width="12" height="2" rx="1" fill="currentColor"></rect>
																			</svg>
																		</span>
																	</div>
																	<h4 class="text-gray-700 fw-bold cursor-pointer mb-0">Jika saya memiliki kendala, apa yang harus dilakukan ?</h4>
																</div>
																<div id="kt_job_8_2" class="collapse fs-6 ms-1">
																	<div class="mb-4 text-gray-600 fw-semibold fs-6 ps-10">First, a disclaimer – the entire process of writing a blog post often takes more than a couple of hours, even if you can type eighty words as per minute and your writing skills are sharp.</div>
																</div>
																<div class="separator separator-dashed"></div>
															</div>
                                                        </div>
                                                    </div>
                                                    <div class="row mt-5 mb-5">
                                                        <div class="col-lg-12">
                                                            <a href="javascript:void(0)" onclick="document_alert()">
                                                                <div class="alert alert-info d-flex align-items-center p-5 mb-10">
                                                                    <span class="svg-icon svg-icon-2hx svg-icon-info me-4">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-file-earmark-text-fill" viewBox="0 0 16 16">
                                                                            <path d="M9.293 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V4.707A1 1 0 0 0 13.707 4L10 .293A1 1 0 0 0 9.293 0zM9.5 3.5v-2l3 3h-2a1 1 0 0 1-1-1zM4.5 9a.5.5 0 0 1 0-1h7a.5.5 0 0 1 0 1h-7zM4 10.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm.5 2.5a.5.5 0 0 1 0-1h4a.5.5 0 0 1 0 1h-4z"/>
                                                                        </svg>
                                                                    </span>
                                                                    <div class="d-flex flex-column" style="width:80%;">
                                                                        <h6 class="mb-1 text-info">Dokumen yang dibutuhkan untuk mengaktifkan akun Anda.</h6>
                                                                    </div>
                                                                    <div style="width:20%;">
                                                                        <span class="svg-icon svg-icon-2x svg-icon-info me-4 float-end">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-up-right-square" viewBox="0 0 16 16">
                                                                                <path fill-rule="evenodd" d="M15 2a1 1 0 0 0-1-1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2zM0 2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2zm5.854 8.803a.5.5 0 1 1-.708-.707L9.243 6H6.475a.5.5 0 1 1 0-1h3.975a.5.5 0 0 1 .5.5v3.975a.5.5 0 1 1-1 0V6.707l-4.096 4.096z"/>
                                                                            </svg>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="flex-column" data-kt-stepper-element="content">
                                                    <div class="fv-row mb-10">
                                                        <div class="row">
                                                            <div class="col-1">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-1-circle" viewBox="0 0 16 16">
                                                                    <path d="M1 8a7 7 0 1 0 14 0A7 7 0 0 0 1 8Zm15 0A8 8 0 1 1 0 8a8 8 0 0 1 16 0ZM9.283 4.002V12H7.971V5.338h-.065L6.072 6.656V5.385l1.899-1.383h1.312Z"/>
                                                                </svg>
                                                            </div>
                                                            <div class="col-11">
                                                                <label class="form-label">Logo Perusahaan</label>
                                                                <br>
                                                                <div class="row">
                                                                    <div class="col-lg-3">
                                                                        <div class="image-input image-input-circle" data-kt-image-input="true" style="background-image: url(/assets/media/svg/avatars/blank.svg)">
                                                                            <!--begin::Image preview wrapper-->
                                                                            <div class="image-input-wrapper w-125px h-125px" style="background-image: url('<?= ($tenant->logo == NULL) ? asset('theme/admin/media/avatars/blank.png') : $credential->url.'logo/'.$tenant->tenantcompanyid.'/'.$tenant->logo ?>')"></div>
                                                                            <!--end::Image preview wrapper-->

                                                                            <!--begin::Edit button-->
                                                                            <label class="btn btn-icon btn-circle btn-color-muted btn-active-color-primary w-25px h-25px bg-body shadow"
                                                                            data-kt-image-input-action="change"
                                                                            data-bs-toggle="tooltip"
                                                                            data-bs-dismiss="click"
                                                                            title="Change avatar">
                                                                                <i class="bi bi-pencil-fill fs-7"></i>

                                                                                <!--begin::Inputs-->
                                                                                <input type="file" name="logo" id="logo" accept=".png, .jpg, .jpeg" />
                                                                                <input type="hidden" name="avatar_remove" />
                                                                                <!--end::Inputs-->
                                                                            </label>
                                                                            <!--end::Edit button-->
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-9">
                                                                        <h6>Unggah Logo Perusahaan</h6>
                                                                        <span>Harus setidaknya 200px X 200px dan lebih kecil dari 2MB<br> dengan format file JPG atau PNG.</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="fv-row mb-10">
                                                        <div class="row">
                                                            <div class="col-1">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-2-circle" viewBox="0 0 16 16">
                                                                    <path d="M1 8a7 7 0 1 0 14 0A7 7 0 0 0 1 8Zm15 0A8 8 0 1 1 0 8a8 8 0 0 1 16 0ZM6.646 6.24v.07H5.375v-.064c0-1.213.879-2.402 2.637-2.402 1.582 0 2.613.949 2.613 2.215 0 1.002-.6 1.667-1.287 2.43l-.096.107-1.974 2.22v.077h3.498V12H5.422v-.832l2.97-3.293c.434-.475.903-1.008.903-1.705 0-.744-.557-1.236-1.313-1.236-.843 0-1.336.615-1.336 1.306Z"/>
                                                                </svg>
                                                            </div>
                                                            <div class="col-11">
                                                                <label class="form-label">Nama Perusahaan</label>
                                                                <input type="text" class="form-control" name="companyname" id="companyname" placeholder="" value="{{ $tenant->companyname }}"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="fv-row mb-10">
                                                        <div class="row">
                                                            <div class="col-1">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-3-circle" viewBox="0 0 16 16">
                                                                    <path d="M7.918 8.414h-.879V7.342h.838c.78 0 1.348-.522 1.342-1.237 0-.709-.563-1.195-1.348-1.195-.79 0-1.312.498-1.348 1.055H5.275c.036-1.137.95-2.115 2.625-2.121 1.594-.012 2.608.885 2.637 2.062.023 1.137-.885 1.776-1.482 1.875v.07c.703.07 1.71.64 1.734 1.917.024 1.459-1.277 2.396-2.93 2.396-1.705 0-2.707-.967-2.754-2.144H6.33c.059.597.68 1.06 1.541 1.066.973.006 1.6-.563 1.588-1.354-.006-.779-.621-1.318-1.541-1.318Z"/>
                                                                    <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0ZM1 8a7 7 0 1 0 14 0A7 7 0 0 0 1 8Z"/>
                                                                </svg>
                                                            </div>
                                                            <div class="col-11">
                                                                <label class="form-label">Email Perusahaan</label>
                                                                <input type="email" class="form-control" name="companymail" id="companymail" value="{{ $email }}"/>
                                                                <small>Note: jika email Anda sama dengan email perusahaan, maka tidak perlu di ubah</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="fv-row mb-10">
                                                        <div class="row">
                                                            <div class="col-1">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-4-circle" viewBox="0 0 16 16">
                                                                    <path d="M7.519 5.057c.22-.352.439-.703.657-1.055h1.933v5.332h1.008v1.107H10.11V12H8.85v-1.559H4.978V9.322c.77-1.427 1.656-2.847 2.542-4.265ZM6.225 9.281v.053H8.85V5.063h-.065c-.867 1.33-1.787 2.806-2.56 4.218Z"/>
                                                                    <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0ZM1 8a7 7 0 1 0 14 0A7 7 0 0 0 1 8Z"/>
                                                                </svg>
                                                            </div>
                                                            <div class="col-11">
                                                                <label class="form-label">NPWP Perusahaan <span class="text-gray-500">(Optional)</span></label>
                                                                <input type="text" class="form-control" name="npwp" id="npwp" value="{{ $npwp }}"/>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="fv-row mb-10">
                                                        <div class="row">
                                                            <div class="col-1">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-5-circle" viewBox="0 0 16 16">
                                                                    <path d="M1 8a7 7 0 1 1 14 0A7 7 0 0 1 1 8Zm15 0A8 8 0 1 0 0 8a8 8 0 0 0 16 0Zm-8.006 4.158c-1.57 0-2.654-.902-2.719-2.115h1.237c.14.72.832 1.031 1.529 1.031.791 0 1.57-.597 1.57-1.681 0-.967-.732-1.57-1.582-1.57-.767 0-1.242.45-1.435.808H5.445L5.791 4h4.705v1.103H6.875l-.193 2.343h.064c.17-.258.715-.68 1.611-.68 1.383 0 2.561.944 2.561 2.585 0 1.687-1.184 2.806-2.924 2.806Z"/>
                                                                </svg>
                                                            </div>
                                                            <div class="col-11">
                                                                <label class="form-label p-0 m-0">Alamat Perusahaan</label>
                                                                <div>Alamat ini harus sesuai dengan alamat di NPWP Perusahaan Anda</div>
                                                                <div class="row mt-10">
                                                                    <div class="col-lg-6">
                                                                        <label class="form-label">Provinsi</label>
                                                                        <select class="form-select" data-control="select2" data-placeholder="Select an option" name="provinsi" id="provinsi">
                                                                            <option></option>
                                                                            @foreach ($provinsi as $value)
                                                                                <option value="{{ $value->provinsi }}" <?= ($tenant->provinsi == $value->provinsi) ? 'selected' : ''?>>{{ $value->provinsi }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-6">
                                                                        <label class="form-label">Kota</label>
                                                                        <select class="form-select" data-control="select2" data-placeholder="Select an option" name="kota" id="kota">
                                                                            <option>{{ $tenant->kabupaten }}</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="row mt-10">
                                                                    <div class="col-lg-6">
                                                                        <label class="form-label">Kecamatan</label>
                                                                        <select class="form-select" data-control="select2" data-placeholder="Select an option" name="kecamatan" id="kecamatan">
                                                                            <option>{{ $tenant->kecamatan }}</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-6">
                                                                        <label class="form-label">Kelurahan</label>
                                                                        <select class="form-select" data-control="select2" data-placeholder="Select an option" name="kelurahan" id="kelurahan">
                                                                            <option>{{ $tenant->kelurahan }}</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="row mt-10">
                                                                    <div class="col-lg-6">
                                                                        <label class="form-label">Kode Pos</label>
                                                                        <input type="text" class="form-control" name="kodepos" id="kodepos" value="{{ $tenant->kodepos }}"/>
                                                                        <small id="alert_kodepos"></small>
                                                                    </div>
                                                                </div>
                                                                <div class="row mt-10">
                                                                    <div class="col-lg-12">
                                                                        <label class="form-label">Detail Alamat</label>
                                                                        <textarea class="form-control" rows="3" name="alamat" id="alamat" placeholder="Detail Alamat Perusahaan Anda">{{ $tenant->address }}</textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="flex-column" data-kt-stepper-element="content">
                                                    <div class="fv-row mb-10">
                                                        <div class="row">
                                                            <div class="col-1">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-1-circle" viewBox="0 0 16 16">
                                                                    <path d="M1 8a7 7 0 1 0 14 0A7 7 0 0 0 1 8Zm15 0A8 8 0 1 1 0 8a8 8 0 0 1 16 0ZM9.283 4.002V12H7.971V5.338h-.065L6.072 6.656V5.385l1.899-1.383h1.312Z"/>
                                                                </svg>
                                                            </div>
                                                            <div class="col-11">
                                                                <label class="form-label">Informasi Tenant</label>
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <label class="form-label">Nama Tenant</label>
                                                                        <input type="text" class="form-control" name="tenantname" id="tenantname" value="{{ $tenant->storename }}">
                                                                    </div>
                                                                </div>
                                                                <div class="fv-row mb-10 mt-10">
                                                                    <div class="row">
                                                                        <div class="col-11">
                                                                            <label class="form-label">Email Tenant</label>
                                                                            <input type="email" class="form-control" name="tenantemail" id="tenantemail" placeholder="" value="{{ $email }}"/>
                                                                            <small>Note: jika email Anda sama dengan email perusahaan, maka tidak perlu di ubah</small>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row mt-10">
                                                                    <div class="col-lg-6">
                                                                        <label class="form-label">Jenis Tenant</label>
                                                                        <select class="form-select" data-control="select2" name="jenistenant" id="jenistenant" data-placeholder="Select an option">
                                                                            <option></option>
                                                                            @foreach ($storetype as $value)
                                                                                <option value="{{ $value->id }}" <?= ($tenant->businesstype == $value->id)? "selected" : ""?>>{{ $value->nama }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-6">
                                                                        <label class="form-label">Luas Tenant</label>
                                                                        <input type="text" class="form-control" id="luas" name="luas" value="{{ $tenant->luas }}">
                                                                    </div>
                                                                </div>
                                                                <div class="row mt-10">
                                                                    <div class="col-lg-12">
                                                                        <label class="form-label">Deskripsi Tenant</label>
                                                                        <textarea class="form-control" rows="3" name="deskripsitenant" id="deskripsitenant" placeholder="Detail Tenant Anda">{{ $tenant->description }}</textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="fv-row mb-10">
                                                        <div class="row">
                                                            <div class="col-1">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-2-circle" viewBox="0 0 16 16">
                                                                    <path d="M1 8a7 7 0 1 0 14 0A7 7 0 0 0 1 8Zm15 0A8 8 0 1 1 0 8a8 8 0 0 1 16 0ZM6.646 6.24v.07H5.375v-.064c0-1.213.879-2.402 2.637-2.402 1.582 0 2.613.949 2.613 2.215 0 1.002-.6 1.667-1.287 2.43l-.096.107-1.974 2.22v.077h3.498V12H5.422v-.832l2.97-3.293c.434-.475.903-1.008.903-1.705 0-.744-.557-1.236-1.313-1.236-.843 0-1.336.615-1.336 1.306Z"/>
                                                                </svg>
                                                            </div>
                                                            <div class="col-11">
                                                                <label class="form-label">Lokasi Tenant</label>
                                                                <div class="row mt-5">
                                                                    <div class="col-lg-6">
                                                                        <label class="form-label">Lantai</label>
                                                                        <select class="form-select" data-control="select2" name="lantai" id="lantai" data-placeholder="Select an option">
                                                                            <option></option>
                                                                            <option value="LG" <?= ($tenant->lantai == "LG")? "selected" : ""?>>LG</option>
                                                                            <option value="G" <?= ($tenant->lantai == "G")? "selected" : ""?>>G</option>
                                                                            <option value="UG" <?= ($tenant->lantai == "UG")? "selected" : ""?>>UG</option>
                                                                            <option value="L1" <?= ($tenant->lantai == "L1")? "selected" : ""?>>L1</option>
                                                                            <option value="L2" <?= ($tenant->lantai == "L2")? "selected" : ""?>>L2</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-6">
                                                                        <label class="form-label">Unit</label>
                                                                        <input type="text" class="form-control" name="unit" id="unit" placeholder="" value="{{ $tenant->unit }}"/>
                                                                    </div>
                                                                </div>
                                                                <div class="row mt-5">
                                                                    <div class="col-lg-6">
                                                                        <label class="form-label">Lokasi Mall</label>
                                                                        <input type="text" class="form-control" name="lokasi" id="lokasi" value="{{ $site->sitename }}" disabled/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="flex-column" data-kt-stepper-element="content">
                                                    <div class="fv-row mb-10">
                                                        <div class="row">
                                                            @foreach ($listdocument as $value)
                                                                <div class="col-lg-6">
                                                                    <label class="form-label d-flex align-items-center">
                                                                        <span class="required">{{ $value->documentname }}</span>
                                                                    </label>
                                                                    <input type="hidden" name="id_document[]" value="{{ $value->documentid }}">
                                                                    <input type="file" name="document[]" class="dropify" data-height="120" data-max-file-size="15M" data-allowed-file-extensions="pdf" accept=".pdf" required>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="d-flex flex-stack">
                                                <div class="me-2">
                                                    <button type="button" class="btn btn-light btn-active-light-primary" data-kt-stepper-action="previous">
                                                        Kembali
                                                    </button>
                                                </div>
                                                <div>
                                                    <button type="button" class="btn btn-primary" data-kt-stepper-action="submit" id="kt_submit">
                                                        <span class="" id="indicator-label-submit">
                                                            Kirim
                                                        </span>
                                                        <span class="d-none" id="indicator-progress-submit">
                                                            Mohon Tunggu... <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                                                        </span>
                                                    </button>
                                                    <button type="button" class="btn btn-primary" data-kt-stepper-action="next" id="next">
                                                        <span class="d-none d-md-block"><span id="indicator-label">Teruskan step</span> </span>
                                                        <span class="d-block d-md-none"><span id="indicator-label">Teruskan</span></span>
                                                        <span class="d-none" id="indicator-progress">
                                                            Mohon Tunggu... <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                                                        </span>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" id="modaldocument">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <h5>Kami membutuhkan beberapa dokumen untuk menjadi persyaratan sebagai berikut:</h5>
                <ul style="font-size:14px;" class="fw-bold text-gray-700 mt-5 p-2">
                    @foreach ($listdocument as $value)
                        <li>Dokumen {{ $value->documentname }}</li>
                    @endforeach
                </ul>
                <div class="row">
                    <div class="col-lg-12 mt-5">
                        <button type="button" class="btn btn-primary btn-block w-100" data-bs-dismiss="modal">Saya Mengerti</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $('.dropify').dropify({
                messages: {
                    default: '<div style="font-size:12px;">Drag and drop or click to select document</div>',
                    replace: 'Ganti',
                    remove:  'Hapus',
                    error:   'error'
                }
            });
        });

        Inputmask({"mask" : "99.999.999-9.999.999"}).mask("#npwp");
        // Stepper lement
        var csrf = "{{ csrf_token() }}";
        var tenantid = "{{ $tenant->id }}"
        var tenantcompanyid = "{{ $tenant->tenantcompanyid }}"
        var element = document.querySelector("#kt_stepper_example_vertical");

        // Initialize Stepper
        var stepper = new KTStepper(element);

        // Handle next step
        stepper.on("kt.stepper.next", function (stepper) {

            var url = "{{ url('tenant/save_started') }}";
            var step = stepper.currentStepIndex;
            var form_data = new FormData($('#getstartedform')[0]);
            form_data.append('step', step);
            form_data.append('tenantid', tenantid);
            form_data.append('tenantcompanyid', tenantcompanyid);

            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                contentType: false,
                processData: false,
                dataType: "JSON",
                headers: {
                    'X-CSRF-TOKEN': csrf
                },
                beforeSend: function() {
                    $('#indicator-progress').removeClass('d-none');
                    $('#indicator-label').addClass('d-none');
                },
                success: function (data)
                {
                    if(data.status){
                        toastr.success(data.message, 'Success', {timeOut: 3000});
                        stepper.goNext();
                    }
                },
                complete: function() {
                    $('#indicator-progress').addClass('d-none');
                    $('#indicator-label').removeClass('d-none');
                },

            });
        });

        // Handle previous step
        stepper.on("kt.stepper.previous", function (stepper) {
            stepper.goPrevious(); // go previous step test
        });

        $("#kt_submit").click(function(e) {
            e.preventDefault();

            var url = "{{ url('tenant/save_started') }}";
            var form_data = new FormData($('#getstartedform')[0]);
            form_data.append('step', 4);
            form_data.append('tenantid', tenantid);
            form_data.append('tenantcompanyid', tenantcompanyid);

            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                contentType: false,
                processData: false,
                dataType: "JSON",
                headers: {
                    'X-CSRF-TOKEN': csrf
                },
                beforeSend: function() {
                    $('#indicator-progress-submit').removeClass('d-none');
                    $('#indicator-label-submit').addClass('d-none');
                },
                success: function (data)
                {
                    if(data.status){
                        Swal.fire({
                            title: 'Data Berhasil Dikirim',
                            html: `Kami sedang memproses permintaan Anda dan akan menghubungi Anda kembali via email dalam 1-3 hari kerja`,
                            icon: 'success',
                            type: 'success',
                            allowOutsideClick: false
                        }).then(function() {
                            $("#overlay").removeClass('d-none');
						    location.href = `{{url('tenant/profile/${tenantcompanyid}')}}`;
                        });
                    }
                },
                complete: function() {
                    $('#indicator-progress-submit').addClass('d-none');
                    $('#indicator-label-submit').removeClass('d-none');
                },

            });
        });

        function document_alert() {
            console.log('test');
            $('#modaldocument').modal('show');
        }

        $("#provinsi").on('change', function () {
            $.ajax({
                url: "{{ url('tenant/getwilayah') }}",
                type: "POST",
                data: {data: $(this).val(), get: "kabupaten", filter: "provinsi"},
                headers: {
                    'X-CSRF-TOKEN': csrf
                },
                beforeSend: function() {
                    var html = `<option></option><option>Memuat .. <i class="fas fa-1x fa-sync-alt fa-spin"></i></option>`;
                    $("#kota").html(html);
                },
                success: function (data) {
                    var html = '<option></option>';
                    for (let i = 0; i < data.length; i++) {
                        html += `<option value="${data[i].kabupaten}">${data[i].kabupaten}</option>`;
                    }

                    $("#kota").html(html);
                }
            });
        })

        $("#kota").on('change', function () {
            $.ajax({
                url: "{{ url('tenant/getwilayah') }}",
                type: "POST",
                data: {data: $(this).val(), get: "kecamatan", filter: "kabupaten"},
                headers: {
                    'X-CSRF-TOKEN': csrf
                },
                beforeSend: function() {
                    var html = `<option></option><option>Memuat .. <i class="fas fa-1x fa-sync-alt fa-spin"></i></option>`;
                    $("#kecamatan").html(html);
                },
                success: function (data) {
                    var html = '<option></option>';
                    for (let i = 0; i < data.length; i++) {
                        html += `<option value="${data[i].kecamatan}">${data[i].kecamatan}</option>`;
                    }

                    $("#kecamatan").html(html);
                }
            });
        })

        $("#kecamatan").on('change', function () {
            $.ajax({
                url: "{{ url('tenant/getwilayah') }}",
                type: "POST",
                data: {data: $(this).val(), get: "kelurahan", filter: "kecamatan"},
                headers: {
                    'X-CSRF-TOKEN': csrf
                },
                beforeSend: function() {
                    var html = `<option></option><option>Memuat .. <i class="fas fa-1x fa-sync-alt fa-spin"></i></option>`;
                    $("#kelurahan").html(html);
                },
                success: function (data) {
                    var html = '<option></option>';
                    for (let i = 0; i < data.length; i++) {
                        html += `<option value="${data[i].kelurahan}">${data[i].kelurahan}</option>`;
                    }

                    $("#kelurahan").html(html);
                }
            });
        })

        $("#kelurahan").on('change', function () {
            $.ajax({
                url: "{{ url('tenant/getwilayah') }}",
                type: "POST",
                data: {data: $(this).val(), get: "kodepos", filter: "kelurahan"},
                headers: {
                    'X-CSRF-TOKEN': csrf
                },
                success: function (data) {
                    console.log(data[0].kodepos);
                    $("#kodepos").val(data[0].kodepos);
                    $("#alert_kodepos").html('jika kode pos tidak sesuai, silahkan diubah');
                }
            });
        })
    </script>

@endsection
